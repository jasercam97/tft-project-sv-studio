﻿using Photon.Pun;
using UnityEngine;

public class ProjectileReflection : MonoBehaviourPunCallbacks
{
    #region VARIABLES

    public GameObject ExplosionEffect;
    public int Team;

    public float Speed;
    public float Damage = 25;
    public bool DamageInArea;
    public Transform Target;
    private bool init = false;

    public float LifeTime;

    [SerializeField]
    private LayerMask _mask;

    [SerializeField]
    private float _detectionRange = 10f;


    #endregion

    private void Awake()
    {
        if (photonView.IsMine)
        {
            Invoke("DestroyProjectile", LifeTime);
        }
    }

    private void Update()
    {
        if (photonView.IsMine)
        {
            if(Target == null && init)
                DestroyProjectile();
            else
                MoveForward();
        }
    }

    #region MOVE

    private void MoveForward()
    {
        transform.LookAt(Target);
        transform.Translate(Vector3.forward * Time.deltaTime * Speed);
    }

    #endregion

    #region TRIGGERS

    private void OnTriggerEnter(Collider other)
    {
        if (photonView.IsMine)
        {
            if (other.CompareTag("Troop") || other.CompareTag("Build"))
            {
                Selectable selectable = other.gameObject.GetComponent<Selectable>();
                if (selectable.RenderTroop == Target)
                {
                    if (selectable.Team != Team)
                    {
                        if (DamageInArea)
                        {
                            Collider[] troops = Physics.OverlapSphere(transform.position, _detectionRange, _mask);
                            for (int i = 0; i < troops.Length; i++)
                            {
                                if (troops[i].gameObject.GetComponent<Selectable>().Team != Team && troops[i].gameObject.GetComponent<Selectable>().Team != -1)
                                {
                                    troops[i].gameObject.GetComponent<Selectable>().TakeDamage(Damage);
                                }
                            }
                        }
                        else
                            selectable.TakeDamage(Damage);
                        DestroyProjectile();
                    }
                }
            }
        }
    }

    #endregion

    #region DESTROY

    private void DestroyProjectile()
    {
        if (ExplosionEffect != null)
        {
            Instantiate(ExplosionEffect, transform.position, Quaternion.identity);
        }

        PhotonNetwork.Destroy(photonView);
    }

    #endregion

    public void Init(int group, int team, float d, Transform target, bool damageInArea, float projectileSpeed)
    {
        Team = team;
        Damage = d;
        Target = target;
        DamageInArea = damageInArea;
        Speed = projectileSpeed;
        init = true;
    }
}
