﻿using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Utils;

namespace Network
{
    public class NetworkManager : NetworkSingleton<NetworkManager>
    {
        [SerializeField] private byte _maxPlayersPerRoom = 4;
        [SerializeField] private TextMeshProUGUI _player0, _player1, _player2, _player3;

        private string _gameVersion = "1";

        private void Awake()
        {
            // This makes sure we can use PhotonNetwork.LoadLvel on the master client
            // and all clients in the same room sync their level automatically

            PhotonNetwork.AutomaticallySyncScene = true;
        }

        public void Login()
        {
            if (!PhotonNetwork.IsConnected)
            {
                PhotonNetwork.ConnectUsingSettings();
                PhotonNetwork.GameVersion = _gameVersion;
            }
        }

        public void QuickPlay()
        {
            if (PhotonNetwork.IsConnected)
            {
                // #Critical we need at this point to attempt joining a Random Room. If it fails, we'll get notified in OnJoinRandomFailed() and we'll create one.
                PhotonNetwork.JoinRandomRoom();
            }
            else
            {
                Debug.LogError("[Network Manager]: Not connected");
                Login();
            }
        }

        public void LoadMatch()
        {
            if (PhotonNetwork.IsMasterClient && PhotonNetwork.CurrentRoom.PlayerCount >= 1)
            {
                PhotonNetwork.CurrentRoom.IsOpen = false;

                Debug.Log("[Network Manager]: Loading match level");
                PhotonNetwork.LoadLevel(3);
            }
        }

        public void LoadTutorial()
        {
            if (PhotonNetwork.IsMasterClient && PhotonNetwork.CurrentRoom.PlayerCount >= 1)
            {
                PhotonNetwork.CurrentRoom.IsOpen = false;

                Debug.Log("[Network Manager]: Loading tutorial level");
                PhotonNetwork.LoadLevel(4);
            }
        }

        #region PUN Callbacks

        public override void OnConnectedToMaster()
        {
            Debug.Log("[Network Manager]: Connected to " + PhotonNetwork.CloudRegion + " server");
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            Debug.LogWarning("[Network Manager]: disconnected with reason " + cause);
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            Debug.Log("[Network Manager]: Join random failed. No random room available, so we create one.");

            // #Critical: we failed to join a random room, maybe none exists or they are all full. No worries, we create a new room.
            PhotonNetwork.CreateRoom(null, new RoomOptions { MaxPlayers = _maxPlayersPerRoom });
        }

        public void CreateRoom()
        {
            PhotonNetwork.CreateRoom(null, new RoomOptions { MaxPlayers = _maxPlayersPerRoom });
        }

        public override void OnJoinedRoom()
        {
            Debug.Log("[Network Manager]: Joined room " + PhotonNetwork.CurrentRoom.Name);
            UpdatePlayersName();
        }

        #endregion

        public void UpdatePlayersName()
        {
            Debug.Log("UpdatePlayersName");
            if(PhotonNetwork.PlayerList.Length>0)
                _player0.text = PhotonNetwork.PlayerList[0].NickName + " (Host)";
            else
            {
                _player0.text = "";
            }

            if (PhotonNetwork.PlayerList.Length > 1)
                _player1.text = PhotonNetwork.PlayerList[1].NickName;
            else
            {
                _player1.text = "";
            }

            if (PhotonNetwork.PlayerList.Length > 2)
                _player2.text = PhotonNetwork.PlayerList[2].NickName;
            else
                _player2.text = "";

            if (PhotonNetwork.PlayerList.Length > 3)
                _player3.text = PhotonNetwork.PlayerList[3].NickName;
            else
                _player3.text = "";

            RoomPanel.Instance.UpdatePlayersInfo();
        }
    }
}