﻿using Build;
using GameManagers;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utils;

public class GameManager : NetworkSingleton<GameManager>
{
    [SerializeField] private GameObject _queen, _distanceTroop, _meleeTroop, _airTroop, _gatherer;
    [SerializeField] private List<Transform> SpawnPositions;
    public bool debug = true;

    private int playerNumber;
    [HideInInspector] public bool Show = true;
    private bool _gameOver = false;
    AnimationEvents _events;
    private bool victory = false;

    public GameObject VictoryPanel, GameOverPanel;
    private TutorialManager tuto;

    private void Awake()
    {
        tuto = GameObject.FindObjectOfType<TutorialManager>();
        _events = GetComponent<AnimationEvents>();
    }

    private void Start()
    {
        Init();
    }

    public void GameOver()
    {
        GameOverFunction();
        ShowGameOver();
    }

    public void GameOverFunction()
    {
        _gameOver = true;
        Invoke(nameof(Disconnect), 5f);
    }

    public void ShowGameOver()
    {
        Debug.Log("GameOver");
        GameOverPanel.SetActive(true);
        AudioManager.Instance.PlayGameOverSound(); 
    }

    public int GetPlayerNumber() { return playerNumber; }

    public void CheckPlayers()
    {
        Debug.Log("PhotonNetwork.PlayerList.Length: "+PhotonNetwork.PlayerList.Length);
        if (PhotonNetwork.PlayerList.Length == 1 && !_gameOver)
        {
            ShowVictory();
        }
    }

    public void ShowVictory()
    {
        victory = true;
        Debug.Log("Victory");
        VictoryPanel.SetActive(true);
        AudioManager.Instance.PlayVictorySound();
        Invoke(nameof(Disconnect), 5f);
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            GoToQueen();
        }
/*
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.I))
        {
            Init();
        }
#endif
*/
    }

    public void Disconnect()
    {
        PhotonNetwork.Disconnect();
    }

    #region PUN Callbacks

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        Invoke(nameof(CheckPlayers), 0.3f);
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);
        SceneManager.LoadScene(2);
    }
    #endregion

    public void InstantiatePhotonView(GameObject prefab, Vector3 pos)
    {
        var PhotonObj = PhotonNetwork.Instantiate(prefab.name, pos, Quaternion.identity, 0);

        if (tuto == null)
        {
            if (PhotonObj.TryGetComponent<Selectable>(out Selectable selectable0))
                selectable0.SetGroupAndTeam(playerNumber, playerNumber);
        }
        else
        {
            if (PhotonObj.TryGetComponent<Selectable>(out Selectable selectable1))
                selectable1.SetGroupAndTeam(0, 0);
        }
    }

    public void GoToQueen()
    {
        RTSCameraController.Instance.transform.position = new Vector3(TeamManager.Instance.Queens[playerNumber].transform.position.x, RTSCameraController.Instance.transform.position.y, TeamManager.Instance.Queens[playerNumber].transform.position.z - 50f);
    }

    public Vector3 QueenPositionCamera()
    {
        return new Vector3(TeamManager.Instance.Queens[playerNumber].transform.position.x, RTSCameraController.Instance.transform.position.y, TeamManager.Instance.Queens[playerNumber].transform.position.z - 50f);
    }

    public void Init()
    {
        playerNumber = PhotonNetwork.LocalPlayer.GetPlayerNumber();
        if (playerNumber != -1)
        {
            InstantiatePhotonView(_queen, SpawnPositions[playerNumber].position);
            InstantiatePhotonView(_gatherer, SpawnPositions[playerNumber].position + new Vector3(10, 0, 0));
            InstantiatePhotonView(_gatherer, SpawnPositions[playerNumber].position + new Vector3(-10, 0, 0));
            InstantiatePhotonView(_gatherer, SpawnPositions[playerNumber].position + new Vector3(0, 0, 10));
            InstantiatePhotonView(_gatherer, SpawnPositions[playerNumber].position + new Vector3(0, 0, -10));

            if (debug)
            {
                switch (playerNumber)
                {
                    case 0:
                    case 3:
                        InstantiatePhotonView(_meleeTroop, SpawnPositions[playerNumber].position + new Vector3(0, 0, -20));
                        InstantiatePhotonView(_distanceTroop, SpawnPositions[playerNumber].position + new Vector3(10, 0, -20));
                        InstantiatePhotonView(_airTroop, SpawnPositions[playerNumber].position + new Vector3(-10, 0, -20));
                        break;
                    case 1:
                    case 2:
                        InstantiatePhotonView(_meleeTroop, SpawnPositions[playerNumber].position + new Vector3(0, 0, 20));
                        InstantiatePhotonView(_distanceTroop, SpawnPositions[playerNumber].position + new Vector3(10, 0, 20));
                        InstantiatePhotonView(_airTroop, SpawnPositions[playerNumber].position + new Vector3(-10, 0, 20));
                        break;
                }
            }
            
            if(tuto != null)
            {
                TutorialManager.Instance.Init();
                SelectorRTS.Instance.InitGroupAndTeamTuto();
                RTSCameraController.Instance.transform.position = new Vector3(SpawnPositions[0].position.x, RTSCameraController.Instance.transform.position.y, SpawnPositions[0].position.z - 50f);
                CanvasManager.Instance.InitInfoPanel();
            }
            else
            {
                CanvasManager.Instance.InitInfoPanel();
                SelectorRTS.Instance.InitGroupAndTeam();
                RTSCameraController.Instance.transform.position = new Vector3(SpawnPositions[playerNumber].position.x, RTSCameraController.Instance.transform.position.y, SpawnPositions[playerNumber].position.z - 50f);
            }
        }
        else
        {
            if (!IsInvoking(nameof(Init)))
            {
                Invoke(nameof(Init), 0.1f);  
            }
        }
    }

}
