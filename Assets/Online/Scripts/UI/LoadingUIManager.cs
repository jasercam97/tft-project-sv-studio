﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Network
{
    public class LoadingUIManager : Singleton<LoadingUIManager>
    {
        [SerializeField] private GameObject LoadingScreen;

        public void ShowLoadingScreen(bool show)
        {
            LoadingScreen.SetActive(show);
            Debug.Log("loading screen is " + show.ToString());
        }
    }
}