﻿using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Utils;

namespace Network
{
    public class RoomPanel : NetworkSingleton<RoomPanel>
    {
        [SerializeField] private TextMeshProUGUI _roomNameText;
        [SerializeField] private TextMeshProUGUI _playersCountText;

        [SerializeField] private Button _playButton;

        public void Show(bool show)
        {
            gameObject.SetActive(show);

            if (show)
            {
                Init();
            }
        }

        private void Init()
        {
            UpdateRoomName();
            UpdatePlayersInfo();
        }

        public void UpdatePlayersInfo()
        {
            UpdatePlayersCount();
            SetupPlayButton();
        }

        private void UpdatePlayersCount()
        {
            Debug.Log("UpdatePlayersCount");
            int playersCount = PhotonNetwork.CurrentRoom.PlayerCount;
            _playersCountText.text = "Players: " + playersCount.ToString()+ "/4";
        }

        private void SetupPlayButton()
        {
            _playButton.interactable = (PhotonNetwork.IsMasterClient && PhotonNetwork.CurrentRoom.PlayerCount >= 1);
        }

        private void UpdateRoomName()
        {
            _roomNameText.text = "BUGS ZONE";
        }

        public void OnPlayButtonPressed()
        {
            NetworkManager.Instance.LoadMatch();
        }

        #region PUN Callbacks

        public override void OnPlayerEnteredRoom(Player otherPlayer)
        {
            Debug.Log("Player " + otherPlayer.NickName + " joined");
            UpdatePlayersInfo();
            NetworkManager.Instance.UpdatePlayersName();
        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            Debug.Log("Player " + otherPlayer.NickName + " left");
            UpdatePlayersInfo();
            NetworkManager.Instance.UpdatePlayersName();
        }

        public override void OnMasterClientSwitched(Player newMasterClient)
        {
            Debug.Log(newMasterClient + " is the new host");
            SetupPlayButton();
        }

        public void Disconnect()
        {
            PhotonNetwork.Disconnect();
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            base.OnDisconnected(cause);
            SceneManager.LoadScene(2);
        }

        #endregion
    }
}