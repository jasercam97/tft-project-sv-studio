﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Utils;

namespace Network
{
    public class PlayerNameInputField : Singleton<PlayerNameInputField>
    {
        private TMP_InputField _inputField;

        private void Awake()
        {
            _inputField = GetComponent<TMP_InputField>();
        }

        private void Start()
        {
            LoadLastPlayerName();
        }

        private void LoadLastPlayerName()
        {
            string playerName = LoadPlayerName();

            PhotonNetwork.NickName = playerName;
            _inputField.text = playerName;
        }

        private void SaveCurrentName(string name)
        {
            PlayerPrefs.SetString("PlayerName", name);
            PlayerPrefs.Save();
        }

        public string LoadPlayerName()
        {
            return PlayerPrefs.GetString("PlayerName", "");
        }

        public void SetPlayerName(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                PhotonNetwork.NickName = name;
                SaveCurrentName(name);
            }
            else
            {
                Debug.LogError("Player name can't be null or empty");
            }
        }
    }
}