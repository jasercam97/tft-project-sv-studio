using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InformationManager : MonoBehaviour
{
    public GameObject informationButton, quitInformation, information;

    public void ShowInformation()
    {
        informationButton.SetActive(!informationButton.activeInHierarchy);
        quitInformation.SetActive(!quitInformation.activeInHierarchy);
        information.SetActive(!information.activeInHierarchy);
    }
}
