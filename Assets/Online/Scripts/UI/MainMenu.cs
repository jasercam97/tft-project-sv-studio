﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Network {
    public class MainMenu : MonoBehaviourPunCallbacks
    {
        [SerializeField] private GameObject _loginPanel;
        [SerializeField] private GameObject _lobbyPanel;
        [SerializeField] private GameObject _creditsPanel, _information, _quitInformation;

        [SerializeField] private Button _quickPlayButton;
        [SerializeField] private RoomPanel _roomPanel;

        private void Awake()
        {
           // ShowLogin(true);
            //ShowLobby(false);
        }

        private void ShowLogin(bool show)
        {
            _loginPanel.SetActive(show);
        }

        private void ShowLobby(bool show)
        {
            _lobbyPanel.SetActive(show);
        }

        private void ShowQuickButton(bool show)
        {
            _quickPlayButton.gameObject.SetActive(show);
        }

        private void ShowRoom(bool show)
        {
            _roomPanel.Show(show);
            ShowQuickButton(!show);
        }

        #region Buttons

        public void LoginButtonPressed()
        {
            ShowLogin(false);
            LoadingUIManager.Instance.ShowLoadingScreen(true);
            NetworkManager.Instance.Login();
        }

        public void QuickPlayButtonPressed()
        {
            ShowQuickButton(false);
            LoadingUIManager.Instance.ShowLoadingScreen(true);
            NetworkManager.Instance.QuickPlay();
        }

        #endregion

        #region PUN Callbacks

        public override void OnConnectedToMaster()
        {
            ShowLobby(true);
            ShowLogin(false);
            LoadingUIManager.Instance.ShowLoadingScreen(false);
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            ShowLobby(false);
            ShowLogin(true);
        }

        public override void OnJoinedRoom()
        {
            LoadingUIManager.Instance.ShowLoadingScreen(false);
            ShowRoom(true);
        }

        public override void OnLeftRoom()
        {
            ShowRoom(false);
        }

        #endregion

        public void Exit()
        {
            Application.Quit();
        }

        public void ShowCredits()
        {
            _creditsPanel.SetActive(!_creditsPanel.activeInHierarchy);
            _information.SetActive(!_information.activeInHierarchy);
            _quitInformation.SetActive(!_quitInformation.activeInHierarchy); 
        }
    }
}