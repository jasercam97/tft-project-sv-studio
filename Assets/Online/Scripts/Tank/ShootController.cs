﻿using Photon.Pun;
using UnityEngine;

public class ShootController : MonoBehaviour
{
    #region Inspector properties

    [SerializeField] private Transform _projectileSpawn;
    [SerializeField] private GameObject _projectilePrefab;

    #endregion

    public void Fire(int group, int team, float damage, Transform target, bool damageInArea, float projectileSpeed)
    {
        var bullet = PhotonNetwork.Instantiate(_projectilePrefab.name, _projectileSpawn.position, _projectileSpawn.rotation, 0);
        bullet.GetComponent<ProjectileReflection>().Init(group, team, damage, target, damageInArea, projectileSpeed);
    }
}
