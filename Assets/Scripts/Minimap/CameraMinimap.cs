/*
 * Jose Manuel Herrera Vera 
 * @projects_ia (Twitter)
 * https://josemhv.itch.io
 *
 * Camera Minimap v1.0
 * 
 * Date: 2021/08/21
 */
using UnityEngine;
using UnityEngine.Rendering;

public class CameraMinimap : MonoBehaviour
{
    public Material cameraBoxMaterial;

    public Color colorCamera;

    public Camera minimap;

    public float lineWidth;

    public Collider mapCollider;

    private float _depth = 0.25f;

    private Vector3 GetCameraFrustumPoint(Vector3 position)
    {
        var positionRay = Camera.main.ScreenPointToRay(position);
        RaycastHit hit;
        Vector3 result = mapCollider.Raycast(positionRay, out hit, Mathf.Infinity) ? hit.point : new Vector3();

        return result;
    }

    private void OnEnable()
    {
        RenderPipelineManager.endCameraRendering += RenderPipelineManager_endCameraRendering;
    }

    private void OnDisable()
    {
        RenderPipelineManager.endCameraRendering -= RenderPipelineManager_endCameraRendering;
    }

    private void RenderPipelineManager_endCameraRendering(ScriptableRenderContext context, Camera camera)
    {
        if (camera == minimap)
        {
            OnPostRender();
        }
    }
    
    public void OnPostRender()
    {
        Vector3 minViewportPoint = minimap.WorldToViewportPoint(GetCameraFrustumPoint(new Vector3(0f, 0f)));
        Vector3 maxViewportPoint = minimap.WorldToViewportPoint(GetCameraFrustumPoint(new Vector3(Screen.width, Screen.height)));

        float minX = minViewportPoint.x;
        float minY = minViewportPoint.y;

        float maxX = maxViewportPoint.x;
        float maxY = maxViewportPoint.y;

        GL.PushMatrix();
        {
            cameraBoxMaterial.SetPass(0);
            GL.LoadOrtho();

            GL.Begin(GL.QUADS);
            GL.Color(colorCamera);
            {

                GL.Vertex(new Vector3(minX, minY + lineWidth, _depth));
                GL.Vertex(new Vector3(minX, minY - lineWidth, _depth));
                GL.Vertex(new Vector3(maxX, minY - lineWidth, _depth));
                GL.Vertex(new Vector3(maxX, minY + lineWidth, _depth));


                GL.Vertex(new Vector3(minX + lineWidth, minY, _depth));
                GL.Vertex(new Vector3(minX - lineWidth, minY, _depth));
                GL.Vertex(new Vector3(minX - lineWidth, maxY, _depth));
                GL.Vertex(new Vector3(minX + lineWidth, maxY, _depth));



                GL.Vertex(new Vector3(minX, maxY + lineWidth, _depth));
                GL.Vertex(new Vector3(minX, maxY - lineWidth, _depth));
                GL.Vertex(new Vector3(maxX, maxY - lineWidth, _depth));
                GL.Vertex(new Vector3(maxX, maxY + lineWidth, _depth));

                GL.Vertex(new Vector3(maxX + lineWidth, minY, _depth));
                GL.Vertex(new Vector3(maxX - lineWidth, minY, _depth));
                GL.Vertex(new Vector3(maxX - lineWidth, maxY, _depth));
                GL.Vertex(new Vector3(maxX + lineWidth, maxY, _depth));

            }
            GL.End();
        }
        GL.PopMatrix();
    }
}
