﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Utils;

public class MinimapMovement : Singleton<MinimapMovement>, IPointerDownHandler, IDragHandler
{
    public RawImage Minimap;

    [Header("Map variables")]
    public Transform LimitUp;
    public Transform LimitDown;
    public Transform LimitRight;
    public Transform LimitLeft;

    private Vector3 _originPosition;
    private float _mapWidth;
    private float _mapHeight;
    private Vector2 _localCursor;

    private void Start()
    {
        _originPosition = new Vector3(LimitLeft.position.x, Camera.main.transform.position.y, LimitDown.position.z);
        _mapWidth = LimitRight.position.x - LimitLeft.position.x;
        _mapHeight = LimitUp.position.z - LimitDown.position.z;
    }

    private Vector3 CheckMousePosition()
    {
        Vector3 posInImage = (Vector3) _localCursor - Minimap.rectTransform.position;
        float imageWidth = Minimap.rectTransform.rect.width;
        float imageHeight = Minimap.rectTransform.rect.height;

        Vector3 normalizedPos = new Vector3(posInImage.x / imageWidth,
                                            posInImage.y / imageHeight);

        return normalizedPos;
    }

    private Vector3 CalculateWorldPosition()
    {
        Vector3 minimapPosition = CheckMousePosition();
        Vector3 targetPosition = new Vector3(_originPosition.x + _mapWidth * minimapPosition.x,
                                             _originPosition.y,
                                             _originPosition.z + _mapHeight * minimapPosition.y - 40f);

        return targetPosition;
    }

    public void MoveCamera()
    {
        RTSCameraController.Instance.MoveToPosition(CalculateWorldPosition());
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        RectTransformUtility.ScreenPointToLocalPointInRectangle(Minimap.rectTransform, eventData.position, eventData.pressEventCamera, out _localCursor);

        if (eventData.button == PointerEventData.InputButton.Left)
        {
            RTSCameraController.Instance.MoveToPosition(CalculateWorldPosition());
        }
        else if (eventData.button == PointerEventData.InputButton.Right)
        {
            AudioManager.Instance.PlayMoveCommandSound();
            SelectorRTS.Instance.DoMoveInMinimap(CalculateWorldPosition());
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        Debug.Log("Drag");
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(Minimap.rectTransform, eventData.position, eventData.pressEventCamera, out _localCursor);
            RTSCameraController.Instance.MoveToPosition(CalculateWorldPosition());
        }
    }
}
