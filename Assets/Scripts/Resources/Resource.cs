﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class Resource : MonoBehaviourPunCallbacks, IPunObservable
{
    public enum ResourcesType {Biomass = 0, Energy = 1, Food =2}

    public List<Sprite> sprites;
    public GameObject Render;
    private int _currentSprite;
    private PhotonView _photonView;

    //Resource Panel
    public string Name;
    public string Description;

    public ResourcesType _resourcesType;

    [HideInInspector]
    public int _type;

    public float _initialValue;
    public float _currentValue;

    public Transform _nextResource = null;

    [HideInInspector]
    public float _order;

    public List<GameObject> GathererFocus = new List<GameObject>();

    public float Value 
    {
        get { return _currentValue; }  
        set {
            _currentValue -= value;
            if (_currentValue <= 0)
            {
                GetComponentInParent<Area>().resourcesAvailable.Remove(transform);
                GetComponentInParent<Area>().UpdateResourcesAvaibles();
                for(int i = 0; i < GathererFocus.Count; i++)
                {
                    if (_nextResource != null)
                    {
                        GathererFocus[i].GetComponent<Gatherer>()._currentResourceTarget = _nextResource;
                        _nextResource.GetComponent<Resource>().GathererFocus = GathererFocus;
                    }
                }
                photonView.RPC(nameof(RPC_ResourceDie), RpcTarget.AllViaServer);
            }
            else
            {
                var newSprite = Mathf.CeilToInt((_currentValue / _initialValue) * sprites.Count) - 1;
                if (_currentSprite != newSprite)
                    _photonView.RPC(nameof(RPC_SpriteChange), RpcTarget.AllViaServer, newSprite);
            }
        }  
    }

    [PunRPC]
    public void RPC_ResourceDie()
    {
        if (photonView.IsMine)
            PhotonNetwork.Destroy(gameObject);
    }

    private void Awake()
    {
        _order = transform.position.x + transform.position.z;
        _currentValue = _initialValue;
        if (_resourcesType == ResourcesType.Biomass)
            _type = 0;
        else if (_resourcesType == ResourcesType.Energy)
            _type = 1;
        else
            _type = 2;

        _photonView = GetComponent<PhotonView>();
        _currentSprite = sprites.Count;
    }

    [PunRPC]
    public void RPC_SpriteChange(int newSprite)
    {
        Render.GetComponent<SpriteRenderer>().sprite = sprites[newSprite];
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            // We own this player: send the others our data
            stream.SendNext(_currentValue);
        }
        else
        {
            // Network player, receive data
            this._currentValue = (float)stream.ReceiveNext();
        }
    }
}
