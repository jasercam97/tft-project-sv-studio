﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class Area : MonoBehaviourPunCallbacks
{
    public List<Transform> resourcesAvailable = new List<Transform>();

    private void Awake()
    {
        resourcesAvailable = new List<Transform>(transform.childCount);
        for(int i=0; i<transform.childCount; i++)
        {
            resourcesAvailable.Add(transform.GetChild(i));
        }
    }

    private void Start()
    {
        resourcesAvailable.Sort((p1, p2) => p1.GetComponent<Resource>()._order.CompareTo(p2.GetComponent<Resource>()._order));

        for (int i = 0; i < transform.childCount - 1; i++)
        {
            resourcesAvailable[i].gameObject.GetComponent<Resource>()._nextResource = resourcesAvailable[i + 1];
        }
        if(resourcesAvailable.Count>1)
            resourcesAvailable[resourcesAvailable.Count - 1].gameObject.GetComponent<Resource>()._nextResource = resourcesAvailable[resourcesAvailable.Count - 2];
    }

    public void UpdateResourcesAvaibles()
    {
        resourcesAvailable.Sort((p1, p2) => p1.GetComponent<Resource>()._order.CompareTo(p2.GetComponent<Resource>()._order));

        for (int i = 0; i < resourcesAvailable.Count - 1; i++)
        {
            resourcesAvailable[i].gameObject.GetComponent<Resource>()._nextResource = resourcesAvailable[i + 1];
        }

        if(resourcesAvailable.Count == 1)
            resourcesAvailable[resourcesAvailable.Count - 1].gameObject.GetComponent<Resource>()._nextResource = null;
        else if(resourcesAvailable.Count > 1)
            resourcesAvailable[resourcesAvailable.Count - 1].gameObject.GetComponent<Resource>()._nextResource = resourcesAvailable[resourcesAvailable.Count - 2];

        if (resourcesAvailable.Count <= 0)
            photonView.RPC(nameof(RPC_AreaDie), RpcTarget.AllViaServer);
    }

    [PunRPC]
    public void RPC_AreaDie()
    {
        if (photonView.IsMine)
            PhotonNetwork.Destroy(gameObject);
    }
}
