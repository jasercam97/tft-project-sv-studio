﻿using Build;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace GameManagers
{
    public class BuildManager : Singleton<BuildManager>
    {

        [Header("Blueprint Colors")]
        public Color BuildableColor;
        public Color UnbuildableColor;

        private Transform _buildParent;

        private BuildBehaviour _selectedBuild;

        private bool _newBuildSelected;

        private void Awake()
        {
            _buildParent = ParentManager.Instance.BuildParent;
        }

        #region BUILD_COLLIDERS
        void ShowBuildColliders()
        {
            foreach (Transform child in _buildParent)
            {
                child.GetComponent<LineRenderer>().enabled = true;
            }
        }

        void HideBuildColliders()
        {
            foreach (Transform child in _buildParent)
            {
                if (child.GetComponent<BuildBehaviour>().BuildType != BuildBehaviour.TypeOfBuild.Turret)
                {
                    child.GetComponent<LineRenderer>().enabled = false;
                }
            }
        }
        #endregion

        #region BLUEPRINT_COLOR
        public Color GetBuildableColor()
        {
            return BuildableColor;
        }

        public Color GetUnbuildableColor()
        {
            return UnbuildableColor;
        }
        #endregion

        #region BUILD_BUTTON
        public void SelectNewBuild()
        {
            _newBuildSelected = true;

            ShowBuildColliders();
        }

        public void DeselectNewBuild()
        {
            _newBuildSelected = false;

            HideBuildColliders();
        }

        public bool CanSelectNewBuild()
        {
            return !_newBuildSelected;
        }
        #endregion

        #region SELECT_BUILD

        public void SetSelectedBuild(BuildBehaviour selected)
        {
            if (_selectedBuild) { DeselectBuild(); }
            _selectedBuild = selected;
            _selectedBuild.SelectBuild(true);
        }

        public void DeselectBuild()
        {
            if (_selectedBuild) { _selectedBuild.SelectBuild(false); }
            _selectedBuild = null;
        }

        #endregion
    }
}