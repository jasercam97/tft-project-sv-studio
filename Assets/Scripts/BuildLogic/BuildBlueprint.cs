﻿using System;
using GameManagers;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Build
{
    public class BuildBlueprint : MonoBehaviour
    {
        public GameObject Prefab;
        
        public LayerMask HittableMask;
        public LayerMask PlaceableLayer;

        [Header("Collider Size")]
        public float XSize;
        public float YSize;

        public float MaxDistanceToQueen;

        private RaycastHit _hit;

        private BoxCollider _collider;
     
        private bool _canBuild;
        private bool _collidingBuild;

        private LineRenderer _lineRenderer;
        private List<Vector3> _linePoints;

        private SpriteRenderer _renderer;

        private KeyCode _placeBuild;
        private KeyCode _cancelBuild;

        private float _foodCost;
        private float _biomassCost;
        private float _energyCost;

        private GameObject _queen;

        private QueenRange _queenRange;

        private void Awake()
        {
            _lineRenderer = GetComponent<LineRenderer>();

            _renderer = GetComponentInChildren<SpriteRenderer>();

            _collider = GetComponent<BoxCollider>();
            _collider.size = new Vector3(XSize * 2, YSize * 2, 2f);

            _queen = TeamManager.Instance.Queens[GameManager.Instance.GetPlayerNumber()].gameObject;

            _queenRange = _queen.GetComponentInChildren<QueenRange>();

            InitializeControls();            
        }

        private void Start()
        {
            CheckPlacePosition();
            DrawCollider(_lineRenderer);
            ShowQueenRange();
        }

        private void Update()
        {
            CheckPlacePosition();

            PlaceBuild();
        }

        void InitializeControls()
        {
            _placeBuild = InputManager.Instance.PlaceBuild;
            _cancelBuild = InputManager.Instance.CancelBuild;
        }

        public void SetBuildCosts(float food, float biomass, float energy)
        {
            _foodCost = food;
            _biomassCost = biomass;
            _energyCost = energy;
        }

        void CheckPlacePosition()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            Debug.Log(Vector3.Distance(transform.position, _queen.transform.position));
            
            if (Physics.Raycast(ray, out _hit, Mathf.Infinity, HittableMask))
            {              
                if(((1 << _hit.transform.gameObject.layer) & PlaceableLayer) != 0 && !_collidingBuild && _queenRange.canBuild)//&& Vector3.Distance(transform.position, _queen.transform.position) < MaxDistanceToQueen) 
                {
                    _canBuild = true;

                    CheckColor(BuildManager.Instance.GetBuildableColor());
                }
                else 
                {
                    _canBuild = false;

                    CheckColor(BuildManager.Instance.GetUnbuildableColor());
                }

                transform.position = _hit.point + new Vector3(0, 1.5f);
            }
        }

        void PlaceBuild()
        {
            if (Input.GetKeyDown(_placeBuild) && _canBuild)
            {
                int playerNumber = PhotonNetwork.LocalPlayer.GetPlayerNumber(); 
                GameObject GO = PhotonNetwork.Instantiate(Prefab.name, transform.position, transform.rotation, 0);
                if (GO.TryGetComponent<Selectable>(out Selectable selectable0)) selectable0.SetGroupAndTeam(playerNumber, playerNumber);

                BoxCollider coll = GO.GetComponent<BoxCollider>();
                coll.size = new Vector3(XSize * 2, YSize * 2, 2f);

                DrawCollider(GO.GetComponent<LineRenderer>());

                ParentManager.Instance.SetBuildParent(GO);
                BuildManager.Instance.DeselectNewBuild();
                ResourceManager.Instance.Food -= _foodCost;
                ResourceManager.Instance.Biomass -= _biomassCost;
                ResourceManager.Instance.Energy -= _energyCost;

                Destroy(gameObject);
            }
            else if (Input.GetKeyDown(_cancelBuild))
            {
                BuildManager.Instance.DeselectNewBuild();
                Destroy(gameObject);
            }
        }

        void DrawCollider(LineRenderer line)
        {
            line.startWidth = 0.1f;
            line.endWidth = 0.1f;
            line.loop = true;

            line.SetPosition(0, new Vector3(XSize, YSize, 0));
            line.SetPosition(1, new Vector3(XSize, -YSize, 0));
            line.SetPosition(2, new Vector3(-XSize, -YSize, 0));
            line.SetPosition(3, new Vector3(-XSize, YSize, 0));
        }

        void CheckColor(Color color)
        {
            _renderer.color = color;

            _lineRenderer.startColor = color;
            _lineRenderer.endColor = color;
        }

        void ShowQueenRange()
        {
            _queen.GetComponentInChildren<QueenRange>().ShowRange(true);
        }

        private void OnTriggerStay(Collider other)
        {
            if (other.CompareTag("Build") || other.CompareTag("Troop") || other.CompareTag("Queen") || other.CompareTag("Resources")) { _collidingBuild = true; }
        }
        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Build") || other.CompareTag("Troop") || other.CompareTag("Queen") || other.CompareTag("Resources")) { _collidingBuild = false; }
        }

        #region GIZMOS


        private void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;

            Gizmos.DrawLine(transform.position + new Vector3(XSize,0, YSize), transform.position + new Vector3(-XSize,0,YSize));
            Gizmos.DrawLine(transform.position + new Vector3(-XSize,0,-YSize), transform.position + new Vector3(-XSize,0, YSize));
            Gizmos.DrawLine(transform.position + new Vector3(-XSize,0,-YSize), transform.position + new Vector3(XSize,0, -YSize));
            Gizmos.DrawLine(transform.position + new Vector3(XSize,0, YSize), transform.position + new Vector3(XSize,0,-YSize));
        }

        #endregion

        private void OnDestroy()
        {
            _queen.GetComponentInChildren<QueenRange>().ShowRange(false);
        }
    }
}