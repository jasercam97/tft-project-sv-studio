﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WormEvents : MonoBehaviour
{
    public GameObject Hole;

    public void ShowWorm() { gameObject.SetActive(true); }

    public void HideWorm() { gameObject.SetActive(false); }
    public void ShowHole() { Hole.SetActive(true); }

    public void HideHole() { Hole.SetActive(false); }


}
