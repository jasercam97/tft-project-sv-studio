﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameManagers;
using System;
using Photon.Pun;

namespace Build
{
    public class BuildUpgrades : MonoBehaviourPunCallbacks
    {
        [System.Serializable]
        public class TimeUp
        {
            public int BiomassCost;
            public int EnergyCost;
            public float UpgradeTime;

            [Header("Improvement")]
            public float NewEvolveTime;
        }

        [System.Serializable]
        public class TroopUp
        {
            public int BiomassCost;
            public int EnergyCost;
            public float UpgradeTime;

            [Header("Improvement")]
            public float NewEvolveTime;
        }

        [System.Serializable]
        public class TurretHealthUp
        {
            public int BiomassCost;
            public int EnergyCost;
            public float UpgradeTime;

            [Header("Improvement")]
            public int NewHealth;
        }

        [System.Serializable]
        public class TurretDamageUp
        {
            public int BiomassCost;
            public int EnergyCost;
            public float UpgradeTime;

            [Header("Improvement")]
            public int NewDamage;
        }

        public List<TimeUp> TimeUpgrade;
        public List<TroopUp> TroopUpgrade;
        public List<TurretHealthUp> TurretHealthUpgrade;
        public List<TurretDamageUp> TurretDamageUpgrade;

        public GameObject TimeBarCanvas;
        public Image FillBar;

        public GameObject upgradeParticles0;
        public GameObject upgradeParticles1;
        
        private TroopEvolver _evolver;
        private BuildBehaviour _build;
        private Turret _turret;
        
        private bool _upgradingTime = false;
        private bool _upgradingTroop = false;
        [SerializeField] private bool _timeUpgraded = false;
        [SerializeField] private bool _troopUpgraded = false;

        private bool _upgradingHealth = false;
        private bool _upgradingDamage = false;
        private bool _healthUpgraded = false;
        private bool _damageUpgraded = false;

        private float _upgradeTimer;
        private float _timer;

        private int _upgradeIndex;

        private AtributteManager _attributeManager;

        private void Awake()
        {
            _evolver = GetComponent<TroopEvolver>();
            _build = GetComponent<BuildBehaviour>();
            _turret = GetComponent<Turret>();
            _attributeManager = GetComponent<AtributteManager>();
        }

        private void Update()
        {
            if (_upgradingTime)
            {
                _timer += Time.deltaTime;

                FillUpgradeBar();

                if (_timer >= _upgradeTimer)
                {
                    UpgradeTime(_upgradeIndex);
                }
            }

            if (_upgradingTroop)
            {
                _timer += Time.deltaTime;

                FillUpgradeBar();

                if (_timer >= _upgradeTimer)
                {
                    UpgradeTroop(_upgradeIndex);
                }
            }

            if (_upgradingHealth)
            {
                _timer += Time.deltaTime;

                FillUpgradeBar();

                if (_timer >= _upgradeTimer)
                {
                    UpgradeTurretHealth(_upgradeIndex);
                }
            }

            if (_upgradingDamage)
            {
                _timer += Time.deltaTime;

                FillUpgradeBar();

                if (_timer >= _upgradeTimer)
                {
                    UpgradeTurretDamage(_upgradeIndex);
                }
            }
        }

        private void FillUpgradeBar()
        {
            FillBar.fillAmount = _timer / _upgradeTimer;
        }

        public void OnTimeButtonPressed(int i) 
        {
            AudioManager.Instance.PlayBuildingUpgradedSound();

            ResourceManager.Instance.Biomass -= TimeUpgrade[i].BiomassCost;
            ResourceManager.Instance.Energy -= TimeUpgrade[i].EnergyCost;

            _upgradeTimer = TimeUpgrade[i].UpgradeTime;

            _upgradeIndex++;

            _upgradingTime = true;
            _timeUpgraded = true;

            _attributeManager.ShowLifebar(false);
            TimeBarCanvas.SetActive(true);
        }

        public void OnTroopButtonPressed(int i)
        {
            AudioManager.Instance.PlayBuildingUpgradedSound();

            ResourceManager.Instance.Biomass -= TroopUpgrade[i].BiomassCost;
            ResourceManager.Instance.Energy -= TroopUpgrade[i].EnergyCost;

            _upgradeTimer = TroopUpgrade[i].UpgradeTime;

            _upgradeIndex++;

            _upgradingTroop = true;
            _troopUpgraded = true;

            _attributeManager.ShowLifebar(false);
            TimeBarCanvas.SetActive(true);

        }

        public void OnTurretHealthButtonPressed(int i)
        {
            AudioManager.Instance.PlayBuildingUpgradedSound();

            ResourceManager.Instance.Biomass -= TurretHealthUpgrade[i].BiomassCost;
            ResourceManager.Instance.Energy -= TurretHealthUpgrade[i].EnergyCost;

            _upgradeTimer = TurretHealthUpgrade[i].UpgradeTime;

            _upgradeIndex++;

            _upgradingHealth = true;
            _healthUpgraded = true;

            _attributeManager.ShowLifebar(false);
            TimeBarCanvas.SetActive(true);
        }

        public void OnTurretDamageButtonPressed(int i)
        {
            AudioManager.Instance.PlayBuildingUpgradedSound();

            ResourceManager.Instance.Biomass -= TurretDamageUpgrade[i].BiomassCost;
            ResourceManager.Instance.Energy -= TurretDamageUpgrade[i].EnergyCost;

            _upgradeTimer = TurretDamageUpgrade[i].UpgradeTime;

            _upgradeIndex++;

            _upgradingDamage = true;
            _damageUpgraded = true;

            _attributeManager.ShowLifebar(false);
            TimeBarCanvas.SetActive(true);
        }

        private void UpgradeTroop(int i)
        {
            _evolver.AreTroopsUpgraded();

            _upgradingTroop = false;

            TimeBarCanvas.SetActive(false);
            _attributeManager.ShowLifebar(true);

            photonView.RPC(nameof(RPC_BuildUpgradeParticle0), RpcTarget.AllViaServer);
            
        }

        private void UpgradeTime(int i)
        {
            _evolver.IsTimeToEvolvedUpgraded();

            _upgradingTime = false;            

            TimeBarCanvas.SetActive(false);
            _attributeManager.ShowLifebar(true);

            photonView.RPC(nameof(RPC_BuildUpgradeParticle1), RpcTarget.AllViaServer);
        }

        private void UpgradeTurretHealth(int i)
        {
            _build.SetNewHealth(TurretHealthUpgrade[i - 1].NewHealth);

            _upgradingHealth = false;

            TimeBarCanvas.SetActive(false);
            _attributeManager.ShowLifebar(true);

            photonView.RPC(nameof(RPC_BuildUpgradeParticle1), RpcTarget.AllViaServer);
        }

        private void UpgradeTurretDamage(int i)
        {
            _turret.SetNewDamage(TurretDamageUpgrade[i - 1].NewDamage);

            _upgradingDamage = false;

            TimeBarCanvas.SetActive(false);
            _attributeManager.ShowLifebar(true);
            
            photonView.RPC(nameof(RPC_BuildUpgradeParticle0), RpcTarget.AllViaServer);
        }

        public bool IsUpgraded()
        {
            if(_timeUpgraded || _troopUpgraded || _damageUpgraded || _healthUpgraded) { return true; }
            return false;
        }

        public bool GetTypeOfBuildUpgrade()
        {
            if (_timeUpgraded) { return true; }
            return false;
        }

        public bool GetTypeOfTurretUpgrade()
        {
            if (_healthUpgraded) { return true; }
            return false;
        }

        public int GetUpgradeIndex()
        {
            return _upgradeIndex;
        }

        [PunRPC]
        public void RPC_BuildUpgradeParticle0()
        {
            upgradeParticles0.SetActive(true);
        }

        [PunRPC]
        public void RPC_BuildUpgradeParticle1()
        {
            upgradeParticles1.SetActive(true);
        }
    }
}
