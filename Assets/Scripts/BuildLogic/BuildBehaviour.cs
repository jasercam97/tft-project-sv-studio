﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameManagers;
using UnityEngine.UI;
using System;

namespace Build
{
    public class BuildBehaviour : MonoBehaviour
    {
        public enum TypeOfBuild
        {
            Default,
            Anthill,
            Flyhome,
            Slughome,
            Turret
        };

        public TypeOfBuild BuildType;

        AnimationEvents _events;
        private bool eventConstruction = false;

        public LayerMask HitLayer;

        public List<Sprite> ListOfSprites;
        public List<Sprite> ListOfSpritesRed;
        public List<Sprite> ListOfSpritesBlue;
        public List<Sprite> ListOfSpritesYellow;
        private int _spriteIndex;
        private float _nextChangeTime;

        public float BuildTime;
        public Image FillBar;

        public Animator WormAnimator;

        private KeyCode _selectBuild;
        private KeyCode _deselectBuild;

        [HideInInspector] public bool _isBuilt;
        private float _buildTimer;

        private SpriteRenderer _buildSprite;

        public GameObject Canvas; //Esto hay que mejorarlo cuando se creen los sprites de la UI.

        private GameObject _settingsCanvas;
        private GameObject _buildCanvas;
        private GameObject _buildUpgradesCanvas;
        private GameObject _turretUpgradesCanvas;
        private GameObject _actionsCanvas;

        private bool _isSelected; //Usar para cambiar datos del canvas en Update si está seleccionado.
        public GameObject _targetObject;
        [SerializeField] private LayerMask _mask;
        private LineRenderer line;

        private float _currentHealth;
        private float _maxHealth;

        private float _foodCost;
        private float _biomassCost;
        private float _energyCost;

        private int _foodRepairCost;
        private int _biomassRepairCost;

        private float _totalFoodCost;
        private float _totalBiomassCost;
        private float _totalEnergyCost;

        private bool _isRepairing = false;
        private float _repairTotalTime;

        private AtributteManager _atributteManager;
        public int Group;

        private void Awake()
        {
            //InitInputs();

            _events = GetComponent<AnimationEvents>();
            line = _targetObject.GetComponent<LineRenderer>();
            line.useWorldSpace = true;
            _targetObject.SetActive(false);

            InitBuildType();

            _atributteManager = GetComponent<AtributteManager>();

            _buildSprite = transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>();

            _isBuilt = false;

            CalculateTimeToChangeSprite();

            SetBuildCosts();
        }

        private void Start()
        {
            _atributteManager.ShowLifebar(false);
        }

        void InitBuildType()
        {
            _buildCanvas = CanvasManager.Instance.BuildPanel;
            _buildUpgradesCanvas = CanvasManager.Instance.BuildUpgradesPanel;
            _turretUpgradesCanvas = CanvasManager.Instance.TurretUpgradesPanel;
            _actionsCanvas = CanvasManager.Instance.ActionsPanel;

            switch (BuildType)
            {
                case TypeOfBuild.Anthill:
                    _settingsCanvas = CanvasManager.Instance.AnthillPanel;
                    break;

                case TypeOfBuild.Flyhome:
                    _settingsCanvas = CanvasManager.Instance.FlyhomePanel;
                    break;

                case TypeOfBuild.Slughome:
                    _settingsCanvas = CanvasManager.Instance.SlughomePanel;
                    break;

                case TypeOfBuild.Turret:
                    _settingsCanvas = CanvasManager.Instance.TurretPanel;
                    break;

                default:
                    Debug.LogError("No canvas for this build");
                    break;
            }
        }

        public void SetGroup(int playerNumber)
        {
            Group = playerNumber;
            switch (Group)
            {
                case 0:
                    _buildSprite.sprite = ListOfSprites[0];
                    break;
                case 1:
                    _buildSprite.sprite = ListOfSpritesRed[0];
                    break;
                case 2:
                    _buildSprite.sprite = ListOfSpritesBlue[0];
                    break;
                case 3:
                    _buildSprite.sprite = ListOfSpritesYellow[0];
                    break;
            }
        }

        void InitInputs()
        {
            _selectBuild = InputManager.Instance.SelectBuild;
            _deselectBuild = InputManager.Instance.DeselectBuild;
        }

        public void SetBuildCosts()
        {
            _foodCost = _atributteManager._costNormalFood;
            _biomassCost = _atributteManager._costNormalBiomass;
            _energyCost = _atributteManager._costNormalEnergy;

            _totalFoodCost += _foodCost;
            _totalBiomassCost += _biomassCost;
            _totalEnergyCost += _energyCost;
        }

        /*private void OnMouseDown()
        {
            if (GetComponent<Selectable>().IsOwned())
            {
                OpenBuildSettings();
            }
        }*/

        private void Update()
        {
            if (!_isBuilt)
            {
                if (!eventConstruction)
                {
                    AudioManager.Instance.PlayBuildingConstructionSound();
                    eventConstruction = true;
                }
                _buildTimer += Time.deltaTime;

                FillBuildBar();

                if (_buildTimer >= BuildTime)
                {
                    _isBuilt = true;
                    Canvas.SetActive(false);
                    _atributteManager.ShowLifebar(GameManager.Instance.Show);
                    ChangeBuildSprite();
                    WormAnimator.Play("WormEnd");

                    if (BuildType == TypeOfBuild.Turret) { GetComponent<Turret>().enabled = true; }
                }

                if (_buildTimer >= _nextChangeTime) { ChangeBuildSprite(); }
            }
            else if (_isSelected && Input.GetMouseButtonDown(1))
            {
                var screenRay = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(screenRay, out hit, Mathf.Infinity, _mask))
                {
                    _targetObject.transform.position = hit.point;
                    line.SetPosition(0, GetComponent<TroopEvolver>().SpawnPoint.position);
                    line.SetPosition(1, _targetObject.transform.position); //new Vector3(hit.point.x, 0, hit.point.z));
                }
            }

            /*if (Input.GetMouseButtonDown(0) && GetComponent<Selectable>().IsOwned()) 
            {
                RaycastHit hit;
                Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, HitLayer);
                if (hit.collider.CompareTag("Build"))
                {
                    if(hit.collider.gameObject == this.gameObject) { OpenBuildSettings(); }
                }
                else
                {
                    if (!UIClicks.IsPointerOverUIElement())
                    {
                        CanvasManager.Instance.CloseBuildInfoPanel();
                    }
                }
            }*/


            if (_isRepairing)
            {
                _buildTimer += Time.deltaTime;

                FillRepairBar();

                if(_buildTimer >= _repairTotalTime)
                {
                    _atributteManager._currentHealth = _atributteManager._maxHealth;

                    SetHealthCanvas();

                    _isRepairing = false;
                    Canvas.SetActive(false);
                }
            }
        }

        void ChangeBuildSprite()
        {
            switch (Group)
            {
                case 0:
                    _buildSprite.sprite = ListOfSprites[_spriteIndex];
                    break;
                case 1:
                    _buildSprite.sprite = ListOfSpritesRed[_spriteIndex];
                    break;
                case 2:
                    _buildSprite.sprite = ListOfSpritesBlue[_spriteIndex];
                    break;
                case 3:
                    _buildSprite.sprite = ListOfSpritesYellow[_spriteIndex];
                    break;
            }
            
            CalculateTimeToChangeSprite();
        }

        void CalculateTimeToChangeSprite()
        {
            _spriteIndex++;
            _nextChangeTime = BuildTime * ((float)_spriteIndex / (float)(ListOfSprites.Count - 1f));
        }

        void FillBuildBar()
        {
            FillBar.fillAmount = _buildTimer / BuildTime;
        }

        void FillRepairBar()
        {
            FillBar.fillAmount = _buildTimer / _repairTotalTime;
        }

        public void OpenBuildSettings()
        {
            if (_isBuilt)
            {
                CanvasManager.Instance. InfoPanel.SetActive(true);

                CanvasManager.Instance.SetCurrentBuild(this);

                for(int i = 0; i < _settingsCanvas.transform.parent.childCount - 1; i++)
                {
                    _settingsCanvas.transform.parent.GetChild(i).gameObject.SetActive(false);
                }

                CanvasManager.Instance.OpenCanvas(_settingsCanvas, _buildCanvas);

                if (BuildType == TypeOfBuild.Turret)
                {
                    CanvasManager.Instance.OpenCanvas(_turretUpgradesCanvas, _actionsCanvas);
                    CanvasManager.Instance.SetTurretUpgradeButtonsState();
                }
                else
                {
                    CanvasManager.Instance.OpenCanvas(_buildUpgradesCanvas, _actionsCanvas);
                    CanvasManager.Instance.SetBuildUpgradeButtonsState();
                }

                BuildManager.Instance.SetSelectedBuild(this);

                SetCanvasSettings();
            }
        }

        private void SetCanvasSettings()
        {
            SetHealthCanvas();
        }

        private void SetHealthCanvas()
        {
            CanvasManager.Instance.SetBuildHealth(_currentHealth, _maxHealth, this);

            //(_currentHealth <= 0) { DestroyBuild(); }
        }

        public void SetBuildHealth(float current, float max)
        {
            _currentHealth = current;
            _maxHealth = max;

            if(CanvasManager.Instance.GetCurrentBuild() == this) { SetHealthCanvas(); }
        }

        public void RepairBuild()
        {
            AudioManager.Instance.PlayBuildingRepairSound();
            ResourceManager.Instance.Food -= GetFoodRepairCost();
            ResourceManager.Instance.Biomass -= GetBiomassRepairCost();

            _repairTotalTime = BuildTime * (1f - (float)_atributteManager._currentHealth / (float)_atributteManager._maxHealth);
            _buildTimer = 0;

            Debug.Log("Repair time: " + _repairTotalTime);

            Canvas.SetActive(true);
            _isRepairing = true;
        }

        public void SetNewHealth(int newHealth)
        {
            _atributteManager._maxHealth = newHealth;
            _atributteManager._currentHealth = _atributteManager._maxHealth;
        }

        public int GetFoodRepairCost()
        {
            _foodRepairCost = (int)(_totalFoodCost * 0.5f);

            Debug.Log(_foodRepairCost);

            return _foodRepairCost;
        }

        public int GetBiomassRepairCost()
        {
            _biomassRepairCost = (int)(_totalBiomassCost * 0.5f);

            Debug.Log(_biomassRepairCost);

            return _biomassRepairCost;
        }

        public bool GetReparationState()
        {
            return _isRepairing;
        }

        private void DestroyBuild()
        {
            Destroy(gameObject);
        }

        public GameObject GetBuildCanvas()
        {
            return _settingsCanvas;
        }

        public void SelectBuild(bool s)
        {
            _isSelected = s;
            _targetObject.SetActive(s);
            line.enabled = s;
        }
        /*
                #region TEST

                [ContextMenu("Reduce Health")]
                public void TestHealth()
                {
                    _currentHealth -= 25;
                    SetHealth();
                }

                [ContextMenu("Add Resources")]
                public void AddResources()
                {
                    ResourceManager.Instance.Food += 200;
                    ResourceManager.Instance.Biomass += 200;
                    ResourceManager.Instance.Energy += 200;
                }
                #endregion
        */
    }
}
