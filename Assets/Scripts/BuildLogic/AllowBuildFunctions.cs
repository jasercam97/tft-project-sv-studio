using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllowBuildFunctions : MonoBehaviour
{

    //Aqui poner los scripts a los que se necesiten referencias
    public Turret turret;

    private void OnDisable()
    {
        //Permitir hacer cosas
        if(turret){turret.AllowShot(true);} //Puede disparar
    }

    private void OnEnable()
    {
        //No permitir
        if(turret){turret.AllowShot(false);} //No puede disparar durante construccion o mejora
    }
}
