﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using GameManagers;
using UnityEngine.UI;
using Build;

namespace BuildPlacement {
    public class BuildButton : MonoBehaviour
    {
        public GameObject BuildBlueprint;

        [Header("Build Cost")]
        public float FoodCost;
        public float BiomassCost;
        public float EnergyCost;

        private Button _button;

        private void Awake()
        {
            _button = GetComponent<Button>();

            //SetButtonState();
        }

        public void InstantiateBlueprint()
        {
            if (BuildManager.Instance.CanSelectNewBuild())
            {
                GameObject bp = Instantiate(BuildBlueprint);
                bp.GetComponent<BuildBlueprint>().SetBuildCosts(FoodCost, BiomassCost, EnergyCost);
                BuildManager.Instance.SelectNewBuild();
            }
        }

        public void SetButtonState()
        {
            if (!_button) { _button = GetComponent<Button>(); }

            _button.interactable = CheckResources();
        }

        bool CheckResources()
        {
            if (ResourceManager.Instance.Food >= FoodCost && ResourceManager.Instance.Biomass >= BiomassCost && ResourceManager.Instance.Energy >= EnergyCost) { return true; }

            return false;
        }
    }
}
