
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CustomCursor : MonoBehaviour
{
    public LayerMask raycastLayer;

    private Animator anim;

    private Ray ray;
    private RaycastHit hit;
    
    private void Awake()
    {
        Cursor.visible = false;

        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        transform.position = Input.mousePosition;
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, raycastLayer))
        {
            if (hit.collider.CompareTag("Troop"))
            {
                anim.SetBool("OnTroop", true);
                anim.SetBool("OnBuilding", false);
                anim.SetBool("OnResource", false);
            }
            else if (hit.collider.CompareTag("Build"))
            {
                anim.SetBool("OnTroop", false);
                anim.SetBool("OnBuilding", true);
                anim.SetBool("OnResource", false);
            }
            else if (hit.collider.CompareTag("Resources"))
            {
                anim.SetBool("OnTroop", false);
                anim.SetBool("OnBuilding", false);
                anim.SetBool("OnResource", true);
            }
            else
            {
                anim.SetBool("OnTroop", false);
                anim.SetBool("OnBuilding", false);
                anim.SetBool("OnResource", false);
            }
        }
        else
        {
            anim.SetBool("OnTroop", false);
            anim.SetBool("OnBuilding", false);
            anim.SetBool("OnResource", false);
        }

        if (Input.GetMouseButtonDown(0))
        {
            anim.SetBool("Click", true);
        }

        if (Input.GetMouseButtonUp(0))
        {
            anim.SetBool("Click", false);
        }
    }

    public void PointerEnter()
    {
        anim.SetBool("Hover", true);
    }

    public void PointerExit()
    {
        anim.SetBool("Hover", false);
    }
}
