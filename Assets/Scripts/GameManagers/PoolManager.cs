﻿using Photon.Pun;
using Photon.Pun.UtilityScripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class PoolManager : Singleton<PoolManager>
{
    public GameObject GetFreeObject(Transform poolParent, GameObject objectPrefab)
    {
        GameObject freeObject = null;

        foreach(Transform child in poolParent)
        {
            if (!child.gameObject.activeInHierarchy)
            {
                freeObject = child.gameObject;
                break;
            }
        }
 

        if (freeObject == null)
        {
            //freeObject = Instantiate(objectPrefab, poolParent);
            freeObject = PhotonNetwork.Instantiate(objectPrefab.name, poolParent.position, Quaternion.identity);
            freeObject.transform.SetParent(poolParent);
        }

        return freeObject;
    }
}
