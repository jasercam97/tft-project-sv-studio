﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using GameManagers;

public class UIManager : MonoBehaviour
{
    [Header("Settings")]
    public GameObject SettingsPanel;
    public float SettingsPanelTime;
    private bool settingsOpen = false;

    public Image LifeBarsSettingImage, InfoUnitSettingImage, SoundSettingImage, AmbientSettingImage;
    public GameObject AmbientLight, AmbientNight;
    public Sprite AmbientLightSprite, AmbientNightSprite;
    public Sprite ShowLifeBarsSprite, HideLifeBarsSprite;
    public Sprite ShowInfoUnitSprite, HideInfoUnitSprite;
    public Sprite PlaySoundSprite, StopSoundSprite;

    private bool showLifeBar = true;
    private bool showInfoUnit = true;
    private bool ambientLight = false;

    private AudioManager audioManager;
    private float _musicVolume, _sfxVolume;

    private void Awake()
    {
        audioManager = FindObjectOfType<AudioManager>();
    }

    private void Start()
    {
        _musicVolume = audioManager.MusicVolume;
        _sfxVolume = audioManager.SFXVolume;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            SettingsButton();
    }

    public void SettingsButton()
    {
        if (!settingsOpen)
        {
            OpenSettingsPanel();
            settingsOpen = true;
        }
        else
        {
            CloseSettingsPanel();
            settingsOpen = false;
        }
    }

    void OpenSettingsPanel()
    {
        SettingsPanel.transform.DOScaleY(1, SettingsPanelTime).SetEase(Ease.InQuad).Play();
    }

    void CloseSettingsPanel()
    {
        SettingsPanel.transform.DOScaleY(0, SettingsPanelTime).SetEase(Ease.OutQuad).Play();
    }

    public void SetLifeBars()
    {
        showLifeBar = !showLifeBar;
        if (showLifeBar)
            LifeBarsSettingImage.sprite = ShowLifeBarsSprite;
        else
            LifeBarsSettingImage.sprite = HideLifeBarsSprite;
        CanvasManager.Instance.ShowLifeBars();
    }

    public void SetAmbient()
    {
        ambientLight = !ambientLight;
        if (ambientLight)
        {
            AmbientLight.SetActive(true);
            AmbientNight.SetActive(false);
            AmbientSettingImage.sprite = AmbientLightSprite;
        }
        else
        {
            AmbientLight.SetActive(false);
            AmbientNight.SetActive(true);
            AmbientSettingImage.sprite = AmbientNightSprite;
        }
    }

    public void SetInfoUnits()
    {
        showInfoUnit = !showInfoUnit;
        if (showInfoUnit)
            InfoUnitSettingImage.sprite = ShowInfoUnitSprite;
        else
            InfoUnitSettingImage.sprite = HideInfoUnitSprite;
        CanvasManager.Instance.SwitchInformationStatus();
    }

    public void SetVolumen()
    {
        if (audioManager.MusicVolume > 0 || audioManager.SFXVolume > 0)
        {
            Debug.Log("Mute");
            SoundSettingImage.sprite = StopSoundSprite;
            audioManager.MusicVolumeLevel(0);
            audioManager.SFXVolumeLevel(0);
        }
        else
        {
            Debug.Log("UnMute");
            SoundSettingImage.sprite = PlaySoundSprite;
            audioManager.MusicVolumeLevel(_musicVolume);
            audioManager.SFXVolumeLevel(_sfxVolume);
        }

    }
}
