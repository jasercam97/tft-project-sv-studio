using Utils;
using System.Collections.Generic;
using UnityEngine;
using GameManagers;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using UnityEngine.EventSystems;
using Build;

public class SelectorRTS : NetworkSingleton<SelectorRTS>
{
    [Header("SELECTOR")]
    [SerializeField] private RectTransform _selectSquareImage;
    [SerializeField] private GameObject _targetObject;
    [SerializeField] private LayerMask _mask;
    [SerializeField] private LayerMask _maskMinimap;

    AnimationEvents events;

    //ONLINE
    [HideInInspector] public int _team;
    [HideInInspector] public int _group;

    // KEYS
    private KeyCode _selectionKey;
    private KeyCode _addToSelectionKey;
    private KeyCode _commandKey;
    private KeyCode _attackMoveKey;
    private KeyCode _removeToSelectionKey;

    //LIST OF SELECTABLES AND LIST OF SELECTABLES SELECTED
    public List<GameObject> _selectables, _objectsSelected, _troopsSelected, _buildingsSelected;
    public List<GameObject> Selectables { get => _selectables; private set => this._selectables = value; }
    public List<GameObject> ObjectsSelected { get => _objectsSelected; private set => this._objectsSelected = value; }
    public List<GameObject> TroopsSelected { get => _troopsSelected; private set => this._troopsSelected = value; }
    public List<GameObject> BuildingsSelected { get => _buildingsSelected; private set => this._buildingsSelected = value; }
    public GameObject ResourceSelected;

    //MOUSE POSITIONS
    private Vector2 _startPos, _endPos, _mouseDownPosition, _mouseUpPosition;
    public float _cameraSpeed;
    private bool _startSelection = false;

    //DOUBLE CLICK
    float clicked = 0;
    float clicktime = 0;
    float clickdelay = 0.5f;

    #region UNITY EVENTS

    private void Awake()
    {
        events = GetComponent<AnimationEvents>();
        _selectSquareImage.gameObject.SetActive(false);
        _targetObject.SetActive(false);

        _selectables = new List<GameObject>();
        _objectsSelected = new List<GameObject>();

        // KEYS
        _selectionKey = InputManager.Instance.SelectionKey;
        _addToSelectionKey = InputManager.Instance.AddToSelectionKey;
        _commandKey = InputManager.Instance.CommandKey;
        _attackMoveKey = InputManager.Instance.AttackMoveKey;
        _removeToSelectionKey = InputManager.Instance.RemoveToSelectionKey;

        _cameraSpeed = RTSCameraController.Instance._movSpeed;
    }

    public void InitGroupAndTeam()
    {
        _group = _team = PhotonNetwork.LocalPlayer.GetPlayerNumber();
    }

    public void InitGroupAndTeamTuto()
    {
        _group = _team = 0;
    }

    private void Update()
    {
        if (!UIClicks.IsPointerOverUIElement())
        {
            if (Input.GetKeyDown(_selectionKey))
            {
                StartSelection();
            }
        }

        if (Input.GetKey(_selectionKey) && _startSelection)
        {
            DragSelection();
        }

        if (Input.GetKeyUp(_selectionKey) && _startSelection)
        {
            EndSelection();
        }

        if (!UIClicks.IsPointerOverUIElement())
        {
            if (Input.GetKeyDown(_commandKey))
            {
                if (ObjectsSelected.Count > 0)
                    DoMoveCommand();
            }
        }
        else
        {
                Debug.Log("UI: ");
        }
    }
    #endregion

    #region ACTIONS

    private void StartSelection()
    {
        
        RTSCameraController.Instance._movSpeed = 0f;

        _mouseDownPosition = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        _startPos = Input.mousePosition;
        _startSelection = true;
    }

    private void DragSelection()
    {
        if (!_selectSquareImage.gameObject.activeInHierarchy)
            _selectSquareImage.gameObject.SetActive(true);

        _endPos = Input.mousePosition;

        var sizeX = _endPos.x - _startPos.x;
        var sizeY = _endPos.y - _startPos.y;

        _selectSquareImage.sizeDelta = new Vector2(Mathf.Abs(sizeX), Mathf.Abs(sizeY));
        _selectSquareImage.anchoredPosition = _startPos + new Vector2( sizeX / 2f, sizeY / 2f);
    }

    private void EndSelection()
    {
        _mouseUpPosition = Camera.main.ScreenToViewportPoint(Input.mousePosition);

        _selectSquareImage.gameObject.SetActive(false);

        if (Input.GetKey(_addToSelectionKey))
        {
            AddObjects();
        }
        else if (Input.GetKey(_removeToSelectionKey))
        {
            RemoveObjects();
        }
        else
        {
            SelectObjects();
        }

        RTSCameraController.Instance._movSpeed = _cameraSpeed;

        _startSelection = false;
    }

    private void DoMoveCommand()
    {
        var screenRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(screenRay, out hit, Mathf.Infinity, _mask))
        {
            if (Input.GetKey(_attackMoveKey))
            {
                for (var i = 0; i < _objectsSelected.Count; i++)
                {
                    if (_objectsSelected[i].TryGetComponent(out Warrior warrior))
                        warrior._ignoreEnemies = false;
                }
            }
            else
            {
                for (var i = 0; i < _objectsSelected.Count; i++)
                {
                    if (_objectsSelected[i].TryGetComponent(out Warrior warrior))
                        warrior._ignoreEnemies = true;
                }
            }
            Debug.Log(hit.collider.tag);
            switch (hit.collider.tag)
            {
                case "Terrain":

                    AudioManager.Instance.PlayMoveCommandSound();
                    _targetObject.transform.position = hit.point;
                    _targetObject.SetActive(false);
                    _targetObject.SetActive(true);
                    CancelInvoke(nameof(DeactivateIndicator));
                    Invoke(nameof(DeactivateIndicator), 1f);

                    List<Vector3> targetPositionList = GetPositionListAround(hit.point, new float[] { 2f, 4f, 6f, 8f, 10f }, new int[] { 5, 10, 20, 40, 80 });
                    var targetPositionListIndex = 0;

                    for (var i = 0; i < _objectsSelected.Count; i++)
                    {
                        if (_objectsSelected[i].TryGetComponent(out Mover mover))
                        {
                            _objectsSelected[i].GetComponent<Mover>()._isCollecting = false;
                            _objectsSelected[i].GetComponent<Mover>()._isAttacking = false;
                        }
                        if (_objectsSelected[i].TryGetComponent<Mover>(out _))
                        {
                            mover.MoveToTarget(targetPositionList[targetPositionListIndex]);
                            targetPositionListIndex = (targetPositionListIndex + 1) % targetPositionList.Count;
                        }
                    }
                    break;
                case "CollectionPoint":
                    for (var i = 0; i < _objectsSelected.Count; i++)
                    {
                        if (_objectsSelected[i].TryGetComponent<Mover>(out _))
                        {
                            _objectsSelected[i].GetComponent<Mover>()._isCollecting = true;
                            _objectsSelected[i].GetComponent<Mover>()._isAttacking = false;
                        }
                        if (_objectsSelected[i].TryGetComponent(out Gatherer gatherer))
                            gatherer.MoveToTargetCollectionPoint(hit.collider.transform);
                    }
                    break;
                case "Resources":
                    for (var i = 0; i < _objectsSelected.Count; i++)
                    {
                        if (_objectsSelected[i].TryGetComponent<Mover>(out _))
                        {
                            _objectsSelected[i].GetComponent<Mover>()._isCollecting = true;
                            _objectsSelected[i].GetComponent<Mover>()._isAttacking = false;
                        }
                        if (_objectsSelected[i].TryGetComponent(out Gatherer gatherer))
                            gatherer.MoveToTargetResource(hit.collider.transform);
                    }
                    break;
                case "Troop":

                    if (hit.collider.gameObject.GetComponent<Selectable>().Team == _objectsSelected[0].GetComponent<Selectable>().Team)
                    {
                        AudioManager.Instance.PlayMoveCommandSound();
                    }
                    else
                        AudioManager.Instance.PlayAttackCommandSound();

                    for (var i = 0; i < _objectsSelected.Count; i++)
                    {
                        if (_objectsSelected[i].TryGetComponent<Mover>(out _))
                        {
                            _objectsSelected[i].GetComponent<Mover>()._isCollecting = false;
                        }
                        if(hit.collider.gameObject.GetComponent<Selectable>().Team == _objectsSelected[i].GetComponent<Selectable>().Team)
                        {
                            if (_objectsSelected[i].TryGetComponent(out Mover mover))
                            {
                                _objectsSelected[i].GetComponent<Mover>()._isAttacking = false;
                                mover.MoveToTargetAlly(hit.collider.transform);
                            }
                        }
                        else
                        {
                            if (_objectsSelected[i].TryGetComponent(out Warrior warrior))
                            {
                                if(warrior.warriorType == Warrior.WarriorType.Melee)
                                {
                                    if (_objectsSelected[i].TryGetComponent(out Warrior w))
                                        if (w.warriorType != Warrior.WarriorType.Air)
                                        {
                                            _objectsSelected[i].GetComponent<Mover>()._isAttacking = true;
                                            warrior.MoveToEnemy(hit.collider.transform);
                                        }
                                    else
                                            warrior.MoveToEnemy(hit.collider.transform);
                                }
                                else
                                {
                                    _objectsSelected[i].GetComponent<Mover>()._isAttacking = true;
                                    warrior.MoveToEnemy(hit.collider.transform);
                                }
                            }
                        }
                    }
                    break;
                case "Build":
                    Debug.Log("Build");

                    if (_objectsSelected.Count > 0)
                    {
                        if (hit.collider.gameObject.TryGetComponent<CapturableBuilding>(out _))
                        {
                            for (var i = 0; i < _objectsSelected.Count; i++)
                            {
                                if (_objectsSelected[i].TryGetComponent(out Mover mover))
                                {
                                    _objectsSelected[i].GetComponent<Mover>()._isAttacking = false;
                                    _objectsSelected[i].GetComponent<Mover>()._isCollecting = false;
                                    mover.MoveToTargetAlly(hit.collider.transform);
                                }
                            }
                        }
                        else {
                            Building targetBuilding = hit.collider.GetComponent<Building>();
                            int currentGroup = _objectsSelected[0].GetComponent<Selectable>().Group;
                            int currentTeam = _objectsSelected[0].GetComponent<Selectable>().Team;

                            for (var i = 0; i < _objectsSelected.Count; i++)
                            {
                                if (targetBuilding.Team == currentTeam)
                                {
                                    Debug.Log("HOLAAAA");
                                    if (_objectsSelected[i].TryGetComponent(out Mover mover))
                                    {
                                        _objectsSelected[i].GetComponent<Mover>()._isCollecting = false;
                                        _objectsSelected[i].GetComponent<Mover>()._isAttacking = false;
                                    }
                                    if (_objectsSelected[i].TryGetComponent(out Evolver evolver))
                                        evolver.MoveToBuildEvolver(hit.collider.transform);
                                }
                                else if (targetBuilding.Team != currentTeam)
                                {
                                    Debug.Log("ADIOSS");
                                    if (_objectsSelected[i].TryGetComponent(out Warrior warrior))
                                    {
                                        _objectsSelected[i].GetComponent<Mover>()._isAttacking = true;
                                        warrior.MoveToEnemy(hit.collider.transform);
                                    }
                                }
                            }
                        }   
                    }
                    break;

                default:
                    break;
            }
        }
    }

    public void DoMoveInMinimap(Vector3 position)
    {
        RaycastHit hit;
        if (Physics.Raycast(position, Vector3.down + Vector3.forward, out hit, Mathf.Infinity, _maskMinimap))
        {
            if (Input.GetKey(_attackMoveKey))
            {
                for (var i = 0; i < _objectsSelected.Count; i++)
                {
                    if (_objectsSelected[i].TryGetComponent(out Warrior warrior))
                        warrior._ignoreEnemies = false;
                }
            }
            else
            {
                for (var i = 0; i < _objectsSelected.Count; i++)
                {
                    if (_objectsSelected[i].TryGetComponent(out Warrior warrior))
                        warrior._ignoreEnemies = true;
                }
            }
        }
        else
        {
            Debug.Log("Not detecting terrain");
        }
        _targetObject.transform.position = hit.point;
        _targetObject.SetActive(true);
        CancelInvoke(nameof(DeactivateIndicator));
        Invoke(nameof(DeactivateIndicator), 1f);

        List<Vector3> targetPositionList = GetPositionListAround(hit.point, new float[] { 2f, 4f, 6f, 8f, 10f }, new int[] { 5, 10, 20, 40, 80 });
        var targetPositionListIndex = 0;

        for (var i = 0; i < _objectsSelected.Count; i++)
        {
            _objectsSelected[i].GetComponent<Mover>()._isCollecting = false;
            _objectsSelected[i].GetComponent<Mover>()._isAttacking = false;
            if (_objectsSelected[i].TryGetComponent(out Mover mover))
            {
                mover.MoveToTarget(targetPositionList[targetPositionListIndex]);
                targetPositionListIndex = (targetPositionListIndex + 1) % targetPositionList.Count;
            }
        }
    }

    #endregion

    #region SELECTION

    private void SelectObjects()
    {
        ClearSelection();

        AddObjects();
    }

    private void AddObjects()
    {
        if (_mouseDownPosition != _mouseUpPosition) //If Drag
        {
            Rect selectRect = new Rect(_mouseDownPosition.x, _mouseDownPosition.y, _mouseUpPosition.x - _mouseDownPosition.x, _mouseUpPosition.y - _mouseDownPosition.y);

            if (_selectables.Count > 0)
            {
                AudioManager.Instance.PlaySelectionSound();
            }

            foreach (GameObject selectObject in _selectables)
            {
                if (selectRect.Contains(Camera.main.WorldToViewportPoint(selectObject.transform.position), true))
                {
                    if (selectObject.GetComponent<Selectable>().Group == _group)
                    {
                        if (!_objectsSelected.Contains(selectObject))
                        {
                             _objectsSelected.Add(selectObject);
                            if(selectObject.TryGetComponent(out BuildBehaviour b))
                            {
                                if (b._isBuilt)
                                {
                                    AudioManager.Instance.PlaySelectionSound();
                                    _buildingsSelected.Add(selectObject);
                                }
                                else
                                {
                                    _objectsSelected.Remove(selectObject);
                                }
                            }
                            else
                            {
                                _troopsSelected.Add(selectObject);
                            }
                        }
                    }
                }
            }
            foreach (GameObject selectObject in _objectsSelected)
            {
                selectObject.GetComponent<Selectable>().SetSelected(true);
            }
        }
        else 
        {
            RaycastHit hit;
            Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, _mask);
            if (hit.collider != null)
            {
                if (_selectables.Contains(hit.collider.gameObject))
                {
                    if (hit.collider.gameObject.GetComponent<Selectable>().Group == _group)
                    {
                        if (!_objectsSelected.Contains(hit.collider.gameObject))
                        {
                            _objectsSelected.Add(hit.collider.gameObject);
                            if (hit.collider.gameObject.TryGetComponent(out BuildBehaviour b))
                            {
                                if (b._isBuilt)
                                {
                                    AudioManager.Instance.PlaySelectionSound();
                                    _buildingsSelected.Add(hit.collider.gameObject);
                                }
                                else
                                {
                                    _objectsSelected.Remove(hit.collider.gameObject);
                                }
                            }
                            else
                            {
                                AudioManager.Instance.PlaySelectionSound();
                                _troopsSelected.Add(hit.collider.gameObject);
                            }
                        }
                        foreach (GameObject selectObject in _objectsSelected)
                        {
                            selectObject.GetComponent<Selectable>().SetSelected(true);
                        }
                        //DOUBLE CLICK
                        clicked++;
                        if (clicked == 1) clicktime = Time.time;

                        if (clicked > 1 && Time.time - clicktime < clickdelay)
                        {
                            clicked = 0;
                            clicktime = 0;
                            var troops = TeamManager.Instance.GetTroops(_group, hit.collider.gameObject);
                            if (troops != null)
                            {
                                SelectObjectsSC(troops);
                            }
                            else
                                Debug.Log("NULL");
                            Debug.Log("Double CLick on:  " + hit.collider.gameObject);

                        }
                        else if (clicked > 2 || Time.time - clicktime > 1) clicked = 0;
                    }
                }
                else if (hit.collider.gameObject.TryGetComponent<VisionBuilding>(out _))
                {
                    ClearSelection();
                    CanvasManager.Instance.CloseCanvas(CanvasManager.Instance.BuildPanel);
                    CanvasManager.Instance.ShowVisionBuildingPanel(true);
                    CanvasManager.Instance.ShowMoraleBuildingPanel(false);
                    CanvasManager.Instance.ShowResourcePanel(false);
                }
                else if (hit.collider.gameObject.TryGetComponent<MoraleBuilding>(out _))
                {
                    ClearSelection();
                    CanvasManager.Instance.CloseCanvas(CanvasManager.Instance.BuildPanel);
                    CanvasManager.Instance.ShowMoraleBuildingPanel(true);
                    CanvasManager.Instance.ShowVisionBuildingPanel(false);
                    CanvasManager.Instance.ShowResourcePanel(false);
                }
                else if (hit.collider.gameObject.TryGetComponent<Resource>(out _))
                {
                    ClearSelection();
                    ResourceSelected = hit.collider.gameObject;
                    CanvasManager.Instance.CloseCanvas(CanvasManager.Instance.BuildPanel);
                    CanvasManager.Instance.ShowMoraleBuildingPanel(false);
                    CanvasManager.Instance.ShowVisionBuildingPanel(false);
                    CanvasManager.Instance.ShowResourcePanel(true);
                }
                else
                {
                    CanvasManager.Instance.ShowMoraleBuildingPanel(false);
                    CanvasManager.Instance.ShowVisionBuildingPanel(false);
                    CanvasManager.Instance.ShowResourcePanel(false);
                }
            }
        }
    }

    private void RemoveObjects()
    {
        if (_mouseDownPosition != _mouseUpPosition) //If Drag
        {
            var selectRect = new Rect(_mouseDownPosition.x, _mouseDownPosition.y, _mouseUpPosition.x - _mouseDownPosition.x, _mouseUpPosition.y - _mouseDownPosition.y);

            foreach (var selectObject in _selectables)
            {
                if (selectRect.Contains(Camera.main.WorldToViewportPoint(selectObject.transform.position), true))
                {
                    _objectsSelected.Remove(selectObject);
                    if (selectObject.TryGetComponent<Building>(out _))
                    {
                        _buildingsSelected.Remove(selectObject);
                    }
                    else
                    {
                        _troopsSelected.Remove(selectObject);
                    }
                    selectObject.GetComponent<Selectable>().SetSelected(false);
                }
            }
        }
        else
        {
            RaycastHit hit;
            Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity, _mask);

            if (_selectables.Contains(hit.collider.gameObject))
            {
                _objectsSelected.Remove(hit.collider.gameObject);
                if (hit.collider.gameObject.TryGetComponent<Building>(out _))
                {
                    _buildingsSelected.Remove(hit.collider.gameObject);
                }
                else
                {
                    _troopsSelected.Remove(hit.collider.gameObject);
                }
                hit.collider.gameObject.GetComponent<Selectable>().SetSelected(false);
            }
        }
    }

    public void ClearSelection()
    {
        for (var i = 0; i < _objectsSelected.Count; i++)
            if(_objectsSelected[i]!=null) _objectsSelected[i].GetComponent<Selectable>().SetSelected(false);
        _objectsSelected.Clear();
        _troopsSelected.Clear();
        _buildingsSelected.Clear();
    }

    private void DeactivateIndicator()
    {
        _targetObject.SetActive(false);
    }
    #endregion

    #region RING FORMATION

    // Calculate rings
    private List<Vector3> GetPositionListAround(Vector3 startPosition, float[] ringDistanceArray, int[] ringPositionCountArray)
    {
        List<Vector3> positionList = new List<Vector3>();
        positionList.Add(startPosition);
        for (var i = 0; i < ringDistanceArray.Length; i++)
        {
            positionList.AddRange(GetPositionListAround(startPosition, ringDistanceArray[i], ringPositionCountArray[i]));
        }
        return positionList;
    }

    // Calculate points within a ring
    private List<Vector3> GetPositionListAround(Vector3 startPosition, float distance, int positionCount)
    {
        List<Vector3> positionList = new List<Vector3>();
        for (var i = 0; i < positionCount; i++)
        {
            var angle = i * (360f / positionCount);
            var dir = ApplyRotationToVector(new Vector3(1, 0, 0), angle);
            var position = startPosition + dir * distance;
            positionList.Add(position);
        }
        return positionList;
    }

    //Apply rotation within a position
    private Vector3 ApplyRotationToVector(Vector3 vec, float angle)
    {
        return Quaternion.Euler(0, angle, 0) * vec;
    }

    #endregion

    public void ClearTroop(GameObject troop)
    {
        _objectsSelected.Remove(troop);
        if (troop.TryGetComponent<Building>(out _))
        {
            _buildingsSelected.Remove(troop);
        }
        else
        {
            _troopsSelected.Remove(troop);
        }
    }

    // SHORTCUTS
    public void SelectObjectsSC(List<GameObject> SC)
    {
        if (SC.Count > 0)
        {
            AudioManager.Instance.PlaySelectionSound();
        }

        for (int i = 0; i < SC.Count; i++)
        {
            if (!_objectsSelected.Contains(SC[i]))
            {
                _objectsSelected.Add(SC[i]);
                _troopsSelected.Add(SC[i]);
            }
                _objectsSelected[i].GetComponent<Selectable>().SetSelected(true);
        }
    }
}
