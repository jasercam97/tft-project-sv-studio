﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace GameManagers {
    public class InputManager : Singleton<InputManager>
    {
        /// <summary>
        /// 1- Crear HEADER para cada conjunto de variables que se quiera almacenar.
        /// 2- Crear función CHANGE para cada variable. Meter la función dentro de un #region con el mismo nombre que el HEADER.
        /// 3- Almacenar la tecla en el AWAKE del script desde el que se va a llamar y utilizar dicha variable privada.
        /// 3b- Si es necesario, crear función para actualizar los controles en el script desde el que se va a llamar. Esta función almacenaría de nuevo en la variable privada el valor de cada tecla.
        /// Esto se puede hacer una vez esten creados todos los controles de un mismo script.
        /// </summary>

        [Header("Build Keys")]
        public KeyCode PlaceBuild;
        public KeyCode CancelBuild;
        public KeyCode SelectBuild;
        public KeyCode DeselectBuild;

        [Header("Selector Keys")]
        public KeyCode SelectionKey;
        public KeyCode AddToSelectionKey;
        public KeyCode CommandKey;
        public KeyCode AttackMoveKey;
        public KeyCode RemoveToSelectionKey;

        [Header("Camera")]
        public KeyCode MoveUpKey;
        public KeyCode MoveDownKey;
        public KeyCode MoveLeftKey;
        public KeyCode MoveRightKey;

        [Header("Shorcuts")]
        public KeyCode GathererTroopsKey;
        public KeyCode WarriorTroopsKey;
        public KeyCode MeleeTroopsKey;
        public KeyCode DistanceTroopsKey;
        public KeyCode AirTroopsKey;
        public KeyCode QueenKey;
        public KeyCode MeleeBuildingKey;
        public KeyCode DistanceBuildingKey;
        public KeyCode AirBuildingKey;
        public KeyCode TroopsKey;

        #region BUILD_KEYS

        public void ChangePlaceBuild(KeyCode newKey)
        {
            PlaceBuild = newKey;
        }

        public void ChangeCancelBuild(KeyCode newKey)
        {
            CancelBuild = newKey;
        }

        public void ChangeSelectBuild(KeyCode newKey)
        {
            SelectBuild = newKey;
        }

        public void ChangeDeselectBuild(KeyCode newKey)
        {
            DeselectBuild = newKey;
        }

        #endregion

        #region SELECTOR_KEYS

        public void ChangeSelectionKey(KeyCode newKey)
        {
            SelectionKey = newKey;
        }

        public void ChangeAddToSelectionKey(KeyCode newKey)
        {
            AddToSelectionKey = newKey;
        }

        public void ChangeRemoveToSelectionKey(KeyCode newKey)
        {
            RemoveToSelectionKey = newKey;
        }

        public void ChangeCommandKey(KeyCode newKey)
        {
            CommandKey = newKey;
        }

        public void ChangeAttackMoveKey(KeyCode newKey)
        {
            AttackMoveKey = newKey;
        }
        #endregion

        #region CAMERA_KEYS

        public void MoveUP(KeyCode newKey)
        {
            MoveUpKey = newKey;
        }

        public void MoveDown(KeyCode newKey)
        {
            MoveDownKey = newKey;
        }

        public void MoveLeft(KeyCode newKey)
        {
            MoveLeftKey = newKey;
        }

        public void MoveRight(KeyCode newKey)
        {
            MoveRightKey = newKey;
        }
        #endregion

        #region SHORTCUTS

        public void SelectGathererTroopsKey(KeyCode newKey)
        {
            GathererTroopsKey = newKey;
        }

        public void SelectWarriorTroopsKey(KeyCode newKey)
        {
            WarriorTroopsKey = newKey;
        }

        public void SelectMeleeTroopsKey(KeyCode newKey)
        {
            MeleeTroopsKey = newKey;
        }

        public void SelectDistanceTroopsKey(KeyCode newKey)
        {
            DistanceTroopsKey = newKey;
        }

        public void SelectAirTroopsKey(KeyCode newKey)
        {
            AirTroopsKey = newKey;
        }

        public void SelectQueenKey(KeyCode newKey)
        {
            QueenKey = newKey;
        }

        public void SelectMeleeBuildingKey(KeyCode newKey)
        {
            MeleeBuildingKey = newKey;
        }

        public void SelectDistanceBuildingKey(KeyCode newKey)
        {
            DistanceBuildingKey = newKey;
        }

        public void SelectAirBuildingKey(KeyCode newKey)
        {
            AirBuildingKey = newKey;
        }

        public void SelectTroopsKey(KeyCode newKey)
        {
            TroopsKey = newKey;
        }

        #endregion
    }
}
