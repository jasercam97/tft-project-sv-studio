﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class TeamManager : Singleton<TeamManager>
{
    public List<Queen> Queens;
    public List<List<Building>> BuildingPerGroup;
    public List<List<Mover>> TroopsPerGroup;
    public List<List<Mover>> TroopsPerTeam;

    [HideInInspector] public int _playerAmount = 12;

    private void Awake()
    {
        TroopsPerGroup = new List<List<Mover>>(_playerAmount);
        TroopsPerTeam = new List<List<Mover>>(_playerAmount);
        BuildingPerGroup = new List<List<Building>>(_playerAmount);
        Queens = new List<Queen>(_playerAmount);

        for (int i=0; i < _playerAmount; i++)
        {
            TroopsPerGroup.Add(new List<Mover>());
            TroopsPerTeam.Add(new List<Mover>());
            BuildingPerGroup.Add(new List<Building>());
            Queens.Add(null);
        }
    }

    #region LIST MANAGER
    public void AddToGroup(Mover troop, int group)
    {
        TroopsPerGroup[group].Add(troop);
    }

    public void AddToTeam(Mover troop, int team)
    {
        TroopsPerTeam[team].Add(troop);
    }

    public void RemoveFromGroup(Mover troop, int group)
    {
        TroopsPerGroup[group].Remove(troop);
    }

    public void RemoveFromTeam(Mover troop, int team)
    {
        TroopsPerTeam[team].Remove(troop);
    }

    public void AddBuildingToGroup(Building building, int group)
    {
        BuildingPerGroup[group].Add(building);
    }

    public void RemoveBuildingFromGroup(Building building, int group)
    {
        BuildingPerGroup[group].Remove(building);
    }
    #endregion

    #region DOUBLE CLICK

    public List<GameObject> GetTroops(int group, GameObject troop)
    {
        if (troop.TryGetComponent<Gatherer>(out Gatherer g))
        {
            return GetGatherers(group);
        }
        else if (troop.TryGetComponent<Warrior>(out Warrior w))
        {
            if (w.warriorType == Warrior.WarriorType.Melee)
                return GetMeleeTroops(group);
            else if (w.warriorType == Warrior.WarriorType.Air)
                return GetAirTroops(group);
            else if (w.warriorType == Warrior.WarriorType.Distance)
                return GetDistanceTroops(group);
            else
                return null;
        }
        else
            return null;
    }

    public List<GameObject> GetGatherers(int group)
    {
        List<GameObject> gatherers = new List<GameObject>(TroopsPerGroup[group].Count);

        for (int i = 0; i < TroopsPerGroup[group].Count; i++)
        {
            if (TroopsPerGroup[group][i].TryGetComponent<Gatherer>(out Gatherer g))
            {
                gatherers.Add(TroopsPerGroup[group][i].gameObject);
            }
        }

        return gatherers;
    }

    public List<GameObject> GetMeleeTroops(int group)
    {
        List<GameObject> warriors = new List<GameObject>(TroopsPerGroup[group].Count);

        for (int i = 0; i < TroopsPerGroup[group].Count; i++)
        {
            if (TroopsPerGroup[group][i].TryGetComponent<Warrior>(out Warrior w))
            {
                if(w.warriorType == Warrior.WarriorType.Melee)
                    warriors.Add(TroopsPerGroup[group][i].gameObject);
            }
        }

        return warriors;
    }


    public List<GameObject> GetAirTroops(int group)
    {
        List<GameObject> warriors = new List<GameObject>(TroopsPerGroup[group].Count);

        for (int i = 0; i < TroopsPerGroup[group].Count; i++)
        {
            if (TroopsPerGroup[group][i].TryGetComponent<Warrior>(out Warrior w))
            {
                if (w.warriorType == Warrior.WarriorType.Air)
                    warriors.Add(TroopsPerGroup[group][i].gameObject);
            }
        }

        return warriors;
    }

    public List<GameObject> GetDistanceTroops(int group)
    {
        List<GameObject> warriors = new List<GameObject>(TroopsPerGroup[group].Count);

        for (int i = 0; i < TroopsPerGroup[group].Count; i++)
        {
            if (TroopsPerGroup[group][i].TryGetComponent<Warrior>(out Warrior w))
            {
                if (w.warriorType == Warrior.WarriorType.Distance)
                    warriors.Add(TroopsPerGroup[group][i].gameObject);
            }
        }

        return warriors;
    }

    #endregion
}
