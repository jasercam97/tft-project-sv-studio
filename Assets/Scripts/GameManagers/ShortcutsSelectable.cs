﻿/*
 * Jose Manuel Herrera Vera 
 * @projects_ia (Twitter)
 * https://josemhv.itch.io
 *
 * Shorcuts Selectable v1.0
 * 
 * Date: 2021/08/21
 */
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class ShortcutsSelectable : Singleton<ShortcutsSelectable>
{
    private List<GameObject> List0 = new List<GameObject>();
    private List<GameObject> List1 = new List<GameObject>();
    private List<GameObject> List2 = new List<GameObject>();
    private List<GameObject> List3 = new List<GameObject>();
    private List<GameObject> List4 = new List<GameObject>();
    private List<GameObject> List5 = new List<GameObject>();
    private List<GameObject> List6 = new List<GameObject>();
    private List<GameObject> List7 = new List<GameObject>();
    private List<GameObject> List8 = new List<GameObject>();
    private List<GameObject> List9 = new List<GameObject>();

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            if (Input.GetKey(KeyCode.LeftControl))
            {
                List0.Clear();
                foreach (GameObject obj in SelectorRTS.Instance.ObjectsSelected) { List0.Add(obj); }
            }
            else
            {
                SelectorRTS.Instance.SelectObjectsSC(List0);
            }
        }
        else if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (Input.GetKey(KeyCode.LeftControl))
            {
                List1.Clear();
                foreach (GameObject obj in SelectorRTS.Instance.ObjectsSelected) { List1.Add(obj); }
            }
            else
            {
                SelectorRTS.Instance.SelectObjectsSC(List1);
            }
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            if (Input.GetKey(KeyCode.LeftControl))
            {
                List2.Clear();
                foreach (GameObject obj in SelectorRTS.Instance.ObjectsSelected) { List2.Add(obj); }
            }
            else
            {
                SelectorRTS.Instance.SelectObjectsSC(List2);
            }
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            if (Input.GetKey(KeyCode.LeftControl))
            {
                List3.Clear();
                foreach (GameObject obj in SelectorRTS.Instance.ObjectsSelected) { List3.Add(obj); }
            }
            else
            {
                SelectorRTS.Instance.SelectObjectsSC(List3);
            }
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            if (Input.GetKey(KeyCode.LeftControl))
            {
                List4.Clear();
                foreach (GameObject obj in SelectorRTS.Instance.ObjectsSelected) { List4.Add(obj); }
            }
            else
            {
                SelectorRTS.Instance.SelectObjectsSC(List4);
            }
        }
        else if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            if (Input.GetKey(KeyCode.LeftControl))
            {
                List5.Clear();
                foreach (GameObject obj in SelectorRTS.Instance.ObjectsSelected) { List5.Add(obj); }
            }
            else
            {
                SelectorRTS.Instance.SelectObjectsSC(List5);
            }
        }
        else if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            if (Input.GetKey(KeyCode.LeftControl))
            {
                List6.Clear();
                foreach (GameObject obj in SelectorRTS.Instance.ObjectsSelected) { List6.Add(obj); }
            }
            else
            {
                SelectorRTS.Instance.SelectObjectsSC(List6);
            }
        }
        else if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            if (Input.GetKey(KeyCode.LeftControl))
            {
                List7.Clear();
                foreach (GameObject obj in SelectorRTS.Instance.ObjectsSelected) { List7.Add(obj); }
            }
            else
            {
                SelectorRTS.Instance.SelectObjectsSC(List7);
            }
        }
        else if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            if (Input.GetKey(KeyCode.LeftControl))
            {
                List8.Clear();
                foreach (GameObject obj in SelectorRTS.Instance.ObjectsSelected) { List8.Add(obj); }
            }
            else
            {
                SelectorRTS.Instance.SelectObjectsSC(List8);
            }
        }
        else if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            if (Input.GetKey(KeyCode.LeftControl))
            {
                List9.Clear();
                foreach (GameObject obj in SelectorRTS.Instance.ObjectsSelected) { List9.Add(obj); }
            }
            else
            {
                SelectorRTS.Instance.SelectObjectsSC(List9);
            }
        }
    }
}
