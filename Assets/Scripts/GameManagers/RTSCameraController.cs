﻿using Utils;
using UnityEngine;
using GameManagers;

public class RTSCameraController : Singleton<RTSCameraController>
{
    [Header("PARAMETERS")]
    public float _movSpeed = 20f;
    [SerializeField] private float _zoomSpeed = 50f;
    [SerializeField] private float _borderWidth = 10f;
    [SerializeField] private float _zoomMin = 3.0f;
    [SerializeField] private float _zoomMax = 12.0f;

    [Header("CLAMPING")]
    public Transform MinXPoint;
    public Transform MaxXPoint;
    public Transform MinZPoint;
    public Transform MaxZPoint;

    private float _minXPosition;
    private float _maxXPosition;
    private float _minZPosition;
    private float _maxZPosition;


    private KeyCode _moveUpKey;
    private KeyCode _moveDownKey;
    private KeyCode _moveLeftKey;
    private KeyCode _moveRightKey;

    private void Awake()
    {
        _moveUpKey = InputManager.Instance.MoveUpKey;
        _moveDownKey = InputManager.Instance.MoveDownKey;
        _moveLeftKey = InputManager.Instance.MoveLeftKey;
        _moveRightKey = InputManager.Instance.MoveRightKey;

        _minXPosition = MinXPoint.position.x;
        _maxXPosition = MaxXPoint.position.x;
        _minZPosition = MinZPoint.position.z - transform.position.y;
        _maxZPosition = MaxZPoint.position.z - transform.position.y;
    }

    private void LateUpdate()
    {
        if (_movSpeed != 0)
        {
            Movement();
            Zoom();
        }


#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.P) && _movSpeed == 20f)
            _movSpeed = 0f;
        else if(Input.GetKeyDown(KeyCode.P) && _movSpeed == 0f)
            _movSpeed = 20f;
#endif
    }

    private void Movement()
    {
        if (Input.GetKey(_moveUpKey) || Input.mousePosition.y >= Screen.height - _borderWidth)
        {
            transform.position += Vector3.forward * _movSpeed * Time.deltaTime;
        }

        if (Input.GetKey(_moveDownKey) || Input.mousePosition.y <= _borderWidth)
        {
            transform.position -= Vector3.forward * _movSpeed * Time.deltaTime;
        }

        if (Input.GetKey(_moveRightKey) || Input.mousePosition.x >= Screen.width - _borderWidth)
        {
            transform.position += Vector3.right * _movSpeed * Time.deltaTime;
        }

        if (Input.GetKey(_moveLeftKey) || Input.mousePosition.x <= _borderWidth)
        {
            transform.position -= Vector3.right * _movSpeed * Time.deltaTime;
        }

        transform.position = ClampCamera(transform.position);
    }

    private void Zoom()
    {
        if (Camera.main.orthographic)
        {
            if (Input.GetAxis("Mouse ScrollWheel") > 0f && Camera.main.orthographicSize > _zoomMin)
            {
                var temp = Camera.main.orthographicSize - _zoomSpeed * Time.deltaTime;

                if (temp > _zoomMin)
                    Camera.main.orthographicSize = temp;
                else
                    Camera.main.orthographicSize = _zoomMin;

                transform.position = ClampCamera(transform.position);
            }

            if (Input.GetAxis("Mouse ScrollWheel") < 0f && Camera.main.orthographicSize < _zoomMax)
            {
                var temp = Camera.main.orthographicSize + _zoomSpeed * Time.deltaTime;

                if (temp < _zoomMax)
                    Camera.main.orthographicSize = temp;
                else
                    Camera.main.orthographicSize = _zoomMax;

                transform.position = ClampCamera(transform.position);
            }
        }
        else
        {
            if (Input.GetAxis("Mouse ScrollWheel") > 0f && Camera.main.fieldOfView > _zoomMin)
            {
                var temp = Camera.main.fieldOfView - _zoomSpeed * Time.deltaTime;

                if (temp > _zoomMin)
                    Camera.main.fieldOfView = temp;
                else
                    Camera.main.fieldOfView = _zoomMin;
            }

            if (Input.GetAxis("Mouse ScrollWheel") < 0f && Camera.main.fieldOfView < _zoomMax)
            {
                var temp = Camera.main.fieldOfView + _zoomSpeed * Time.deltaTime;

                if (temp < _zoomMax)
                    Camera.main.fieldOfView = temp;
                else
                    Camera.main.fieldOfView = _zoomMax;
            }
        }
    }

    private Vector3 ClampCamera(Vector3 targetPosition)
    {
        float camHeight = Camera.main.orthographicSize;
        float camWidth = Camera.main.orthographicSize * Camera.main.aspect;

        float minX = _minXPosition + camWidth;
        float maxX = _maxXPosition - camWidth;
        float minZ = _minZPosition + camHeight;
        float maxZ = _maxZPosition - camHeight;

        float newX = Mathf.Clamp(targetPosition.x, minX, maxX);
        float newZ = Mathf.Clamp(targetPosition.z, minZ, maxZ);

        return new Vector3(newX, targetPosition.y, newZ);
    }

    public void MoveToPosition(Vector3 position)
    {
        transform.position = ClampCamera(position);
    }
}