﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using Network;
using Photon.Pun;
using Photon.Realtime;

public class MenuManager : MonoBehaviourPunCallbacks
{
    [Header("Panels")]
    public GameObject MainPanel;
    public GameObject SettingsPanel;
    public GameObject CreditsPanel;
    public GameObject LoginPanel;
    public GameObject LobbyPanel;
    public GameObject RoomPanel;

    [Header("Buttons")]
    public Button QuickPlayButton;

    [Header("DoTween Time")]
    public float ShowPanelTime;
    public float HidePanelTime;

    [Header("Sound Sliders")]
    public Slider MusicSlider;
    //public TMP_Text MusicValue;
    public Slider EffectsSlider;
    //public TMP_Text EffectsValue;

    private bool tutorial = false;

    private void Awake()
    {
        InitializePanels();
    }

    private void Start()
    {
        InitializeMusicSlider();
        InitializeSFXSlider();
    }

    void InitializePanels()
    {
        MainPanel.transform.localScale = Vector3.one;
        SettingsPanel.transform.localScale = Vector3.zero;
        CreditsPanel.transform.localScale = Vector3.zero;
    }

    #region PANELS

    public void ShowPanel(GameObject panel)
    {
        panel.transform.DOScale(Vector3.one, ShowPanelTime).
            SetEase(Ease.InQuad).Play();
    }

    public void HidePanel(GameObject panel)
    {
        panel.transform.DOScale(Vector3.zero, HidePanelTime).
            SetEase(Ease.InQuad).Play();
    }

    #endregion

    #region BUTTONS
    public void LoginButtonPressed()
    {
        LoadingUIManager.Instance.ShowLoadingScreen(true);
        NetworkManager.Instance.Login();
    }

    public void QuickPlayButtonPressed()
    {
        LoadingUIManager.Instance.ShowLoadingScreen(true);
        NetworkManager.Instance.QuickPlay();
    }

    public void TutorialButtonPressed()
    {
        LoadingUIManager.Instance.ShowLoadingScreen(true);
        tutorial = true;
        NetworkManager.Instance.CreateRoom();
    }

    #endregion

    #region SOUND

    private void InitializeMusicSlider()
    {
        MusicSlider.value = AudioManager.Instance.MusicVolume;
    }

    private void InitializeSFXSlider()
    {
        EffectsSlider.value = AudioManager.Instance.SFXVolume;
    }

    public void SetMusicSliderValue()
    {
        //int value = (int)(MusicSlider.value / 1f * 100f);
        AudioManager.Instance.MusicVolumeLevel(MusicSlider.value);
        //MusicValue.text =  value.ToString() + "%";
    }
    public void SetEffectsSliderValue()
    {
        //int value = (int)(EffectsSlider.value / 1f * 100f);
        AudioManager.Instance.SFXVolumeLevel(EffectsSlider.value);
        //EffectsValue.text = value.ToString() + "%";
    }

    #endregion

    #region PUN Callbacks

    public override void OnConnectedToMaster()
    {
        HidePanel(LoginPanel);
        ShowPanel(LobbyPanel);
        LoadingUIManager.Instance.ShowLoadingScreen(false);
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        HidePanel(LobbyPanel);
        ShowPanel(LoginPanel);
    }

    public override void OnJoinedRoom()
    {
        if (tutorial)
        {
            NetworkManager.Instance.LoadTutorial();
        }
        else
        {
            LoadingUIManager.Instance.ShowLoadingScreen(false);
            ShowPanel(RoomPanel);
        }
    }

    public override void OnLeftRoom()
    {
        HidePanel(RoomPanel);
    }

    #endregion

    public void LeaveRoom()
    {
        Debug.Log("Desconectar de la sala.");
    }

    public void LoadSceneByIndex(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
