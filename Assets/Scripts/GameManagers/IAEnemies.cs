﻿using System.Collections.Generic;
using UnityEngine;
using Utils;

public class IAEnemies : Singleton<IAEnemies>
{
    public List<GameObject> _gatherers, _warriors, _buildings;

    static int resourceTarget = 0;

    public List<GameObject> Gatherers { get => _gatherers; private set => this._gatherers = value; }
    public List<GameObject> Warriors { get => _warriors; private set => this._warriors = value; }
    public List<GameObject> Buildings { get => _buildings; private set => this._buildings = value; }

    public List<ResourcesList> ResourcesLists;

    public void Gather(Evolver evolver)
    {
        Debug.Log("Gather");
        if (ResourcesLists.Count > 0)
        {
            Debug.Log("Gather Do");
            if (resourceTarget == 0)
            {
                Debug.Log("Gather 0");
                resourceTarget++;
                if (ResourcesLists[evolver.Group].Energy.Count > 0)
                    evolver.MoveToTargetResource(ResourcesLists[evolver.Group].Energy[Random.Range(0, ResourcesLists[evolver.Group].Energy.Count)].transform);
                else
                    Gather(evolver);
            }
            else if (resourceTarget == 1)
            {
                Debug.Log("Gather 1");
                resourceTarget++;
                if (ResourcesLists[evolver.Group].Energy.Count > 0)
                    evolver.MoveToTargetResource(ResourcesLists[evolver.Group].Food[Random.Range(0, ResourcesLists[evolver.Group].Food.Count)].transform);
                else
                    Gather(evolver);
            }
            else
            {
                Debug.Log("Gather 2");
                resourceTarget = 0;
                if (ResourcesLists[evolver.Group].Energy.Count > 0)
                    evolver.MoveToTargetResource(ResourcesLists[evolver.Group].Biomass[Random.Range(0, ResourcesLists[evolver.Group].Biomass.Count)].transform);
                else
                    Gather(evolver);
            }
        }
    }

    private void Evolve()
    {

    }


}

[System.Serializable]
public class ResourcesList
{
    public List<GameObject> Energy;
    public List<GameObject> Food;
    public List<GameObject> Biomass;
}
