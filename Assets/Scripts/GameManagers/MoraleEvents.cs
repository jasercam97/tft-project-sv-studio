﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MoraleEvents
{
    TroopDeath, BuildingCaptured, BuildingLost, BuildingDestroyed
}
