﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace GameManagers {
    public class ParentManager : Singleton<ParentManager>
    {
        public Transform BuildParent;

        public void SetBuildParent(GameObject GO)
        {
            GO.transform.SetParent(BuildParent);
        }
    }
}
