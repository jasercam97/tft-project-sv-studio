﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utils;

public class MoraleController : Singleton<MoraleController>
{
    [Header("Morale")]
    public Image MoraleImage;
    public MoraleStats MoraleStatsMelee, MoraleStatsDistance, MoraleStatsAir;
    public float InitialMorale;
    public float InitialTimeToDecreaseMorale;
    public float DecreaseValuePerSecond;
    public float IncreaseValuePerAttack;
    private float _currentMorale;
    private float _currentTimeToDecreaseMorale;
    private float _minMorale = 0;

    private void Awake()
    {
        _currentMorale = InitialMorale;
        _currentTimeToDecreaseMorale = InitialTimeToDecreaseMorale;
        InvokeRepeating(nameof(DecreaseMorale), 1f, 1f);
    }

    public void DecreaseMorale()
    {
        _currentTimeToDecreaseMorale--;
        if (_currentTimeToDecreaseMorale < 0)
        {
            _currentMorale = Mathf.Max(_currentMorale - DecreaseValuePerSecond, _minMorale);
        }
        MoraleImage.fillAmount = _currentMorale / 100f;
    }

    public void SetMorale()
    {
        _currentTimeToDecreaseMorale = InitialTimeToDecreaseMorale;
        _currentMorale = Mathf.Min(_currentMorale + IncreaseValuePerAttack, 100);
    }

    public void UpdateMinMorale(float amount)
    {
        _minMorale += amount;
        if (_currentMorale < _minMorale)
        {
            _currentMorale = _minMorale;
        }
    }

    public float GetDamage(int type)
    {
        float damage = 0;
        switch (type)
        {
            case 0:
                damage = 1 + ((MoraleStatsMelee.MaxIncreaseDamage - 1) * (_currentMorale / 100));
                break;
            case 1:
                damage = 1 + ((MoraleStatsDistance.MaxIncreaseDamage - 1) * (_currentMorale / 100));
                break;
            case 2:
                damage = 1 + ((MoraleStatsAir.MaxIncreaseDamage - 1) * (_currentMorale / 100));
                break;
        }
        return damage;
    }

    public float GetSpeedAttack(int type)
    {
        float speedAttack = 0;
        switch (type)
        {
            case 0:
                speedAttack = 1 + ((MoraleStatsMelee.MaxIncreaseSpeedAttack - 1) * (_currentMorale / 100));
                break;
            case 1:
                speedAttack = 1 + ((MoraleStatsMelee.MaxIncreaseSpeedAttack - 1) * (_currentMorale / 100));
                break;
            case 2:
                speedAttack = 1 + ((MoraleStatsMelee.MaxIncreaseSpeedAttack - 1) * (_currentMorale / 100));
                break;
        }
        return speedAttack;
    }

    public float GetScale(int type)
    {
        float scale = 0;
        switch (type)
        {
            case 0:
                scale = 1 + ((MoraleStatsMelee.MaxIncreaseScale - 1) * (_currentMorale / 100));
                break;
            case 1:
                scale = 1 + ((MoraleStatsMelee.MaxIncreaseScale - 1) * (_currentMorale / 100));
                break;
            case 2:
                scale = 1 + ((MoraleStatsMelee.MaxIncreaseScale - 1) * (_currentMorale / 100));
                break;
        }
        return scale;
    }

    /*
    [Header("Team morale values")]
    public List<float> MoralePerTeam;
    private int _teamAmount;

    [Header("Event morale values")]
    public float TroopDeathValue;
    public float BuildingCapturedValue;
    public float BuildingDestroyedValue;

    [Header("Multiplier edge values")]
    public float MaxAttackDamageMultiplier;
    public float MinAttackDamageMultiplier;

    [Space(5)]
    public float MaxAttackSpeedMultiplier;
    public float MinAttackSpeedMultiplier;

    [Space(5)]
    public float MaxMoveSpeedMultiplier;
    public float MinMoveSpeedMultiplier;

    [Space(5)]
    public float MaxScaleMultiplier;
    public float MinScaleMultiplier;

    private void Start()
    {
        _teamAmount = TeamManager.Instance.TroopsPerTeam.Count;

        for (int i=0; i < _teamAmount; i++)
        {
            MoralePerTeam.Add(50);
        }
    }

    public void UpdateMorale(MoraleEvents moraleEvent, int allyTeam, int enemyTeam)
    {
        switch (moraleEvent)
        {
            case MoraleEvents.TroopDeath:
                DecreaseMorale(TroopDeathValue, allyTeam);
                IncreaseMorale(TroopDeathValue, enemyTeam);
                break;

            case MoraleEvents.BuildingCaptured:
                IncreaseMorale(BuildingCapturedValue, allyTeam);
                break;

            case MoraleEvents.BuildingLost:
                DecreaseMorale(BuildingCapturedValue, allyTeam);
                break;

            case MoraleEvents.BuildingDestroyed:
                DecreaseMorale(BuildingDestroyedValue, allyTeam);
                IncreaseMorale(BuildingDestroyedValue, enemyTeam);
                break;
        }
    }

    private void IncreaseMorale(float value, int team)
    {
        float currentMorale = MoralePerTeam[team];

        currentMorale += value;

        if (currentMorale > 100)
        {
            currentMorale = 100;
        }

        MoralePerTeam[team] = currentMorale;

        UpdateStats(team, currentMorale);
    }

    private void DecreaseMorale(float value, int team)
    {
        float currentMorale = MoralePerTeam[team];

        currentMorale -= value;

        if (currentMorale < 0)
        {
            currentMorale = 0;
        }

        MoralePerTeam[team] = currentMorale;

        UpdateStats(team, currentMorale);
    }

    private void UpdateStats(int team, float morale)
    {
        float newAttackDamageMultiplier = CalculateMultiplier(MinAttackDamageMultiplier, MaxAttackDamageMultiplier, morale);
        float newAttackSpeedMultiplier = CalculateMultiplier(MinAttackSpeedMultiplier, MaxAttackSpeedMultiplier, morale);
        float newMoveSpeedMultiplier = CalculateMultiplier(MinMoveSpeedMultiplier, MaxMoveSpeedMultiplier, morale);
        float newScaleMultiplier = CalculateMultiplier(MinScaleMultiplier, MaxScaleMultiplier, morale);

        foreach (Mover troop in TeamManager.Instance.TroopsPerTeam[team])
        {
            troop.GetComponent<AtributteManager>().UpdateAtributtes(newAttackDamageMultiplier, newAttackSpeedMultiplier, newMoveSpeedMultiplier, newScaleMultiplier);
        }
    }


    // Devuelve un valor entre minValue y maxValue en función de moraleValue
    private float CalculateMultiplier(float minValue, float maxValue, float moraleValue)
    {
        if (moraleValue == 50)
        {
            return 1;
        }
        else if (moraleValue > 50)
        {
            return 1 + ((maxValue - 1) * (moraleValue - 50) / 50);
        }
        else
        {
            return minValue + ((1 - minValue) * moraleValue / 50);
        }
    }

    public float GetAttackDamageMultiplier(int team)
    {
        return CalculateMultiplier(MinAttackDamageMultiplier, MaxAttackDamageMultiplier, MoralePerTeam[team]);
    }

    public float GetAttackSpeedMultiplier(int team)
    {
        return CalculateMultiplier(MinAttackSpeedMultiplier, MaxAttackSpeedMultiplier, MoralePerTeam[team]);
    }

    public float GetMoveSpeedMultiplier(int team)
    {
        return CalculateMultiplier(MinMoveSpeedMultiplier, MaxMoveSpeedMultiplier, MoralePerTeam[team]);
    }

    public float GetScaleMultiplier(int team)
    {
        return CalculateMultiplier(MinScaleMultiplier, MaxScaleMultiplier, MoralePerTeam[team]);
    }

    // TODO quitar, solo para testeo
    private void OnValidate()
    {
       // UpdateStats(0, MoralePerTeam[0]);
       // UpdateStats(1, MoralePerTeam[1]);
    }*/

#if UNITY_EDITOR
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            _currentMorale += 20;
        }
    }
#endif
}

[System.Serializable]
public class MoraleStats
{

    public float MaxIncreaseDamage;
    public float MaxIncreaseSpeedAttack;
    public float MaxIncreaseScale;
}