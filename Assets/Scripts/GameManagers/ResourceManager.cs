﻿using BuildPlacement;
using GameManagers;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Utils;

public class ResourceManager : Singleton<ResourceManager>
{
    [SerializeField]
    private TextMeshProUGUI _energyText;
    [SerializeField]
    private TextMeshProUGUI _biomassText;
    [SerializeField]
    private TextMeshProUGUI _foodText;

    [SerializeField] private float _energy = 0;
    [SerializeField] private float _biomass = 0;
    [SerializeField] private float _food = 0;

    public List<BuildButton> listOfButtons;
    public bool isTutorial = false;

    public float Biomass
    {
        get { return _biomass; }
        set
        {
            _biomass = value; _biomassText.text = _biomass.ToString();
            SetBuildButtons();
            if (isTutorial)
                ResourceTrigger.Instance.RegisterResourceChange(Food,Biomass,Energy);
        }
    }

    public float Food
    {
        get { return _food; }
        set
        {
            _food = value; _foodText.text = _food.ToString();
            SetBuildButtons();
            if (isTutorial)
                ResourceTrigger.Instance.RegisterResourceChange(Food, Biomass, Energy);
        }
    }

    public float Energy
    {
        get { return _energy; }
        set
        {
            _energy = value; _energyText.text = _energy.ToString();
            SetBuildButtons();
            if (isTutorial)
                ResourceTrigger.Instance.RegisterResourceChange(Food, Biomass, Energy);
        }
    }

    public void SetBuildButtons()
    {
        
        foreach(BuildButton button in listOfButtons)
        {
            button.SetButtonState();
        }

        CanvasManager.Instance.SetBuildUpgradeButtonsState();
        CanvasManager.Instance.SetTurretUpgradeButtonsState();
    }

#if UNITY_EDITOR
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            Energy += 20;
        }

        if (Input.GetKeyDown(KeyCode.B))
        {
            Biomass += 20;
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            Food += 20;
        }
    }
#endif
}
