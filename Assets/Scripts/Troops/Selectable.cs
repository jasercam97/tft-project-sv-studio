using System;
using System.Collections.Generic;
using Build;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public abstract class Selectable : MonoBehaviourPunCallbacks, IPunOwnershipCallbacks
{
    #region ONLINE

    public GameObject _visionInFog;
    public GameObject _visionInMinimap;
    public SpriteRenderer _miniatureMinimap;

    public int Group = -1;
    public int Team = -1;
    public bool ready = false;

    #endregion

    public bool IsFixed = false;

    public bool IsDead = false;

    [SerializeField] protected List<string> _animatorsPath;
    [SerializeField] protected Animator _animator;
    [SerializeField] protected List<GameObject> DeadTroopList;
    [SerializeField] protected GameObject Selected;
    [SerializeField] public Transform RenderTroop;

    protected int _selectedDeathAnim = 0;
    protected bool _isSelected;
    protected bool _isBuildingNotTurret;
    BuildBehaviour buildNotTurret;
    [HideInInspector] public AtributteManager _atributteManager;
    private Color orange;

    protected virtual void Awake()
    {
        _atributteManager = GetComponent<AtributteManager>();
        orange = new Color(1F, 0.7F, 0F);
        //PhotonNetwork.AddCallbackTarget(this);
        if (!IsFixed)
            photonView.RequestOwnership();

        _isBuildingNotTurret = TryGetComponent<BuildBehaviour>(out BuildBehaviour b) && !TryGetComponent<Turret>(out _);
        if (_isBuildingNotTurret)
            buildNotTurret = b;
    }

    private void OnDestroy()
    {
        SelectorRTS.Instance?.Selectables.Remove(gameObject);
        //PhotonNetwork.RemoveCallbackTarget(this); 
    }

    public bool IsOwned()
    {
        return Group == PhotonNetwork.LocalPlayer.GetPlayerNumber();
    }

    private void SetMinimapColor()
    {
        switch (Group)
        {
            case 0:
                if (_miniatureMinimap != null) _miniatureMinimap.color = orange;
                break;
            case 1:
                if (_miniatureMinimap != null) _miniatureMinimap.color = Color.red;
                break;
            case 2:
                if (_miniatureMinimap != null) _miniatureMinimap.color = Color.blue;
                break;
            case 3:
                if (_miniatureMinimap != null) _miniatureMinimap.color = Color.yellow;
                break;
        }
    }

    public void SetSelected(bool visible)
    {
        Debug.Log("SetSelected");
        _isSelected = visible;
        if (Selected != null)
        {
            Selected.SetActive(_isSelected);
            Debug.Log("Selected");
        }

        if (_isBuildingNotTurret && SelectorRTS.Instance.TroopsSelected.Count == 0) { 
            buildNotTurret.SelectBuild(_isSelected);
            Debug.Log("SelectBuild");
        }
        else if (_isBuildingNotTurret)
        {
            buildNotTurret.SelectBuild(false);
            Invoke(nameof(RemoveFromList), 0.1f);
            Debug.Log("Remove Build");
        }
    }

    private void RemoveFromList()
    {
        SelectorRTS.Instance.ObjectsSelected.Remove(this.gameObject);
        SelectorRTS.Instance.BuildingsSelected.Remove(this.gameObject);
    }

    public virtual void TakeDamage(float amount)
    {
        photonView.RPC("RPC_TakeDamage", RpcTarget.AllViaServer, amount);
    }

    [PunRPC]
    public void RPC_TakeDamage(float amount)
    {
        _atributteManager.TakeDamage(amount);
        Debug.Log("TakeDamage: "+amount);
    }

    public void PreviousDie()
    {
        photonView.RPC(nameof(RPC_Die), RpcTarget.AllViaServer);
    }

    [PunRPC]
    public void RPC_Die()
    {
        Die();
    }

    public virtual void Die()
    {
        if (photonView.IsMine)
        {
            SelectorRTS.Instance.Selectables.Remove(gameObject);
            if (SelectorRTS.Instance.ObjectsSelected.Contains(gameObject))
                SelectorRTS.Instance.ObjectsSelected.Remove(gameObject);
            IsDead = true;
            SetSelected(false);
        }

        _animator.SetBool("IsMoving", false);

        _selectedDeathAnim = UnityEngine.Random.Range(0, DeadTroopList.Count);
        _animator.SetInteger("DeathAnim", _selectedDeathAnim);
        _animator.SetTrigger("Death");

        Invoke(nameof(BecomeCorpse), 1);

    }

    protected virtual void BecomeCorpse()
    {
        if (photonView.IsMine)
            PhotonNetwork.Destroy(gameObject);
    }

    public void UpdateAtributtes()
    {
        _atributteManager.UpdateAtributtes(1,
                             1,
                             1,
                             1);
    }

    [PunRPC]
    public void RPC_Initialize()
    {
        Initialize();
    }

    public virtual void Initialize()
    {
        Debug.Log("Initialize");
        if (_animatorsPath.Count == 4)
        {
            _animator.runtimeAnimatorController = Resources.Load(_animatorsPath[Group]) as RuntimeAnimatorController;
        }
        var visibleInsideMask = GetComponent<VisibleInsideMask>();
        if (photonView.IsMine)
        {
            Debug.Log("Ini Selectable");
            SelectorRTS.Instance.Selectables.Add(gameObject);
            visibleInsideMask.DestroyVisibleInsideMask();
            ready = true;
            if (Group <= 4)
            {
                if (_visionInFog != null) _visionInFog.SetActive(true);
                if (_visionInMinimap != null) _visionInMinimap.SetActive(true);
            }
        }
        else
        {
            visibleInsideMask.Initialize(Team);
        }

        

        Invoke(nameof(UpdateAtributtes), 1f);
        SetMinimapColor();
    }

    public void SetGroupAndTeam(int group, int team)
    {
        Debug.Log("setGroup");
        if (photonView.IsMine)
        {
            Debug.Log("setGroupisMine");
            photonView.RPC(nameof(RPCGroupAndTeam), RpcTarget.AllViaServer, group, team);
        }

    }

    [PunRPC]
    public void RPCGroupAndTeam(int group, int team)
    {
        if (Group == -1 && Team == -1)
        {
            Group = group;
            Team = team;
        }
        Initialize();
        if(gameObject.TryGetComponent<BuildBehaviour>(out BuildBehaviour buildBehaviour))
        {
            buildBehaviour.SetGroup(Group);
        }
    }

    public void OnOwnershipRequest(PhotonView targetView, Player requestingPlayer)
    {
        if (targetView != photonView)
            return;

        photonView.TransferOwnership(requestingPlayer);
    }

    public void OnOwnershipTransfered(PhotonView targetView, Player previousOwner)
    {
        if (targetView != photonView)
            return;

        Initialize();
    }

    public void OnOwnershipTransferFailed(PhotonView targetView, Player senderOfFailedRequest)
    {
        Debug.Log("Failed Transfer OwnerShip");
    }
}