﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewAtributte", menuName = "Scriptables/Atributtes")]
public class AtributtesSO : ScriptableObject
{
    //SELECTABLE
    [SerializeField] private string _name;

    public string Name { get => _name; private set => this._name = value; }

    [SerializeField] private Sprite _image;

    public Sprite Image { get => _image; private set => this._image = value; }

    [SerializeField] private string _description;

    public string Description { get => _description; private set => this._description = value; }

    [SerializeField] private string _littleDescription;

    public string LittleDescription { get => _littleDescription; private set => this._littleDescription = value; }

    [SerializeField] private float _maxHealth;

    public float MaxHealth { get => _maxHealth; private set => this._maxHealth = value; }

    [SerializeField] private float _regenerationHealth;

    public float RegenerationHealth { get => _regenerationHealth; private set => this._regenerationHealth = value; }

    //MOVER
    [SerializeField] private float _moveSpeed;

    public float MoveSpeed { get => _moveSpeed; private set => this._moveSpeed = value; }

    [SerializeField] private float _damage;

    //WARRIOR && TURRET

    public float Damage { get => _damage; set => this._damage = value; }

    [SerializeField] private float _attackSpeed;

    public float AttackSpeed { get => _attackSpeed; private set => this._attackSpeed = value; }

    [SerializeField] private float _range;

    public float AttackRange { get => _range; private set => this._range = value; }

    [SerializeField] private float _projectileSpeed;

    public float ProjectileSpeed { get => _projectileSpeed; private set => this._projectileSpeed = value; }

    //GATHERER

    [SerializeField] private float _energyTake;

    public float EnergyTake { get => _energyTake; set => this._energyTake = value; }

    [SerializeField] private float _biomassTake;

    public float BiomassTake { get => _biomassTake; private set => this._biomassTake = value; }

    [SerializeField] private float _foodTake;

    public float FoodTake { get => _foodTake; private set => this._foodTake = value; }

    [SerializeField] private float _distanceToCollectionPoint;

    public float DistanceToCollectionPoint { get => _distanceToCollectionPoint; private set => this._distanceToCollectionPoint = value; }

    //BUILD

    [SerializeField] private float _costNormalEnergy;

    public float CostNormalEnergy { get => _costNormalEnergy; set => this._costNormalEnergy = value; }

    [SerializeField] private float _costNormalBiomass;

    public float CostNormalBiomass { get => _costNormalBiomass; set => this._costNormalBiomass = value; }

    [SerializeField] private float _costNormalFood;

    public float CostNormalFood { get => _costNormalFood; set => this._costNormalFood = value; }

    [SerializeField] private float _evolutionNormalTime;

    public float EvolutionNormalTime { get => _evolutionNormalTime; set => this._evolutionNormalTime = value; }

    [SerializeField] private GameObject _normalTroop;

    public GameObject NormalTroop { get => _normalTroop; set => this._normalTroop = value; }


    [SerializeField] private float _costUpgradedEnergy;

    public float CostUpgradedEnergy { get => _costUpgradedEnergy; set => this._costUpgradedEnergy = value; }

    [SerializeField] private float _costUpgradedBiomass;

    public float CostUpgradedBiomass { get => _costUpgradedBiomass; set => this._costUpgradedBiomass = value; }

    [SerializeField] private float _costUpgradedFood;

    public float CostUpgradedFood { get => _costUpgradedFood; set => this._costUpgradedFood = value; }

    [SerializeField] private float _evolutionUpgradedTime;

    public float EvolutionUpgradedTime { get => _evolutionUpgradedTime; set => this._evolutionUpgradedTime = value; }

    [SerializeField] private GameObject _upgradedTroop;

    public GameObject UpgradedTroop { get => _upgradedTroop; set => this._upgradedTroop = value; }
}
