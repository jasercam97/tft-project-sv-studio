﻿using Build;
using Photon.Pun;
using UnityEngine;
using UnityEngine.AI;

public class Evolver : Gatherer
{
    [Header("Evolution")]
    public float QueueStoppingDistance;

    private Transform _targetBuilding = null;
    private TroopEvolver _targetEvolver = null;

    public override void Initialize()
    {
        base.Initialize();

        /*if (Group != 0)
        {
            IAEnemies.Instance?.Gatherers.Add(gameObject);
            IAEnemies.Instance?.Gather(this);
        }*/
    }

    /*protected override void OnDisable()
    {
        if (Group != 0)
        {
            IAEnemies.Instance?.Gatherers.Remove(gameObject);
        }
        else
            base.OnDisable();
    }*/

    protected override void Update()
    {
        if (photonView.IsMine)
        {
            if (!IsDead)
            {
                base.Update();
                CheckOnDestination();
            }
        }
    }

    public void MoveToEvolvePoint()
    {
        if (_targetEvolver != null)
        {
            DoShrinkTween();

            _targetEvolver.SetFull();
            SelectorRTS.Instance.ClearTroop(gameObject);
            _agent.stoppingDistance = MovementStoppingDistance;
            _agent.destination = _targetEvolver.SpawnPoint.position;

            Invoke(nameof(Evolve), 1);
        }
    }

    private void CheckEvolverFree()
    {
        if (_targetBuilding.GetComponent<TroopEvolver>() != null && _targetBuilding.GetComponent<BuildBehaviour>()._isBuilt)
        {
            _targetEvolver = _targetBuilding.GetComponent<TroopEvolver>();

            if (_targetEvolver.IsFull)
            {
                _targetEvolver.AddToQueue(this);
            }
            else
            {
                MoveToEvolvePoint();
            }
        }
    }

    private void Evolve()
    {
        _targetEvolver.StartEvolution(Group, Team);
        //MoraleController.Instance.RemoveTroop(IsEnemy, this);

        _targetBuilding = null;
        _targetEvolver = null;
        Die();
        //PhotonNetwork.Destroy(gameObject);
    }

    public override void MoveToTarget(Vector3 target)
    {
        base.MoveToTarget(target);

        _isCollecting = false;
        _targetBuilding = null;

        if (_targetEvolver != null)
        {
            _targetEvolver.RemoveFromQueue(this);
            _targetEvolver = null;
        }
    }

    public void MoveToBuildEvolver(Transform target)
    {
        _targetBuilding = null;

        if (_targetEvolver != null)
        {
            _targetEvolver.RemoveFromQueue(this);
            _targetEvolver = null;
        }

        _targetBuilding = target;
        _agent.stoppingDistance = QueueStoppingDistance;

        if (_targetBuilding.GetComponent<TroopEvolver>() != null)
        {
            _agent.destination = _targetBuilding.GetComponent<TroopEvolver>().SpawnPoint.position;
        }
        else
        {
            NavMeshHit hit;
            if (NavMesh.SamplePosition(_targetBuilding.position, out hit, Mathf.Infinity, NavMesh.AllAreas))
            {
                _agent.destination = hit.position;
            }
        }
    }

    private void CheckOnDestination()
    {  
        if (!_agent.pathPending && _targetBuilding != null && _agent.remainingDistance < QueueStoppingDistance)
        {
            CheckEvolverFree();
        }
    }
}
