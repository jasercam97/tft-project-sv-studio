using System;
using System.Collections;
using System.Collections.Generic;
using Build;
using UnityEngine;

public class QueenRange : MonoBehaviour
{
    
    [Header("Range Zone")]
    [Range(0, 50)]
    public int Segments = 50;
    public float Width = 0.5f;
    public float Range;
    
    private LineRenderer _line;
    [HideInInspector]public bool canBuild = false;

    private void Awake()
    {
        _line = GetComponent<LineRenderer>();
    }

    private void Start()
    {
        _line.positionCount = Segments + 1;
        _line.useWorldSpace = false;
        _line.startWidth = Width;
        _line.endWidth = Width;
        
        //_line.enabled = false;
        
        CreatePoints();
    }
    
    public void ShowRange(bool state)
    {
        _line.enabled = state;
        if(!state)
            canBuild = false;
    }
    
    void CreatePoints()
    {
        float x;
        float y;

        float angle = 20f;

        for (int i = 0; i < (Segments + 1); i++)
        {
            x = Mathf.Sin(Mathf.Deg2Rad * angle) * Range;
            y = Mathf.Cos(Mathf.Deg2Rad * angle) * Range;

            _line.SetPosition(i, new Vector3(x, y, 0));

            angle += (360f / Segments);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent<BuildBlueprint>(out _))
            canBuild = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.TryGetComponent<BuildBlueprint>(out _))
            canBuild = false;
    }
}
