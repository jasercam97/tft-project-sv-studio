﻿public enum TroopType
{
    Gatherer, Melee, Ranged, Flying
}
