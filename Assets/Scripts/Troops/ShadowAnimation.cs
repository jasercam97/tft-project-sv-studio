﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ShadowAnimation : MonoBehaviour
{
    public float Strengh = 1;
    public int Vibrato = 10;
    public float Randomness = 90;

    private void Start()
    {
        transform.DOShakeScale(1, Strengh, Vibrato, Randomness).SetLoops(-1).Play();
    }
}
