using System.Collections;
using System.Collections.Generic;
using Network;
using UnityEngine;

public class Warrior : Mover
{
    public enum WarriorType {Melee = 0, Air = 1, Distance = 2};
    public WarriorType warriorType;
    //Attack
    private Vector3 _attackDirection;

    public Transform _followedTarget;
    private Selectable _currentTroop;

    [Header("Combat")]
    public float DetectionRange;    // TODO Este rango debería ser un poco más grande que el rango de ataque más amplio
    public LayerMask TroopLayer;
    public float DamageDelay;   // Delay del daño por la animación de ataque
    public bool _isHitting = false;
    [HideInInspector] public bool _ignoreEnemies = true;
    public List<Transform> _detectedTroops;

    protected override void Awake()
    {
        base.Awake();
        _detectedTroops = new List<Transform>();
    }

    public override void Initialize()
    {
        base.Initialize();
        if (photonView.IsMine)
        {
            InvokeRepeating(nameof(UpdateAtributtesWarrior), 0f, 0.3f);
        }
    }

    protected override void Update()
    {
        if (photonView.IsMine)
        {
            if (!IsDead && ready)
            {
                base.Update();
                
                if (!_ignoreEnemies)
                {
                    DetectEnemies();

                    if (_isAttacking && !CheckFollowedTargetDeath())
                    {
                        MoveToAttackRange(_followedTarget.position);
                    }
                }
                else if (!IsInvoking(nameof(IsIdle)))
                    Invoke(nameof(IsIdle), 0.1f);

            }
        }
    }

    public void IsIdle()
    {
        if (photonView.IsMine)
        {
            if(_agent.velocity == Vector3.zero)
                _ignoreEnemies = false;
        }
    }

    public override void MoveToTarget(Vector3 target)
    {
        base.MoveToTarget(target);

        CancelAttack();
        _followedTarget = null;
    }

    #region DETECTION ENEMIES
    protected void DetectEnemies()
    {
        if (_followedTarget == null)
        {
            _isAttacking = true;
            _detectedTroops.Clear();

            foreach (Collider troop in Physics.OverlapSphere(transform.position, DetectionRange, TroopLayer))
            {
                if (troop.gameObject.activeInHierarchy)
                {
                    _currentTroop = troop.GetComponent<Selectable>();

                    if (_currentTroop != null && _currentTroop.Team != this.Team && _currentTroop.Group != -1 && !_currentTroop.IsDead)    // Detects troops only if they are in opposite Groups and alive
                    {
                        if (warriorType == WarriorType.Melee)
                        {
                            if (troop.TryGetComponent(out Warrior warrior))
                            {
                                if(warrior.warriorType != WarriorType.Air)
                                    _detectedTroops.Add(troop.transform);
                            }
                            else
                            {
                                _detectedTroops.Add(troop.transform);
                            }
                        }
                        else
                            _detectedTroops.Add(troop.transform);
                    }
                }
            }

            if (_detectedTroops.Count == 1) // If only 1 troop is detected, moves to its position
            {
                _followedTarget = _detectedTroops[0];
                MoveToAttackRange(_followedTarget.position);
            }
            else if (_detectedTroops.Count > 1) // If multiple troops are detected, finds the closest one
            {
                _followedTarget = FindClosestEnemy();
                MoveToAttackRange(_followedTarget.position);
            }
            else
            {
                _followedTarget = null;
                _isAttacking = false;
                CancelAttack();
            }
        }
    }

    private Transform FindClosestEnemy()
    {
        Transform closestEnemy = _detectedTroops[0];
        float minDistance = Vector3.Distance(transform.position, closestEnemy.position);

        Transform currentTroop;
        float distanceToTroop;

        for (int i = 1; i < _detectedTroops.Count; i++)
        {
            currentTroop = _detectedTroops[i];
            distanceToTroop = Vector3.Distance(transform.position, currentTroop.position);

            if (distanceToTroop < minDistance)
            {
                minDistance = distanceToTroop;
                closestEnemy = currentTroop;
            }
        }

        return closestEnemy;
    }
    #endregion

    #region ATTACK
    private void MoveToAttackRange(Vector3 target)
    {
        if (!_isHitting)
        {
            _agent.destination = target;
            _agent.stoppingDistance = _atributteManager._attackRange;
        }

        CheckDistanceToTarget();
    }

    private void CheckDistanceToTarget()
    {
        if (_followedTarget != null)
        {
            if (!_isHitting)
            {
                float attackDistance = _atributteManager._attackRange;

                if (_followedTarget.GetComponent<Building>() != null)
                {
                    attackDistance += 3f;
                }

                if (Vector3.Distance(transform.position, _followedTarget.position) <= attackDistance)
                {
                    DoAttack();
                }
                else
                {
                    CancelAttack();
                }
            }
        }
        else
        {
            _followedTarget = null;
            CancelAttack();
        }
    }

    protected void DoDamage()
    {
        if (_followedTarget != null)
        {
            MoraleController.Instance.SetMorale();

            _followedTarget.GetComponent<Selectable>().TakeDamage(_atributteManager._attackDamage);
            AudioManager.Instance.PlayHormigaAttackSound();
        }
    }

    private void DoShoot()
    {
        if (_followedTarget != null)
        {
            MoraleController.Instance.SetMorale();

            GetComponent<ShootController>().Fire(Group, Team, _atributteManager._attackDamage, _followedTarget.GetComponent<Selectable>().RenderTroop, false, _atributteManager._projectileSpeed);

            if (warriorType == WarriorType.Distance)
            {
                AudioManager.Instance.PlayBabosaAttackSound();
            }
            else if (warriorType == WarriorType.Air)
            {
                AudioManager.Instance.PlayHormigaAttackSound();
            }
        }
    }

    protected void DoAttack()
    {
        if (_followedTarget != null && !_isHitting)
        {
            Invoke(nameof(EnableAttack), _atributteManager._attackSpeed);
            _isHitting = true;

            CheckAttackDirection(); // Change animation state
            _animator.SetTrigger("Attack");

            if (warriorType == WarriorType.Melee)
            {
                Invoke(nameof(DoDamage), DamageDelay); // Small delay because of animation
            }
            else if(warriorType == WarriorType.Distance)
            {
                Invoke(nameof(DoShoot), DamageDelay);
            }
            else if (warriorType == WarriorType.Air)
            {
                Invoke(nameof(DoShoot), DamageDelay);
            }
        }
    }

    protected void CheckAttackDirection()
    {
        _attackDirection = _followedTarget.position - transform.position;

        _animator.SetFloat("H", _attackDirection.normalized.x);
        _animator.SetFloat("V", _attackDirection.normalized.z);
    }

    private bool CheckFollowedTargetDeath()
    {
        if (_followedTarget.GetComponent<AtributteManager>()._currentHealth <= 0)
        {
            _followedTarget = null;
            CancelAttack();

            return true;
        }

        return false;
    }

    private void CancelAttack()
    {
        CancelInvoke(nameof(DoAttack));
        CancelInvoke(nameof(DoDamage));
        CancelInvoke(nameof(DoShoot));
    }

    private void EnableAttack()
    {
        _isHitting = false;
    }
    #endregion

    #region DIE
    public override void Die()
    {
        CancelAttack();
        base.Die();

        if (warriorType == WarriorType.Melee)
        {
            AudioManager.Instance.PlayHormigaDeathSound();
        }
        else if (warriorType == WarriorType.Distance)
        {
            AudioManager.Instance.PlayLarvaDeathSound();
        }
        else if (warriorType == WarriorType.Air)
        {
            AudioManager.Instance.PlayMoscaDeathSound();
        }
    }

    protected override void BecomeCorpse()
    {
        base.BecomeCorpse();
        //Instantiate(DeadTroopList[_selectedDeathAnim], RenderTroop.position, RenderTroop.rotation);
    }
    #endregion

    //FOCUS
    public void MoveToEnemy(Transform target)
    {
        if (!_isHitting)
        {
            _followedTarget = target;
            _ignoreEnemies = false;
        }
    }

    public void UpdateAtributtesWarrior()
    {
        _atributteManager.UpdateAtributtes(MoraleController.Instance.GetDamage((int)warriorType),
                            MoraleController.Instance.GetSpeedAttack((int)warriorType),
                            1,
                            MoraleController.Instance.GetScale((int)warriorType));
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, DetectionRange);
    }
}
