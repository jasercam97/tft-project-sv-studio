﻿using UnityEngine;
using UnityEngine.AI;

public class Gatherer : Mover
{
    private enum ResourcesType { Biomass, Energy, Food }
    private ResourcesType _currentResourcesType;

    public Transform CollectionPoint;
    private bool _resourcesGiven = true;

    [HideInInspector] public Transform _currentResourceTarget;

    public override void Initialize()
    {
        base.Initialize();
        if (photonView.IsMine)
        {
            CollectionPoint = TeamManager.Instance.Queens[Group].gameObject.transform;
        }
    }

    protected override void Update()
    {
        if (photonView.IsMine)
        {
            if (!IsDead)
            {
                base.Update();
                if (_isCollecting)
                {
                    Collection();
                }
                else if (_currentResourceTarget != null)
                {
                    _currentResourceTarget.GetComponent<Resource>().GathererFocus.Remove(this.gameObject);
                }
            }
        }
    }

    private bool CheckArriveCollectionPoint()
    {
        if (Vector3.Distance(transform.position,CollectionPoint.position) < _atributteManager._distanceToCollectionPoint)
        {
            return true;
        }
        return false;
    }

    private bool CheckArriveResource()
    {
        if (Vector3.Distance(transform.position, _currentResourceTarget.position) < _atributteManager._distanceToCollectionPoint)
        {
            return true;
        }
        return false;
    }

    private void CheckType()
    {
        if (_currentResourceTarget.GetComponent<Resource>()._type == 0)
            _currentResourcesType = ResourcesType.Biomass;
        else if (_currentResourceTarget.GetComponent<Resource>()._type == 1)
            _currentResourcesType = ResourcesType.Energy;
        else
            _currentResourcesType = ResourcesType.Food;
    }

    private void Collection()
    {
        if (_currentResourceTarget == null)
        {
            _isCollecting = false;
            NavMeshHit hit;
            if (NavMesh.SamplePosition(CollectionPoint.position, out hit, Mathf.Infinity, NavMesh.AllAreas))
            {
                _agent.destination = hit.position + Vector3.right * Random.Range(-6f, 6f) + Vector3.forward * Random.Range(-6f, 6f);
            }
        }
        else if (!_resourcesGiven)
        {
            NavMeshHit hit;
            if (NavMesh.SamplePosition(CollectionPoint.position, out hit, Mathf.Infinity, NavMesh.AllAreas))
            {
                _agent.destination = hit.position;
            }

            if (CheckArriveCollectionPoint())
            {
                    if (_currentResourcesType == ResourcesType.Biomass)
                        ResourceManager.Instance.GetComponent<ResourceManager>().Biomass += _atributteManager._biomassTake;
                    else if (_currentResourcesType == ResourcesType.Energy)
                        ResourceManager.Instance.GetComponent<ResourceManager>().Energy += _atributteManager._energyTake;
                    else
                        ResourceManager.Instance.GetComponent<ResourceManager>().Food += _atributteManager._foodTake;


                _resourcesGiven = true;
            }
        }
        else
        {
            NavMeshHit hit;
            if (NavMesh.SamplePosition(_currentResourceTarget.position, out hit, Mathf.Infinity, NavMesh.AllAreas))
            {
                _agent.destination = hit.position;
            }

            if (CheckArriveResource())
            {
                _resourcesGiven = false;
                AudioManager.Instance.PlayLarvaEatSound();

                CheckType();

                    if (_currentResourcesType == ResourcesType.Energy)
                        _currentResourceTarget.GetComponent<Resource>().Value = _atributteManager._energyTake;
                    else if (_currentResourcesType == ResourcesType.Biomass)
                        _currentResourceTarget.GetComponent<Resource>().Value = _atributteManager._biomassTake;
                    else
                        _currentResourceTarget.GetComponent<Resource>().Value = _atributteManager._foodTake;
            }
        }
    }

    public void MoveToTargetResource(Transform target)
    {
        if (_resourcesGiven)
        {
            _isCollecting = true;
            _currentResourceTarget = target;
            _currentResourceTarget.GetComponent<Resource>().GathererFocus.Add(this.gameObject);
        }
        else
        {
            _isCollecting = true;
            if(_currentResourceTarget!=null)
                _currentResourceTarget.GetComponent<Resource>().GathererFocus.Remove(this.gameObject);
            _currentResourceTarget = target;
            _currentResourceTarget.GetComponent<Resource>().GathererFocus.Add(this.gameObject);

            if ((_currentResourceTarget.GetComponent<Resource>()._type == 0 && _currentResourcesType != ResourcesType.Biomass) ||
                (_currentResourceTarget.GetComponent<Resource>()._type == 1 && _currentResourcesType != ResourcesType.Energy) ||
                (_currentResourceTarget.GetComponent<Resource>()._type == 2 && _currentResourcesType != ResourcesType.Food))
            {
                _resourcesGiven = true;
            }
            else
            {
                _resourcesGiven = false;
            }
        }
    }

    public void MoveToTargetCollectionPoint(Transform target)
    {
        if (!_resourcesGiven)
        {
            _isCollecting = true;
        }
        else
        {
            _isCollecting = false;
            NavMeshHit hit;
            if (NavMesh.SamplePosition(CollectionPoint.position, out hit, Mathf.Infinity, NavMesh.AllAreas))
            {
                _agent.destination = hit.position;
            }
        }
    }

    public override void Die()
    {
        base.Die();

        AudioManager.Instance.PlayLarvaDeathSound();
    }
}
