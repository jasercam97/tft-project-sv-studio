﻿using Photon.Pun;
using UnityEngine;
using UnityEngine.AI;

public class TroopGenerator : MonoBehaviour
{
    public float TimeToGenerate;
    public Transform SpawnPoint;
    public float MaxSpawnVariance;

    public GameObject TroopPrefab;
    [SerializeField] private Animator _animator;

    private Queen _queen;
    private Mover _currentTroop;
    private Vector3 _spawnPosition;
    private int _minTroops = 5;

    private void Awake()
    {
        _queen = GetComponent<Queen>();

        if(_queen.photonView.IsMine)
            InvokeRepeating(nameof(TryGenerateTroop), 1f, TimeToGenerate);
    }

    private void TryGenerateTroop()
    {
        if (TeamManager.Instance.TroopsPerGroup[_queen.Group].Count <= ResourceManager.Instance.Food || TeamManager.Instance.TroopsPerGroup[_queen.Group].Count <= 5)
        {
            GenerateTroop();
        }
    }

    private void GenerateTroop()
    {
        _animator.SetTrigger("CreateLarve");
        GameObject GO = PhotonNetwork.Instantiate(TroopPrefab.name, transform.position, transform.rotation, 0);
        if (GO.TryGetComponent<Selectable>(out Selectable selectable0)) selectable0.SetGroupAndTeam(_queen.Group, _queen.Team);
        _currentTroop = GO.GetComponent<Mover>();

        NavMeshHit hit;
        if (NavMesh.SamplePosition(SpawnPoint.position, out hit, Mathf.Infinity, NavMesh.AllAreas))
        {
            _currentTroop.transform.position = hit.position;
        }
        else
        {
            _currentTroop.transform.position = SpawnPoint.position;
        }

        _spawnPosition = new Vector3(hit.position.x + Random.Range(-MaxSpawnVariance, MaxSpawnVariance), hit.position.y, hit.position.z + Random.Range(-MaxSpawnVariance, 0));
        _currentTroop.gameObject.GetComponent<Mover>().MoveToTarget(_spawnPosition);
    }
}
