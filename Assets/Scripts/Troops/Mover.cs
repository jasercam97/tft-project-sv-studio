using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;
using System.Collections;

public abstract class Mover : Selectable
{
    [SerializeField] protected NavMeshAgent _agent;

    protected float MovementStoppingDistance;

    [HideInInspector] public bool _isCollecting = false, _isAttacking = false;

    [SerializeField]
    [Range(1f, 10f)] public float Duration;

    protected virtual void Update()
    {
        if (photonView.IsMine)
        {
            CheckMovement();
        }
        if (_agent.isOnOffMeshLink)
        {
            StartCoroutine(Parabola(_agent, 2.0f, Duration / 10));
            _agent.CompleteOffMeshLink();
        }
    }

    protected void CheckMovement()
    {
        if (Mathf.Abs(_agent.velocity.normalized.x) < 0.1f && Mathf.Abs(_agent.velocity.normalized.z) < 0.1f)
        {
            _animator.SetBool("IsMoving", false);
        }
        else
        {
            _animator.SetBool("IsMoving", true);

            _animator.SetFloat("H", _agent.velocity.normalized.x);
            _animator.SetFloat("V", _agent.velocity.normalized.z);
        }
    }

    public override void Initialize()
    {
        base.Initialize();
        if (photonView.IsMine)
        {
            Debug.Log("Ini mover");
            TeamManager.Instance.AddToGroup(this, Group);
            TeamManager.Instance.AddToTeam(this, Team);
            if (ResourceManager.Instance.isTutorial)
            {
                CombatTrigger.Instance.RegisterTroopChange(TeamManager.Instance.GetMeleeTroops(Group).Count, TeamManager.Instance.GetDistanceTroops(Group).Count, TeamManager.Instance.GetAirTroops(Group).Count);
            }


            DoGrowTween();
        }

        _atributteManager.StartHealthRegen();
    }

    public override void TakeDamage(float amount)
    {
        base.TakeDamage(amount);
        _atributteManager.RestartHealthRegen();
        AudioManager.Instance.PlayTroopHitSound();
    }

    #region MOVE

    public void MoveToTargetAlly(Transform target)
    {
        NavMeshHit hit;

        _agent.stoppingDistance = MovementStoppingDistance;
        if (NavMesh.SamplePosition(target.position, out hit, Mathf.Infinity, NavMesh.AllAreas))
        {
            _agent.destination = hit.position;
        }
    }

    public virtual void MoveToTarget(Vector3 target)
    {
        NavMeshHit hit;

        _agent.stoppingDistance = MovementStoppingDistance;
        if (NavMesh.SamplePosition(target, out hit, Mathf.Infinity, NavMesh.AllAreas))
        {
            _agent.destination = hit.position;
        }
    }
    #endregion

    #region DIE

    public override void Die()
    {
        base.Die();
        TeamManager.Instance?.RemoveFromGroup(this, Group);
        TeamManager.Instance?.RemoveFromTeam(this, Team);
        if (ResourceManager.Instance.isTutorial)
        {
            CombatTrigger.Instance.RegisterTroopChange(TeamManager.Instance.GetMeleeTroops(Group).Count, TeamManager.Instance.GetDistanceTroops(Group).Count, TeamManager.Instance.GetAirTroops(Group).Count);
        }
        _agent.speed = 0f;
        _atributteManager.StopHealthRegen();
    }

    #endregion

    #region TWEENING
    protected void DoGrowTween()
    {
        transform.DOScale(Vector3.one * 1, _atributteManager._growTime)
                 .SetEase(Ease.OutQuad)
                 .From(Vector3.zero)
                 .Play();
    }

    protected void DoShrinkTween()
    {
        transform.DOScale(Vector3.zero, _atributteManager._shrinkTime)
                 .SetEase(Ease.Linear)
                 .Play();
    }
    #endregion


    IEnumerator Parabola(NavMeshAgent agent, float height, float duration)
    {
        OffMeshLinkData data = agent.currentOffMeshLinkData;
        Vector3 startPos = agent.transform.position;
        Vector3 endPos = data.endPos + Vector3.up * agent.baseOffset;
        float distance = Vector3.Distance(startPos, endPos);
        float normalizedTime = 0.0f;
        while (normalizedTime < 1.0f)
        {
            float yOffset = height * 4.0f * (normalizedTime - normalizedTime * normalizedTime);
            agent.transform.position = Vector3.Lerp(startPos, endPos, normalizedTime) + yOffset * Vector3.up;
            normalizedTime += Time.deltaTime / (duration * distance);
            yield return null;
        }
    }
}
