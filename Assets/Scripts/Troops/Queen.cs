﻿using Photon.Pun;
using UnityEngine;

public class Queen : Mover
{
    public int initialFood, initialBiomass, initialEnergy;

    private bool _canPlayAttackedSound = true;
    private float _timeToEnableSound = 30f; 
    /*
        protected override void Awake()
        {
            base.Awake();
            ResourceManager.Instance.Food = initialFood;
            ResourceManager.Instance.Biomass = initialBiomass;
            ResourceManager.Instance.Energy = initialEnergy;
        }

        protected override void Start()
        {
            base.Start();
            TeamManager.Instance.Queens[Group] = this;
            ResourceManager.Instance.SetBuildButtons();
        }
    */

    public override void Initialize()
    {
        base.Initialize();

        if (photonView.IsMine)
        {
            EnableQueenAttackedSound();

            ResourceManager.Instance.Food = initialFood;
            ResourceManager.Instance.Biomass = initialBiomass;
            ResourceManager.Instance.Energy = initialEnergy;
            TeamManager.Instance.Queens[Group] = this;
            ResourceManager.Instance.SetBuildButtons();
        }
    }

    private void EnableQueenAttackedSound()
    {
        _canPlayAttackedSound = true;
    }

    public override void TakeDamage(float amount)
    {
        base.TakeDamage(amount);

        if (photonView.IsMine)
        {
            if (_canPlayAttackedSound)
            {
                AudioManager.Instance.PlayQueenAttackedSound();
            }
            CancelInvoke(nameof(EnableQueenAttackedSound));
            Invoke(nameof(EnableQueenAttackedSound), _timeToEnableSound);
        }
    }

    public override void Die()
    {
        base.Die();
        if (photonView.IsMine)
            GameManager.Instance.GameOver(); 
    }
}
