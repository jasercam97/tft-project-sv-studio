﻿using System;
using Build;
using GameManagers;
using Photon.Pun;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class AtributteManager : MonoBehaviour, IPunObservable
{
    [SerializeField]
    protected AtributtesSO Atributtes;

    public Slider lifeBar;

    //Selectable Atributtes
    public string _name;
    public Sprite _image;
    public string _description;
    public string _littleDescription;
    public float _maxHealth;
    public float _currentHealth;
    private float _regenerationHealth;
    private float _regenerationTime = 1f;
    [HideInInspector] public SpriteRenderer _sprite;
    [HideInInspector] public Color _spriteColor;

    //Mover Atributtes
    [HideInInspector] public float _moveSpeed;
    [HideInInspector] public float _growTime = 0.5f;
    [HideInInspector] public float _shrinkTime = 1f;

    //Warrior && Turret Atributtes
    [HideInInspector] public float _attackSpeed;
    [HideInInspector] public float _projectileSpeed;
    [HideInInspector] public float _attackDamage;
    [HideInInspector] public float _attackRange;

    //Gatherer Atributes
    [HideInInspector] public float _energyTake;
    [HideInInspector] public float _biomassTake;
    [HideInInspector] public float _foodTake;
    [HideInInspector] public float _distanceToCollectionPoint;

    //Building Atributes
    [HideInInspector] public float _costNormalEnergy;
    [HideInInspector] public float _costNormalBiomass;
    [HideInInspector] public float _costNormalFood;
    [HideInInspector] public float _evolutionNormalTime;
    [HideInInspector] public GameObject _normalTroop;

    [HideInInspector] public float _costUpgradedEnergy;
    [HideInInspector] public float _costUpgradedBiomass;
    [HideInInspector] public float _costUpgradedFood;
    [HideInInspector] public float _evolutionUpgradedTime;
    [HideInInspector] public GameObject _upgradedTroop;

    private bool firstTime = true;

    private PhotonView _photonView;

    private BuildBehaviour _build;

    private void Awake()
    {
        _sprite = GetComponentInChildren<SpriteRenderer>();
        _spriteColor = _sprite.color;
        _photonView = GetComponent<PhotonView>();
        _build = GetComponent<BuildBehaviour>();
        ShowLifebar(GameManager.Instance.Show);
    }

    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Space)) { TakeDamage(15f); }
    }

    public void UpdateAtributtes(float attackDamageMultiplier, float attackSpeedMultiplier, float moveSpeedMultiplier, float scaleMultiplier)
    {
        if (TryGetComponent<Selectable>(out Selectable s))
        {
            _name = Atributtes.Name;
            _image = Atributtes.Image;
            _description = Atributtes.Description;
            _littleDescription = Atributtes.LittleDescription;
            _maxHealth = Atributtes.MaxHealth;
            _regenerationHealth = Atributtes.RegenerationHealth;
            if (firstTime)
            {
                _currentHealth = _maxHealth;
                lifeBar.value = _currentHealth / _maxHealth;
                if (_build) { _build.SetBuildHealth(_currentHealth, _maxHealth); }
                firstTime = false;
            }
        }

        if (TryGetComponent<Mover>(out Mover m))
        {
            _moveSpeed = Atributtes.MoveSpeed * moveSpeedMultiplier;
            GetComponent<NavMeshAgent>().speed = _moveSpeed;
        }

        if (TryGetComponent<Turret>(out Turret t))
        {
            _attackRange = Atributtes.AttackRange;
            _attackDamage = Atributtes.Damage;
            _attackSpeed = Atributtes.AttackSpeed;
            _projectileSpeed = Atributtes.ProjectileSpeed;
        }

        if (TryGetComponent<Warrior>(out Warrior w))
        {
            _attackRange = Atributtes.AttackRange;
            _attackDamage = Atributtes.Damage * attackDamageMultiplier;
            _attackSpeed = Atributtes.AttackSpeed / attackSpeedMultiplier;
            _projectileSpeed = Atributtes.ProjectileSpeed;
            transform.localScale = Vector3.one * scaleMultiplier;
        }

        if (TryGetComponent<Gatherer>(out Gatherer g))
        {
            _energyTake = Atributtes.EnergyTake;
            _biomassTake = Atributtes.BiomassTake;
            _foodTake = Atributtes.FoodTake;
            _distanceToCollectionPoint = Atributtes.DistanceToCollectionPoint;
        }

        if (TryGetComponent<Building>(out Building b))
        {
            _costNormalEnergy = Atributtes.CostNormalEnergy;
            _costNormalBiomass = Atributtes.CostNormalBiomass;
            _costNormalFood = Atributtes.CostNormalFood;
            _evolutionNormalTime = Atributtes.EvolutionNormalTime;
            _normalTroop = Atributtes.NormalTroop;

            _costUpgradedEnergy = Atributtes.CostUpgradedEnergy;
            _costUpgradedBiomass = Atributtes.CostUpgradedBiomass;
            _costUpgradedFood = Atributtes.CostUpgradedFood;
            _evolutionUpgradedTime = Atributtes.EvolutionUpgradedTime;
            _upgradedTroop = Atributtes.UpgradedTroop;
        }
    }

    public void ShowLifebar(bool state)
    {
        lifeBar.gameObject.SetActive(state);
    }

    public void TakeDamage(float damage)
    {
        _currentHealth = _currentHealth - damage;

        _spriteColor.a = 0.25f;
        _sprite.color = _spriteColor;
        Invoke(nameof(SetNormalColor), 0.15f);

        lifeBar.value = _currentHealth / _maxHealth;

        if (_build) { _build.SetBuildHealth(_currentHealth, _maxHealth); }

        if (_currentHealth <= 0)
        {
            Die();
        }
    }

    private void SetNormalColor()
    {
        _spriteColor.a = 1f;
        _sprite.color = _spriteColor;
    }

    private void Die()
    {
        if (_photonView.IsMine)
        {
            GetComponent<Selectable>().PreviousDie();
        }
    }

    private void RegenHealth()
    {
        _currentHealth += _regenerationHealth;

        if (_currentHealth > _maxHealth)
        {
            _currentHealth = _maxHealth;
        }

        lifeBar.value = _currentHealth / _maxHealth;
    }

    public void StartHealthRegen()
    {
        InvokeRepeating(nameof(RegenHealth), 5, _regenerationTime);
    }

    public void StopHealthRegen()
    {
        CancelInvoke(nameof(RegenHealth));
    }

    public void RestartHealthRegen()
    {
        StopHealthRegen();
        StartHealthRegen();
    }


    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            // We own this player: send the others our data
            stream.SendNext(_currentHealth);
        }
        else
        {
            // Network player, receive data
            this._currentHealth = (float)stream.ReceiveNext();
        }
    }
}
