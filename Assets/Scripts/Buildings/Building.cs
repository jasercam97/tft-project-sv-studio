﻿using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class Building : Selectable
{
    public override void Initialize()
    {
        base.Initialize();

        if (photonView.IsMine)
        {
            ready = true;
            if (Group <= 4)
            {
                if (_visionInFog != null) _visionInFog.SetActive(true);
                if (_visionInMinimap != null) _visionInMinimap.SetActive(true);
            }
            TeamManager.Instance.AddBuildingToGroup(this, Group);
        }
    }

    public override void TakeDamage(float amount)
    {
        base.TakeDamage(amount);
        AudioManager.Instance.PlayBuildingHitSound();
    }

    public override void Die()
    {
        if (photonView.IsMine)
        {
            AudioManager.Instance.PlayBuildingDestroyedSound();
            TeamManager.Instance?.RemoveBuildingFromGroup(this, Group);
            PhotonNetwork.Destroy(gameObject);
        }
    }
}
