﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class CapturableBuilding : MonoBehaviourPunCallbacks
{
    public bool isTutorial = false;
    public DialogueObject VictoryDialogue;
    public float CaptureRange;
    public LayerMask TroopLayer;
    public float CaptureFactor;


    [Header("Sprite")]
    public Sprite NotCapturedSprite;
    public Sprite CapturedSprite;
    public SpriteRenderer Renderer;

    [Header("VFX")]
    public List<Color> ColorByTeam;
    public ParticleSystem CapturedEffect;
    public GameObject CapturableAreaEffect;
    public GameObject ProgressBarParent;
    public Image ProgressBarImage;

    [Header("Vision")]
    public List<GameObject> FogObjects;

    private int _largestTeamAmount;
    private int _restOfTeamsAmount;
    private int _largestTeamIndex;
    private int _teamBelong;
    private int _teamInLead;

    private List<int> _troopsPerTeam;
    private bool _canPlayLosingSound = true;
    private float _timeToEnableLosingSound = 5f;

    public float _captureProgress = 0;
    [HideInInspector]public int team = -1;
    public GameObject Mask;

    private void Awake()
    {
        _teamBelong = -1;
        _largestTeamIndex = -1;
        _teamInLead = -1;

        _troopsPerTeam = new List<int>();

        for (int i = 0; i < TeamManager.Instance._playerAmount; i++)
        {
            _troopsPerTeam.Add(0);
        }
    }

    private void Update()
    {
        CheckOnRange();
    }

    private void CheckOnRange()
    {
        Collider[] troopsInRange = Physics.OverlapSphere(transform.position, CaptureRange, TroopLayer);

        for (int i = 0; i < _troopsPerTeam.Count; i++)
        {
            _troopsPerTeam[i] = 0;
        }

        if (troopsInRange.Length > 0)
        {
            CapturableAreaEffect.SetActive(true);

            if (_captureProgress < 100)
            {
                ProgressBarParent.SetActive(true);
                ProgressBarImage.fillAmount = _captureProgress / 100;
            }

            foreach (Collider troop in troopsInRange)
            {
                _troopsPerTeam[troop.gameObject.GetComponent<Selectable>().Team]++;
            }

            _largestTeamAmount = CalculateLargestTeam();
            _restOfTeamsAmount = CalculateRestOfTeams();

            // Check team in lead (team currently capturing building)
            if (_teamInLead == -1)
            {
                if (_largestTeamAmount > _restOfTeamsAmount)
                {
                    _teamInLead = _largestTeamIndex;
                }
            }

            // Check largest team
            if (_largestTeamAmount > _restOfTeamsAmount)
            {
                // If largest team is in lead, increase progress
                if (_teamInLead == _largestTeamIndex)
                {
                    ProgressBarImage.color = ColorByTeam[_teamInLead];
                    _captureProgress += CaptureFactor * Time.deltaTime * (Mathf.Log(_largestTeamAmount - _restOfTeamsAmount) + 1);
                }
                // If largest team is not in lead, reduce progress
                else
                {
                    _captureProgress -= CaptureFactor * Time.deltaTime * (Mathf.Log(_largestTeamAmount - _restOfTeamsAmount) + 1);
                }
            }

            // Building is captured when progress reaches 100
            if (_captureProgress >= 100)
            {
                _captureProgress = 100;

                if (_teamBelong != _largestTeamIndex && _teamInLead != _teamBelong)
                {
                    _teamBelong = _largestTeamIndex;

                    team = _teamBelong;
                    Mask.SetActive(true);
                    DoCaptureEffect(_teamBelong);
                    photonView?.RPC(nameof(RPC_DoCaptureEffect), RpcTarget.Others, _teamBelong);
                }
            }
            // Building is lost when progress reaches 0
            else if (_captureProgress <= 0)
            {
                _captureProgress = 0;

                if (_teamBelong != -1)
                {
                    Mask.SetActive(false);
                    DoLoseEffect(_teamBelong);
                    photonView?.RPC(nameof(RPC_DoLoseEffect), RpcTarget.Others, _teamBelong);
                }

                _teamBelong = -1;
                _teamInLead = -1;
            }

            if (_teamBelong != -1 && _teamBelong == SelectorRTS.Instance._group && _teamBelong != _largestTeamIndex && _canPlayLosingSound)
            {
                AudioManager.Instance.PlayBuildingBeingLostSound();
                _canPlayLosingSound = false;
                CancelInvoke(nameof(EnableLosingSound));
            }
        }
        else if (_teamBelong == -1)
        {
            CapturableAreaEffect.SetActive(false);

            if (_captureProgress > 0)
            {
                _captureProgress -= CaptureFactor * Time.deltaTime;
                ProgressBarImage.fillAmount = _captureProgress / 100;
            }
            else
            {
                _captureProgress = 0;
                ProgressBarParent.SetActive(false);
            }
        }
        else
        {
            CapturableAreaEffect.SetActive(false);
            Invoke(nameof(EnableLosingSound), _timeToEnableLosingSound);
        }
    }

    private void EnableLosingSound()
    {
        _canPlayLosingSound = true;
    }

    private int CalculateRestOfTeams()
    {
        int rest = 0;

        for (int i = 0; i < _troopsPerTeam.Count; i++)
            if (_troopsPerTeam[i] <= _largestTeamAmount && i != _largestTeamIndex)
                rest += _troopsPerTeam[i];

        return rest;
    }

    private int CalculateLargestTeam()
    {
        int max = 0;
        _largestTeamIndex = 0;

        for (int i = 0; i < _troopsPerTeam.Count; i++)
        {
            if (_troopsPerTeam[i] > max)
            {
                max = _troopsPerTeam[i];
                _largestTeamIndex = i;
            }
        }

        return max;
    }

    protected virtual void DoCaptureEffect(int capturedByTeam)
    {
        _captureProgress = 100;
        Renderer.sprite = CapturedSprite;
        CapturedEffect.startColor = ColorByTeam[capturedByTeam];
        CapturedEffect.gameObject.SetActive(true);

        if (SelectorRTS.Instance._group == capturedByTeam)
        {
            AudioManager.Instance.PlayBuildingCapturedSound();

            foreach (GameObject fogObject in FogObjects)
            {
                fogObject.SetActive(true);
            }
        }
        if (isTutorial && capturedByTeam == 0)
        {
            TutorialManager.Instance.PauseGame();
            DialogueUI.Instance.ShowDialogue(VictoryDialogue);
        }
    }

    [PunRPC]
    public void RPC_DoCaptureEffect(int capturedByTeam)
    {
        DoCaptureEffect(capturedByTeam);
        team = capturedByTeam;
    }

    protected virtual void DoLoseEffect(int lostByTeam)
    {
        _captureProgress = 0;
        Renderer.sprite = NotCapturedSprite;
        CapturedEffect.gameObject.SetActive(false);

        if (SelectorRTS.Instance._group == lostByTeam)
        {
            AudioManager.Instance.PlayBuildingLostSound();

            foreach (GameObject fogObject in FogObjects)
            {
                fogObject.SetActive(false);
            }
        }
    }

    [PunRPC]
    public void RPC_DoLoseEffect(int lostByTeam)
    {
        DoLoseEffect(lostByTeam);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, CaptureRange);
    }
}