﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisionBuilding : CapturableBuilding
{
    protected override void DoCaptureEffect(int capturedByTeam)
    {
        base.DoCaptureEffect(capturedByTeam);
    }

    protected override void DoLoseEffect(int lostByTeam)
    {
        base.DoLoseEffect(lostByTeam);
    }
}
