﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoraleBuilding : CapturableBuilding
{
    public float MoraleIncrease = 25f;

    protected override void DoCaptureEffect(int capturedByTeam)
    {
        base.DoCaptureEffect(capturedByTeam);

        if (SelectorRTS.Instance._group == capturedByTeam)
        {
            MoraleController.Instance.UpdateMinMorale(MoraleIncrease);
        }
    }

    protected override void DoLoseEffect(int lostByTeam)
    {
        base.DoLoseEffect(lostByTeam);

        if (SelectorRTS.Instance._group == lostByTeam)
        {
            MoraleController.Instance.UpdateMinMorale(-MoraleIncrease);
        }
    }
}
