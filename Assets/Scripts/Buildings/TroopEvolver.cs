﻿using Build;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TroopEvolver : MonoBehaviour
{
    [HideInInspector] public bool IsFull;
    public bool IsEnabled { get; set; } // TODO poner a true cuando se termine de construir

    public float TimeToEvolve = 12;
    public float TimeToEvolveUpgraded = 8;
    public Transform SpawnPoint;
    public float MaxSpawnVariance = 3;

    //[Header("Pooling")]
    //public TroopType TroopPool;
    //private Transform _poolParent;
    public GameObject EvolvedTroopPrefab;
    public GameObject EvolvedTroopUpgradedPrefab;
    public Sprite ImageTroopEvolved;
    public AtributtesSO AtributtesTroopEvolved;
    public AtributtesSO AtributtesTroopEvolvedUpgraded;

    private List<Evolver> _troopQueue;
    private Vector3 _spawnPosition;
    private Mover _currentTroop;
    private int _currentGroup;
    private int _currentTeam;
    private bool _troopUpgraded = false; //Si true instanciar tropas mejoradas.
    private bool _timeUpgraded = false; //Si true instanciar tropas mejoradas.

    AnimationEvents _events;

    private void Awake()
    {
        _troopQueue = new List<Evolver>();
        IsEnabled = false;
        _events = GetComponent<AnimationEvents>();
       // ChoosePool();
    }
    /*
    private void ChoosePool()
    {
        switch (TroopPool)
        {
            case TroopType.Melee:
                _poolParent = GameObject.FindGameObjectWithTag("MeleePool").transform;
                break;

            case TroopType.Ranged:
                _poolParent = GameObject.FindGameObjectWithTag("RangedPool").transform;
                break;

            case TroopType.Flying:
                _poolParent = GameObject.FindGameObjectWithTag("FlyingPool").transform;
                break;

            default:
                break;
        }
    }
    */
    public void SetFull()
    {
        IsFull = true;  // TODO Si en algún momento puede entrar más de 1 tropa, cambiar esta función por un contador
    }

    public void StartEvolution(int group, int team)
    {
        _currentGroup = group;
        _currentTeam = team;

        Invoke(nameof(EndEvolution), TimeToEvolve);
    }

    public void EndEvolution()
    {
        NavMeshHit hit;
        Vector3 spawnPosition;

        //_currentTroop = PoolManager.Instance.GetFreeObject(_poolParent, EvolvedTroopPrefab).GetComponent<Mover>();

        if (NavMesh.SamplePosition(SpawnPoint.position, out hit, Mathf.Infinity, NavMesh.AllAreas))
        {
            spawnPosition = hit.position;
        }
        else
        {
            spawnPosition = SpawnPoint.position;
        }

        //_currentTroop.Initialize();
        //_currentTroop.gameObject.SetActive(true);

        int playerNumber = PhotonNetwork.LocalPlayer.GetPlayerNumber();
        GameObject GO = PhotonNetwork.Instantiate(EvolvedTroopPrefab.name, spawnPosition, transform.rotation, 0);
        if (GO.TryGetComponent(out Selectable newSelectable)) newSelectable.SetGroupAndTeam(playerNumber, playerNumber);

        AudioManager.Instance.PlayCreateTroopSound();

        _spawnPosition = new Vector3(hit.position.x + Random.Range(-MaxSpawnVariance, MaxSpawnVariance), hit.position.y, hit.position.z + Random.Range(-MaxSpawnVariance, 0));
        GO.GetComponent<Mover>().MoveToTarget(_spawnPosition);

        Transform destination = GetComponent<BuildBehaviour>()._targetObject.transform;

        if (destination.localPosition != Vector3.zero)
            GO.GetComponent<Mover>().MoveToTarget(new Vector3(destination.position.x + Random.Range(-MaxSpawnVariance, MaxSpawnVariance), destination.position.y, destination.position.z + Random.Range(-MaxSpawnVariance, 0)));

        IsFull = false;

        if (_troopQueue.Count > 0)
        {
            _troopQueue[0].MoveToEvolvePoint();
            RemoveFromQueue(_troopQueue[0]);
        }
    }

    public void AddToQueue(Evolver troop)
    {
        _troopQueue.Add(troop);
    }

    public void RemoveFromQueue(Evolver troop)
    {
        _troopQueue.Remove(troop);
    }

    #region BUILD UPDATES

    public void IsTimeToEvolvedUpgraded() { _timeUpgraded = true; TimeToEvolve = TimeToEvolveUpgraded; }

    public void AreTroopsUpgraded() { _troopUpgraded = true; EvolvedTroopPrefab = EvolvedTroopUpgradedPrefab; AtributtesTroopEvolved = AtributtesTroopEvolvedUpgraded; }

    #endregion
}
