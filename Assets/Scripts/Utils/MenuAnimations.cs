using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MenuAnimations : MonoBehaviour
{
    public float minTime;
    public float maxTime;
    public float duration;

    public Transform leftPosition;
    public Transform rightPosition;


    private void Start()
    {
        Invoke(nameof(DoMoveTween), Random.Range(minTime, maxTime));
    }

    private void DoMoveTween()
    {
        Invoke(nameof(DoMoveTween), Random.Range(minTime, maxTime));

        if (Random.Range(0, 2) == 0)    // Hacia la derehca
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);

            transform.DOLocalMoveX(rightPosition.localPosition.x, duration)
                     .From(leftPosition.localPosition.x)
                     .SetEase(Ease.Linear)
                     .Play();
        }
        else    // Hacia la izquierda
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);

            transform.DOLocalMoveX(leftPosition.localPosition.x, duration)
                     .From(rightPosition.localPosition.x)
                     .SetEase(Ease.Linear)
                     .Play();
        }
    }
}
