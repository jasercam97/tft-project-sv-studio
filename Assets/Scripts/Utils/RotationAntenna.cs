using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotationAntenna : MonoBehaviour
{
    public Slider MusicVolume;

    void Update()
    {
        transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, -30 + MusicVolume.value * 30);
    }
}
