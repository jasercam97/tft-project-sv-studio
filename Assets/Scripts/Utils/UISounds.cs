using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UISounds : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler
{
    public void OnPointerDown(PointerEventData eventData)
    {
        AudioManager.Instance.PlayClickSound();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        AudioManager.Instance.PlayHoverSound();
    }
}
