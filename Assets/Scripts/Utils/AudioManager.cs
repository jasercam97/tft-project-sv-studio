using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class AudioManager : Singleton<AudioManager>
{
    public const string KEY_MUSIC_VOLUME = "MusicVolume"; 
    public const string KEY_SFX_VOLUME = "SFXVolume";

    [FMODUnity.EventRef]
    public string MenuMusicEvent;
    private FMOD.Studio.EventInstance menuMusic;

    [FMODUnity.EventRef]
    public string VictoryEvent;

    [FMODUnity.EventRef]
    public string GameOverEvent;

    [FMODUnity.EventRef]
    public string UIClickEvent;

    [FMODUnity.EventRef]
    public string UIClickBlockedEvent;

    [FMODUnity.EventRef]
    public string UIHoverEvent;

    [FMODUnity.EventRef]
    public string UIVolumeCheckEvent;

    [FMODUnity.EventRef]
    public string MoveCommandEvent;

    [FMODUnity.EventRef]
    public string AttackCommandEvent;

    [FMODUnity.EventRef]
    public string EatLarvaEvent;

    [FMODUnity.EventRef]
    public string AttackHormigaEvent;

    [FMODUnity.EventRef]
    public string AttackBabosaEvent;

    [FMODUnity.EventRef]
    public string AttackMoscaEvent;

    [FMODUnity.EventRef]
    public string DeathHormigaEvent;

    [FMODUnity.EventRef]
    public string DeathBabosaEvent;

    [FMODUnity.EventRef]
    public string DeathMoscaEvent;

    [FMODUnity.EventRef]
    public string DeathLarvaEvent;

    [FMODUnity.EventRef]
    public string BuildingBeingCapturedEvent;

    [FMODUnity.EventRef]
    public string BuildingCapturedEvent;

    [FMODUnity.EventRef]
    public string BuildingLostEvent;

    [FMODUnity.EventRef]
    public string QueenAttackedEvent;

    [FMODUnity.EventRef]
    public string BuildingEvent;

    [FMODUnity.EventRef]
    public string RepairBuildingEvent;

    [FMODUnity.EventRef]
    public string DeathBuildingEvent;

    [FMODUnity.EventRef]
    public string UpgradeBuildingEvent;

    [FMODUnity.EventRef]
    public string BuildingHitEvent;

    [FMODUnity.EventRef]
    public string ShootTurretEvent;

    [FMODUnity.EventRef]
    public string TurretProjectileExplosionEvent;

    [FMODUnity.EventRef]
    public string CreateTroopEvent;

    [FMODUnity.EventRef]
    public string SelectBuildingEvent;

    [FMODUnity.EventRef]
    public string SelectHormigaEvent;

    [FMODUnity.EventRef]
    public string SelectBabosaEvent;

    [FMODUnity.EventRef]
    public string SelectMoscaEvent;

    [FMODUnity.EventRef]
    public string SelectLarvaEvent;

    [FMODUnity.EventRef]
    public string TroopHitEvent;

    [FMODUnity.EventRef]
    public string SelectionSound;

    FMOD.Studio.Bus Music;
    FMOD.Studio.Bus SFX;

    public float MusicVolume;
    public float SFXVolume;


    void Awake()
    {
        MusicVolume = PlayerPrefs.GetFloat(KEY_MUSIC_VOLUME, 0.5f);
        SFXVolume = PlayerPrefs.GetFloat(KEY_SFX_VOLUME, 0.5f);

        GameObject[] objs = GameObject.FindGameObjectsWithTag("Music");

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);

        Music = FMODUnity.RuntimeManager.GetBus("bus:/Music");
        SFX = FMODUnity.RuntimeManager.GetBus("bus:/SFX");
    }

    void Start()
    {
        menuMusic = FMODUnity.RuntimeManager.CreateInstance(MenuMusicEvent);
        InvokeRepeating(nameof(MenuMusicStart), 0f, 202f);
        //instance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        //instance.release();

        Music.setVolume(MusicVolume);
        SFX.setVolume(SFXVolume);
    }

    public void MenuMusicStart()
    {
        menuMusic.start();
    }

    public void MusicVolumeLevel(float newMusicVolume)
    {
        Music.setVolume(newMusicVolume);
        MusicVolume = newMusicVolume;

        PlayerPrefs.SetFloat(KEY_MUSIC_VOLUME, newMusicVolume);
    }

    public void SFXVolumeLevel(float newSFXVolume)
    {
        SFX.setVolume(newSFXVolume);
        SFXVolume = newSFXVolume;

        PlayerPrefs.SetFloat(KEY_SFX_VOLUME, newSFXVolume);
    }

    public void OnMouseOverMusic()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/Example");
    }

    public void PlayClickSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(UIClickEvent);
    }

    public void PlayClickBlockedSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(UIClickBlockedEvent);
    }

    public void PlayHoverSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(UIHoverEvent);
    }

    public void PlayVolumeCheckSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(UIVolumeCheckEvent);
    }

    public void PlayMoveCommandSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(MoveCommandEvent);        
    }

    public void PlayAttackCommandSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(AttackCommandEvent);
    }

    public void PlayLarvaEatSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(EatLarvaEvent);
    }

    public void PlayHormigaAttackSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(AttackHormigaEvent);
    }

    public void PlayBabosaAttackSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(AttackBabosaEvent);
    }

    public void PlayMoscaAttackSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(AttackMoscaEvent);
    }

    public void PlayLarvaDeathSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(DeathLarvaEvent);
    }

    public void PlayHormigaDeathSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(DeathHormigaEvent);
    }

    public void PlayBabosaDeathSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(DeathBabosaEvent);
    }

    public void PlayMoscaDeathSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(DeathMoscaEvent);
    }

    public void PlayTroopHitSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(TroopHitEvent);
    }

    public void PlayBuildingBeingLostSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(BuildingBeingCapturedEvent);
    }

    public void PlayBuildingCapturedSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(BuildingCapturedEvent);
    }

    public void PlayBuildingLostSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(BuildingLostEvent);
    }

    public void PlayQueenAttackedSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(QueenAttackedEvent);
    }

    public void PlayCreateTroopSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(CreateTroopEvent);
    }

    public void PlayBuildingConstructionSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(BuildingEvent);
    }

    public void PlayBuildingRepairSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(RepairBuildingEvent);
    }

    public void PlayBuildingDestroyedSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(DeathBuildingEvent);
    }

    public void PlayBuildingUpgradedSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(UpgradeBuildingEvent);
    }

    public void PlayTurretShootSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(ShootTurretEvent);
    }

    public void PlayTurretProjectileExplosionSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(TurretProjectileExplosionEvent);
    }

    public void PlayBuildingHitSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(BuildingHitEvent);
    }

    public void PlaySelectBuildingSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(SelectBuildingEvent);
    }

    public void PlaySelectLarvaSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(SelectLarvaEvent);
    }

    public void PlaySelectHormigaSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(SelectHormigaEvent);
    }

    public void PlaySelectBabosaSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(SelectBabosaEvent);
    }

    public void PlaySelectMoscaSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(SelectMoscaEvent);
    }

    public void PlaySelectionSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(SelectionSound);
    }

    public void PlayVictorySound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(VictoryEvent);
    }

    public void PlayGameOverSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(GameOverEvent);
    }
}
