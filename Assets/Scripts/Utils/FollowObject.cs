using UnityEngine;

public class FollowObject : MonoBehaviour
{
    public GameObject ObjectToFollow;
    public Vector3 offset;

    /*
    private void Awake()
    {
        offset = transform.position - ObjectToFollow.transform.position;
    }
    */
    private void Update()
    {
        transform.position = new Vector3(ObjectToFollow.transform.position.x + offset.x, ObjectToFollow.transform.position.y + offset.y, ObjectToFollow.transform.position.z + offset.z);
    }
}
