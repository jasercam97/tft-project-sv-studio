using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdjustFogY : MonoBehaviour
{
    public GameObject ObjectToFollow;
    private float initialObjectToFollowY;
    private float initialY;

    public bool Larva;
    public bool Troop;
    public bool Queen;


    private void Awake()
    {
        initialObjectToFollowY = ObjectToFollow.transform.localPosition.y;
        initialY = transform.localPosition.y;
    }
    
    private void Update()
    {
        if(Larva)
            transform.localPosition = new Vector3(transform.localPosition.x, initialY + 2 * Mathf.Abs(ObjectToFollow.transform.localPosition.y-initialObjectToFollowY), transform.localPosition.z);

        if (Troop)
            transform.localPosition = new Vector3(transform.localPosition.x, initialY + Mathf.Abs(ObjectToFollow.transform.localPosition.y - initialObjectToFollowY), transform.localPosition.z);

        if (Queen)
            transform.localPosition = new Vector3(transform.localPosition.x, initialY + 0.5f*Mathf.Abs(ObjectToFollow.transform.localPosition.y - initialObjectToFollowY), transform.localPosition.z);
    }
}
