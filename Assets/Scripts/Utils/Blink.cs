using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Blink : MonoBehaviour
{
    public float ParpadeoSpeed;
    public Image BackButton;
    public Sprite Sprite1, Sprite2;
    private bool _sprite1 = true;


    void Start()
    {
        BackButton.sprite = Sprite1;
        InvokeRepeating(nameof(SwitchSprite),0f, ParpadeoSpeed);
    }

    public void SwitchSprite()
    {
        BackButton.sprite = _sprite1 ? Sprite2 : Sprite1;
        _sprite1 = !_sprite1;
    }
}
