using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CountdownToExit : MonoBehaviour
{
    public float secondsToExit = 5;
    private TMP_Text CountText;

    private void Awake()
    {
        CountText = GetComponent<TMP_Text>();
    }

    private void Update()
    {
        secondsToExit -= Time.deltaTime;
        CountText.text = "("+Mathf.CeilToInt(secondsToExit).ToString()+")";
    }
}
