using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotationWheel : MonoBehaviour
{
    public Slider SoundVFX;

    void Update()
    {
        transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, -SoundVFX.value*360);
    }
}
