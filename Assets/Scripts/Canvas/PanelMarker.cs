using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PanelMarker : MonoBehaviour
{
    [SerializeField] private float time;
    [SerializeField] private float maxSize;
    [SerializeField] private Ease ease;
    
    private RectTransform rect;

    private void Awake()
    {
        rect = GetComponent<RectTransform>();
    }

    private void Start()
    {
        rect.DOScale(Vector3.one * maxSize, time).SetEase(ease).SetLoops(-1, LoopType.Yoyo).Play();
    }
}
