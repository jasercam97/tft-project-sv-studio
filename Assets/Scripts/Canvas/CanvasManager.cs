using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using UnityEngine.UI;
using System;
using TMPro;
using Build;

namespace GameManagers
{
    public class CanvasManager : Singleton<CanvasManager>
    {
        [Header("Build Canvas")]
        public GameObject BuildPanel;

        [Header("Build Settings Canvas")]
        [Tooltip("Panel principal que contiene los diferentes paneles de cada edificio")]
        public GameObject InfoPanel;
        public GameObject InfoCapturableVisionPanel;
        public GameObject InfoCapturableMoralePanel;
        public GameObject ResourcePanel;
        public GameObject InfoGroupPanel;
        public GameObject InfoGroupPanelCompact;
        public TMP_Text HealthText;
        public Image SliderFill;
        public Image SliderHandle;
        public Button RepairButton;

        [Header("Selectable Info Panel")]
        public TMP_Text SelectableName;
        public Image SelectableImage;
        public TMP_Text SelectableDescription;
        public Slider SelectableHealth;
        public TMP_Text SelectableHealthText;

        [Header("Resource Info Panel")]
        public TMP_Text ResourceName;
        public Image ResourceImage;
        public TMP_Text ResourceDescription;
        public Slider ResourceHealth;
        public TMP_Text ResourceHealthText;

        [Header("Type of Build Panel")]
        public GameObject AnthillPanel;
        public GameObject FlyhomePanel;
        public GameObject SlughomePanel;
        public GameObject TurretPanel;

        [Header("Actions Canvas")]
        public GameObject ActionsPanel;
        public GameObject BuildUpgradesPanel;
        public GameObject TurretUpgradesPanel;

        [Header("Action Panel Buttons")]
        public Button BuildButton;
        public Button QueenButton;

        [Header("Upgrade Buttons")]
        public TMP_Text HeaderBuildUpgradesText;
        public TMP_Text HeaderTurretUpgradesText;
        public List<Button> TimeUpgradeButtons;
        public List<Button> TroopUpgradeButtons;
        public List<Button> HealthUpgradeButtons;
        public List<Button> DamageUpgradeButtons;

        [Header("Aux Canvas")]
        public GameObject AuxPanel;
        public Image UnitImage;
        public TMP_Text Description;
        public TMP_Text AttackTxt;
        public TMP_Text HealthTxt;
        public TMP_Text SpeedTxt;

        [Header("PopupPanel")]
        public TMP_Text NameText;
        public TMP_Text DescriptionText;
        public TMP_Text CostText;
        public TMP_Text TimeText;

        private List<GameObject> _listOfActiveCanvas = new List<GameObject>();

        private bool ShowInformationOfSelectable = true, showLifeBars = true;

        //INFO PANEL GROUP
        public Image[] _icons;
        public Slider[] _healths;

        //INFO PANEL GROUP COMPACT
        private GameObject[] _iconsCompactGO = new GameObject[5];
        private TMP_Text[] _amounts = new TMP_Text[5];
        private int[] _amountsValue = new int[5];

        private BuildBehaviour _currentBuild;
        private BuildUpgrades _currentBuildUpgrades;

        private string InitialHeaderBuildUpgradesText, InitialHeaderTurretUpgradesText;

        private void Awake()
        {
            InitInfoPanelGroup();
            InitInfoPanelGroupCompact();

            InitialHeaderBuildUpgradesText = HeaderBuildUpgradesText.text;
            InitialHeaderTurretUpgradesText = HeaderTurretUpgradesText.text;
        }


        private void Start()
        {
            //Init
            BuildPanel.SetActive(false);
        }

        public void InitInfoPanel()
        {
            InvokeRepeating(nameof(UpdateInfoPanel), 1f, 0.1f);
        }
        #region CANVAS_PANELS

        /// <summary>
        /// Metodo para cambiar paneles que comparten espacio en el canvas.
        /// </summary>
        /// <param name="openCanvas">Panel que se abre.</param>
        /// <param name="closeCanvas">Panel que se cierra.</param>
        public void OpenCanvas(GameObject openCanvas, GameObject closeCanvas)
        {
            if (!openCanvas.activeSelf)
            {
                if (closeCanvas) { CloseCanvas(closeCanvas); }

                if(openCanvas == TurretUpgradesPanel) { CloseCanvas(BuildUpgradesPanel); }
                if(openCanvas == BuildUpgradesPanel) { CloseCanvas(TurretUpgradesPanel); }

                _listOfActiveCanvas.Add(openCanvas);
                openCanvas.SetActive(true);
            }
        }

        public void OpenCanvas(GameObject openCanvas)
        {
            _listOfActiveCanvas.Add(openCanvas);
            openCanvas.SetActive(true);
        }

        public void CloseCanvas(GameObject closeCanvas)
        {
            if (closeCanvas.activeSelf)
            {
                BuildManager.Instance.DeselectBuild();

                _listOfActiveCanvas.Remove(closeCanvas);
                closeCanvas.SetActive(false);
            }
        }

        public void OpenBuildCanvas()
        {
            SelectorRTS.Instance.ClearSelection();
            OpenCanvas(BuildPanel);
            ShowVisionBuildingPanel(false);
            ShowMoraleBuildingPanel(false);
        }
        #endregion

        public void CloseInfoPanel()
        {
            CloseCanvas(InfoPanel);
            //CloseCanvas(AnthillPanel);
            //CloseCanvas(FlyhomePanel);
            //CloseCanvas(SlughomePanel);
            //CloseCanvas(TurretPanel);

            CloseCanvas(BuildUpgradesPanel);
            CloseCanvas(TurretUpgradesPanel);

            OpenCanvas(ActionsPanel);
        }

        public void SetCurrentBuild(BuildBehaviour build)
        {
            _currentBuild = build;
            _currentBuildUpgrades = build.gameObject.GetComponent<BuildUpgrades>();
        }

        public BuildBehaviour GetCurrentBuild() { return _currentBuild; }

        #region UPGRADES

        public void OnUpgradeTimeButtonPressed(int i)
        {
            _currentBuildUpgrades.OnTimeButtonPressed(i);

            SetBuildUpgradeButtonsState();
        }

        public void OnUpgradeTroopButtonPressed(int i)
        {
            _currentBuildUpgrades.OnTroopButtonPressed(i);

            SetBuildUpgradeButtonsState();
        }

        public void OnUpgradeHealthButtonPressed(int i)
        {
            _currentBuildUpgrades.OnTurretHealthButtonPressed(i);

            SetTurretUpgradeButtonsState();
        }

        public void OnUpgradeDamageButtonPressed(int i)
        {
            _currentBuildUpgrades.OnTurretDamageButtonPressed(i);

            SetTurretUpgradeButtonsState();
        }


        public void SetBuildUpgradeButtonsState()
        {
           /* Debug.Log("SetBuildUpgradeButtonsState");
            if (!_currentBuildUpgrades || _currentBuild.BuildType == BuildBehaviour.TypeOfBuild.Turret) { return; }

            if (!_currentBuildUpgrades.IsUpgraded())
            {
                Debug.Log("NO UPGRADED!");
                var timeIndex = 0;
                foreach (Button button in TimeUpgradeButtons)
                {
                    if (ResourceManager.Instance.Energy >= _currentBuildUpgrades.TimeUpgrade[timeIndex].EnergyCost &&
                        ResourceManager.Instance.Biomass >= _currentBuildUpgrades.TimeUpgrade[timeIndex].BiomassCost) { button.interactable = true; }
                    else { button.interactable = false; }

                    timeIndex++;
                }

                var troopIndex = 0;
                foreach (Button button in TroopUpgradeButtons)
                {
                    if (ResourceManager.Instance.Energy >= _currentBuildUpgrades.TroopUpgrade[troopIndex].EnergyCost &&
                        ResourceManager.Instance.Biomass >= _currentBuildUpgrades.TroopUpgrade[troopIndex].BiomassCost) { button.interactable = true; }
                    else { button.interactable = false; }

                    troopIndex++;
                }
            }

            else if (_currentBuildUpgrades.GetTypeOfBuildUpgrade()) //Time Upgrade
            {
                foreach (Button button in TroopUpgradeButtons)
                {
                    button.interactable = false;
                }

                var timeIndex = 0;
                for (int i = 0; i < _currentBuildUpgrades.GetUpgradeIndex(); i++)
                {
                    TimeUpgradeButtons[i].interactable = false;
                    timeIndex++;
                }
                for(int i = timeIndex; i < _currentBuildUpgrades.TimeUpgrade.Count; i++)
                {
                    if (ResourceManager.Instance.Energy >= _currentBuildUpgrades.TimeUpgrade[timeIndex].EnergyCost &&
                        ResourceManager.Instance.Biomass >= _currentBuildUpgrades.TimeUpgrade[timeIndex].BiomassCost) { TimeUpgradeButtons[i].interactable = true; }
                    else { TimeUpgradeButtons[i].interactable = false; }
                }
            }

            else if (!_currentBuildUpgrades.GetTypeOfBuildUpgrade()) //Troop Upgrade
            {
                foreach (Button button in TimeUpgradeButtons)
                {
                    button.interactable = false;
                }

                var troopIndex = 0;
                for (int i = 0; i < _currentBuildUpgrades.GetUpgradeIndex(); i++)
                {
                    TroopUpgradeButtons[i].interactable = false;
                    troopIndex++;
                }
                for (int i = troopIndex; i < _currentBuildUpgrades.TimeUpgrade.Count; i++)
                {
                    if (ResourceManager.Instance.Energy >= _currentBuildUpgrades.TroopUpgrade[troopIndex].EnergyCost &&
                        ResourceManager.Instance.Biomass >= _currentBuildUpgrades.TroopUpgrade[troopIndex].BiomassCost) { TroopUpgradeButtons[i].interactable = true; }
                    else { TroopUpgradeButtons[i].interactable = false; }
                }
            }*/
        }

        public void SetTurretUpgradeButtonsState()
        {
            if (!_currentBuildUpgrades || _currentBuild.BuildType != BuildBehaviour.TypeOfBuild.Turret) { return; }

            if (!_currentBuildUpgrades.IsUpgraded())
            {
                var healthIndex = 0;
                foreach (Button button in HealthUpgradeButtons)
                {
                    if (ResourceManager.Instance.Energy >= _currentBuildUpgrades.TurretHealthUpgrade[healthIndex].EnergyCost &&
                        ResourceManager.Instance.Biomass >= _currentBuildUpgrades.TurretHealthUpgrade[healthIndex].BiomassCost) { button.interactable = true; }
                    else { button.interactable = false; }

                    healthIndex++;
                }

                var damageIndex = 0;
                foreach (Button button in DamageUpgradeButtons)
                {
                    if (ResourceManager.Instance.Energy >= _currentBuildUpgrades.TurretDamageUpgrade[damageIndex].EnergyCost &&
                        ResourceManager.Instance.Biomass >= _currentBuildUpgrades.TurretDamageUpgrade[damageIndex].BiomassCost) { button.interactable = true; }
                    else { button.interactable = false; }

                    damageIndex++;
                }
            }

            else if (_currentBuildUpgrades.GetTypeOfBuildUpgrade()) //Health Upgrade
            {
                foreach (Button button in DamageUpgradeButtons)
                {
                    button.interactable = false;
                }

                var healthIndex = 0;
                for (int i = 0; i < _currentBuildUpgrades.GetUpgradeIndex(); i++)
                {
                    HealthUpgradeButtons[i].interactable = false;
                    healthIndex++;
                }
                for (int i = healthIndex; i < _currentBuildUpgrades.TurretHealthUpgrade.Count; i++)
                {
                    if (ResourceManager.Instance.Energy >= _currentBuildUpgrades.TurretHealthUpgrade[healthIndex].EnergyCost &&
                        ResourceManager.Instance.Biomass >= _currentBuildUpgrades.TurretHealthUpgrade[healthIndex].BiomassCost) { HealthUpgradeButtons[i].interactable = true; }
                    else { HealthUpgradeButtons[i].interactable = false; }
                }
            }

            else if (!_currentBuildUpgrades.GetTypeOfBuildUpgrade()) //Damage Upgrade
            {
                foreach (Button button in HealthUpgradeButtons)
                {
                    button.interactable = false;
                }

                var damageIndex = 0;
                for (int i = 0; i < _currentBuildUpgrades.GetUpgradeIndex(); i++)
                {
                    DamageUpgradeButtons[i].interactable = false;
                    damageIndex++;
                }
                for (int i = damageIndex; i < _currentBuildUpgrades.TurretDamageUpgrade.Count; i++)
                {
                    if (ResourceManager.Instance.Energy >= _currentBuildUpgrades.TurretDamageUpgrade[damageIndex].EnergyCost &&
                        ResourceManager.Instance.Biomass >= _currentBuildUpgrades.TurretDamageUpgrade[damageIndex].BiomassCost) { DamageUpgradeButtons[i].interactable = true; }
                    else { DamageUpgradeButtons[i].interactable = false; }
                }
            }
        }
        #endregion

        #region HEALTH

        public void SetBuildHealth(float current, float max, BuildBehaviour build)
        {
            SetCurrentBuild(build);

            if (current > max) { current = max; }

            HealthText.text = current.ToString() + "/" + max.ToString();

            //HealthSlider.maxValue = max;
            //HealthSlider.value = current;
            
            if (current < max &&
                ResourceManager.Instance.Biomass >= build.GetBiomassRepairCost() &&
                ResourceManager.Instance.Food >= build.GetFoodRepairCost()
                && !build.GetReparationState()) { RepairButton.interactable = true; }
            else { RepairButton.interactable = false; }
        }

        public void OnRepairButtonPressed()
        {
            _currentBuild = SelectorRTS.Instance.ObjectsSelected[0].GetComponent<BuildBehaviour>();
            _currentBuild.RepairBuild();
            RepairButton.interactable = false;
        }

        #endregion
        public GameObject[] GetTopLevelChildren(Transform Parent)
        {
            GameObject[] Children = new GameObject[Parent.childCount];
            for (int ID = 0; ID < Parent.childCount; ID++)
            {
                Children[ID] = Parent.GetChild(ID).gameObject;
            }
            return Children;
        }

        private void InitInfoPanelGroup()
        {
            var ChildrenFirstLevel = GetTopLevelChildren(InfoGroupPanel.transform.GetChild(0).transform);
            _icons = new Image[ChildrenFirstLevel.Length];
            for (int i = 0; i < ChildrenFirstLevel.Length; i++)
            {
                _icons[i] = ChildrenFirstLevel[i].GetComponent<Image>();
            }
            _healths = new Slider[ChildrenFirstLevel.Length];
            _healths = InfoGroupPanel.GetComponentsInChildren<Slider>();
        }

        private void InitInfoPanelGroupCompact()
        {
            _amounts =  InfoGroupPanelCompact.transform.GetChild(0).GetComponentsInChildren<TMP_Text>();
            _iconsCompactGO = GetTopLevelChildren(InfoGroupPanelCompact.transform.GetChild(0).transform);
        }

        public void UpdateInfoPanel()
        {
            if (ShowInformationOfSelectable)
            {
                if (SelectorRTS.Instance.ObjectsSelected.Count == 1)
                {
                    if (SelectorRTS.Instance.ObjectsSelected[0] !=null && SelectorRTS.Instance.ObjectsSelected[0].TryGetComponent<Selectable>(out Selectable selectable))
                    {
                        AuxPanel.SetActive(true);
                        bool isBuilding = selectable.gameObject.TryGetComponent<Building>(out Building b);

                        if (!InfoPanel.activeInHierarchy)
                        {
                            OpenCanvas(InfoPanel);
                            CloseCanvas(InfoGroupPanel);
                            CloseCanvas(InfoGroupPanelCompact);
                            CloseCanvas(BuildPanel);
                            ShowVisionBuildingPanel(false);
                            ShowMoraleBuildingPanel(false);
                            ShowResourcePanel(false);

                            if (isBuilding)
                            {
                                AudioManager.Instance.PlaySelectBuildingSound();
                            }
                            else
                            {
                                if (selectable.GetComponent<Gatherer>() != null)
                                {
                                    AudioManager.Instance.PlaySelectLarvaSound();
                                }
                                else if (selectable.GetComponent<Warrior>() != null)
                                {
                                    if (selectable.GetComponent<Warrior>().warriorType == Warrior.WarriorType.Melee)
                                    {
                                        AudioManager.Instance.PlaySelectHormigaSound();
                                    }
                                    else if (selectable.GetComponent<Warrior>().warriorType == Warrior.WarriorType.Distance)
                                    {
                                        AudioManager.Instance.PlaySelectBabosaSound();
                                    }
                                    else if (selectable.GetComponent<Warrior>().warriorType == Warrior.WarriorType.Air)
                                    {
                                        AudioManager.Instance.PlaySelectMoscaSound();
                                    }
                                }
                            }
                        }
                        SelectableName.text = selectable._atributteManager._name;
                        SelectableImage.sprite = selectable._atributteManager._image;
                        SelectableDescription.text = selectable._atributteManager._description;
                        SelectableHealth.value = selectable.GetComponent<Selectable>()._atributteManager.lifeBar.value;
                        SelectableHealthText.text = Math.Ceiling(selectable.GetComponent<Selectable>()._atributteManager._currentHealth) + "/" + selectable.GetComponent<Selectable>()._atributteManager._maxHealth;

                        BuildUpgrades buildUpgrades = SelectorRTS.Instance.ObjectsSelected[0].GetComponent<BuildUpgrades>();
                        //RepairButton.gameObject.SetActive(isBuilding);
                        if (isBuilding)
                        {
                            SetBuildHealth(b._atributteManager._currentHealth, b._atributteManager._maxHealth, selectable.gameObject.GetComponent<BuildBehaviour>());

                            if (!InfoPanel.activeInHierarchy)
                            {
                                AudioManager.Instance.PlaySelectBuildingSound();
                            }
                        }
                        if (isBuilding && !selectable.gameObject.TryGetComponent<Turret>(out _))
                        {
                            
                            SetAuxPanel(selectable.gameObject);
                            OpenCanvas(BuildUpgradesPanel, TurretUpgradesPanel);
                            HeaderBuildUpgradesText.text = buildUpgrades.IsUpgraded() ? "Upgraded" : InitialHeaderBuildUpgradesText;
                            SetInteractiveButtons(0, buildUpgrades.IsUpgraded() ? false : true);
                        }
                        else if (isBuilding)
                        {
                            SetAuxPanelTurret(selectable.gameObject);
                            OpenCanvas(TurretUpgradesPanel, BuildUpgradesPanel);
                            HeaderTurretUpgradesText.text = buildUpgrades.IsUpgraded() ? "Upgraded" : InitialHeaderTurretUpgradesText;
                            SetInteractiveButtons(2, buildUpgrades.IsUpgraded() ? false : true);
                        }
                        else
                        {
                            SetAuxPanelTroop(selectable.gameObject);
                            CloseCanvas(BuildUpgradesPanel);
                            CloseCanvas(TurretUpgradesPanel);

                            if (!InfoPanel.activeInHierarchy)
                            {
                                
                            }
                        }
                    }
                }
                else if(SelectorRTS.Instance.TroopsSelected.Count > 1 && SelectorRTS.Instance.TroopsSelected.Count <= 12)
                {
                    if (!InfoGroupPanel.activeInHierarchy)
                    {
                        OpenCanvas(InfoGroupPanel);
                        CloseCanvas(InfoPanel);
                        CloseCanvas(InfoGroupPanelCompact);
                        CloseCanvas(BuildPanel);
                        CloseCanvas(AuxPanel);
                        CloseCanvas(BuildUpgradesPanel);
                        CloseCanvas(TurretUpgradesPanel);
                        ShowVisionBuildingPanel(false);
                        ShowMoraleBuildingPanel(false);
                        ShowResourcePanel(false);
                    }
                    for (int i = 0; i< 12; i++)
                    {
                        if (i < SelectorRTS.Instance.TroopsSelected.Count)
                        {
                            if(SelectorRTS.Instance.TroopsSelected[i] !=null && SelectorRTS.Instance.TroopsSelected[i].TryGetComponent<Selectable>(out Selectable selectable))
                            {
                                _icons[i].sprite = selectable._atributteManager._image;
                                _healths[i].value = selectable._atributteManager.lifeBar.value;
                                _icons[i].gameObject.SetActive(true);
                                _healths[i].gameObject.SetActive(true);
                            }
                            else
                            {
                                _icons[i].gameObject.SetActive(false);
                                _healths[i].gameObject.SetActive(false);
                            }
                        }
                        else
                        {
                            _icons[i].gameObject.SetActive(false);
                            _healths[i].gameObject.SetActive(false);
                        }
                    }
                }
                else if (SelectorRTS.Instance.TroopsSelected.Count > 12)
                {
                    if (!InfoGroupPanelCompact.activeInHierarchy)
                    {
                        OpenCanvas(InfoGroupPanelCompact);
                        CloseCanvas(InfoPanel);
                        CloseCanvas(InfoGroupPanel);
                        CloseCanvas(BuildPanel);
                        CloseCanvas(AuxPanel);
                        CloseCanvas(BuildUpgradesPanel);
                        CloseCanvas(TurretUpgradesPanel);
                        ShowVisionBuildingPanel(false);
                        ShowMoraleBuildingPanel(false);
                        ShowResourcePanel(false);
                    }
                    for (int i = 0; i < _amountsValue.Length; i++)
                    {
                        _amountsValue[i] = 0;
                    }
                    for (int i = 0; i < SelectorRTS.Instance.TroopsSelected.Count; i++)
                    {
                        var SelectableGO = SelectorRTS.Instance.TroopsSelected[i];
                        if (SelectableGO.TryGetComponent<Mover>(out Mover m))
                        {
                            if (SelectableGO.TryGetComponent<Queen>(out Queen q))
                            {
                                _amountsValue[0]++;
                            }
                            else if(SelectableGO.TryGetComponent<Gatherer>(out Gatherer g))
                            {
                                _amountsValue[1]++;
                            }
                            else if(SelectableGO.TryGetComponent<Warrior>(out Warrior w))
                            {
                                switch (w.warriorType)
                                {
                                    case Warrior.WarriorType.Melee:
                                        _amountsValue[2]++;
                                        break;
                                    case Warrior.WarriorType.Distance:
                                        _amountsValue[3]++;
                                        break;
                                    case Warrior.WarriorType.Air:
                                        _amountsValue[4]++;
                                        break;
                                }
                            }
                        }
                    }

                    for(int i = 0; i < _amounts.Length; i++)
                    {
                        if (_amountsValue[i] > 0)
                        {
                            _iconsCompactGO[i].SetActive(true);
                            _amounts[i].text = _amountsValue[i].ToString();
                        }
                        else
                        {
                            _iconsCompactGO[i].SetActive(false);
                        }
                    }
                }
                else
                {
                    if (InfoGroupPanel.activeInHierarchy)
                    {
                        CloseCanvas(InfoGroupPanel);
                    }

                    if (InfoPanel.activeInHierarchy)
                    {
                        CloseCanvas(InfoPanel);
                    }

                    if (InfoGroupPanelCompact.activeInHierarchy)
                    {
                        CloseCanvas(InfoGroupPanelCompact);
                    }

                    if (AuxPanel.activeInHierarchy)
                    {
                        CloseCanvas(AuxPanel);
                    }

                    if (ResourcePanel.activeInHierarchy)
                    {
                        ShowResourcePanel(true);
                    }

                    if (BuildUpgradesPanel.activeInHierarchy)
                        CloseCanvas(BuildUpgradesPanel);

                    if (TurretUpgradesPanel.activeInHierarchy)
                        CloseCanvas(TurretUpgradesPanel);
                }
            }
            else
            {
                if (InfoGroupPanel.activeInHierarchy)
                {
                    CloseCanvas(InfoGroupPanel);
                }

                if (InfoPanel.activeInHierarchy)
                {
                    CloseCanvas(InfoPanel);
                }

                if (InfoGroupPanelCompact.activeInHierarchy)
                {
                    CloseCanvas(InfoGroupPanelCompact);
                }

                if (AuxPanel.activeInHierarchy)
                {
                    CloseCanvas(AuxPanel);
                }

                if (SelectorRTS.Instance.BuildingsSelected.Count == 1)
                {
                    GameObject selectableGO = SelectorRTS.Instance.BuildingsSelected[0];
                    bool isBuilding = selectableGO.TryGetComponent<Building>(out Building b);
                    if (isBuilding && !selectableGO.TryGetComponent<Turret>(out _))
                    {
                        OpenCanvas(BuildUpgradesPanel, TurretUpgradesPanel);
                    }
                    else if(isBuilding)
                    {
                        OpenCanvas(TurretUpgradesPanel,BuildUpgradesPanel);
                    }
                    else
                    {
                        CloseCanvas(BuildUpgradesPanel);
                        CloseCanvas(TurretUpgradesPanel);
                    }
                }
            }
        }

        private void SetAuxPanel(GameObject buildingGO)
        {
            Description.enabled = false;
            UnitImage.enabled = true;
            UnitImage.sprite = buildingGO.GetComponent<TroopEvolver>().ImageTroopEvolved;
            AttackTxt.text = (Math.Ceiling(buildingGO.GetComponent<TroopEvolver>().AtributtesTroopEvolved.Damage * 10) / 10).ToString();
            HealthTxt.text = (Math.Ceiling(buildingGO.GetComponent<TroopEvolver>().AtributtesTroopEvolved.MaxHealth * 10) / 10).ToString();
            SpeedTxt.text = (Math.Ceiling(buildingGO.GetComponent<TroopEvolver>().AtributtesTroopEvolved.MoveSpeed * 10) / 10).ToString();
        }

        private void SetAuxPanelTroop(GameObject troopGO)
        {
            Description.enabled = true;
            UnitImage.enabled = false;
            Description.text = troopGO.GetComponent<Selectable>()._atributteManager._littleDescription;
            AttackTxt.text = (Math.Ceiling(troopGO.GetComponent<Selectable>()._atributteManager._attackDamage * 10) / 10).ToString();
            HealthTxt.text = (Math.Ceiling(troopGO.GetComponent<Selectable>()._atributteManager._maxHealth * 10) / 10).ToString();
            SpeedTxt.text = (Math.Ceiling(troopGO.GetComponent<Selectable>()._atributteManager._moveSpeed * 10) / 10).ToString();
        }

        private void SetAuxPanelTurret(GameObject turretGO)
        {
            Description.enabled = false;
            UnitImage.enabled = true;
            UnitImage.sprite = turretGO.GetComponent<Turret>().Mariquita;
            AttackTxt.text = (Math.Ceiling(turretGO.GetComponent<Selectable>()._atributteManager._attackDamage * 10) / 10).ToString();
            HealthTxt.text = (Math.Ceiling(turretGO.GetComponent<Selectable>()._atributteManager._maxHealth * 10) / 10).ToString();
            SpeedTxt.text = (Math.Ceiling(turretGO.GetComponent<Selectable>()._atributteManager._moveSpeed * 10) / 10).ToString();
        }

        public void SwitchInformationStatus()
        {
            ShowInformationOfSelectable = !ShowInformationOfSelectable;
        }

        public void SetUpgrade(int upgradeType)
        {
            BuildUpgrades buildUpgrades = SelectorRTS.Instance.ObjectsSelected[0].GetComponent<BuildUpgrades>();
            if (!buildUpgrades.IsUpgraded())
            {
                switch (upgradeType)
                {
                    case (int)UpgradeType.TroopDamageUpgrade:
                        buildUpgrades.OnTroopButtonPressed(0);
                        break;
                    case (int)UpgradeType.TroopTimeToEvolvedUpgrade:
                        buildUpgrades.OnTimeButtonPressed(0);
                        break;
                    case (int)UpgradeType.TurretDamageUpgrade:
                        buildUpgrades.OnTurretDamageButtonPressed(0);
                        break;
                    case (int)UpgradeType.TurretHealthUpgrade:
                        buildUpgrades.OnTurretHealthButtonPressed(0);
                        break;
                }
            }
        }

        public void SetInteractiveButtons(int type, bool active)
        {
            BuildUpgrades buildUpgrades = SelectorRTS.Instance.ObjectsSelected[0].GetComponent<BuildUpgrades>();
            switch (type)
            {
                case (int)UpgradeType.TroopDamageUpgrade:
                case (int)UpgradeType.TroopTimeToEvolvedUpgrade:
                    foreach (Button button in TimeUpgradeButtons)
                    {
                        button.interactable = active;
                        if (ResourceManager.Instance.Biomass < buildUpgrades.TimeUpgrade[0].BiomassCost || ResourceManager.Instance.Energy < buildUpgrades.TimeUpgrade[0].EnergyCost)
                            button.interactable = false;
                    }

                    foreach (Button button in TroopUpgradeButtons)
                    {
                            button.interactable = active;
                        if (ResourceManager.Instance.Biomass < buildUpgrades.TroopUpgrade[0].BiomassCost || ResourceManager.Instance.Energy < buildUpgrades.TroopUpgrade[0].EnergyCost)
                            button.interactable = false;
                    }
                    break;
                case (int)UpgradeType.TurretDamageUpgrade:
                case (int)UpgradeType.TurretHealthUpgrade:
                    foreach (Button button in HealthUpgradeButtons)
                    {
                            button.interactable = active;
                        if (ResourceManager.Instance.Biomass < buildUpgrades.TurretHealthUpgrade[0].BiomassCost || ResourceManager.Instance.Energy < buildUpgrades.TurretHealthUpgrade[0].EnergyCost)
                            button.interactable = false;
                    }

                    foreach (Button button in DamageUpgradeButtons)
                    {
                            button.interactable = active;
                        if (ResourceManager.Instance.Biomass < buildUpgrades.TurretDamageUpgrade[0].BiomassCost || ResourceManager.Instance.Energy < buildUpgrades.TurretDamageUpgrade[0].EnergyCost)
                            button.interactable = false;
                    }
                    break;
            }
        }

        public void ShowLifeBars()
        {
            if (showLifeBars)
                Camera.main.cullingMask = Camera.main.cullingMask & ~(1 << 25);
            else
                Camera.main.cullingMask = Camera.main.cullingMask | (1 << 25);

            showLifeBars = !showLifeBars;
        }

        public void ShowVisionBuildingPanel(bool state)
        {
            InfoCapturableVisionPanel.SetActive(state);
        }

        public void ShowMoraleBuildingPanel(bool state)
        {
            InfoCapturableMoralePanel.SetActive(state);
        }

        public void ShowResourcePanel(bool state)
        {
            if (SelectorRTS.Instance.ResourceSelected != null)
            {
                ResourcePanel.SetActive(state);
                var resource = SelectorRTS.Instance.ResourceSelected.GetComponent<Resource>();

                ResourceName.text = resource.Name;
                ResourceDescription.text = resource.Description;
                ResourceHealthText.text = resource._currentValue + "/" + resource._initialValue;
                ResourceHealth.value = resource._currentValue/resource._initialValue;
                ResourceImage.sprite = resource.Render.GetComponent<SpriteRenderer>().sprite;
            }
            else
            {
                ResourcePanel.SetActive(false);
            }
        }
    }
}

public enum UpgradeType
{
    TroopDamageUpgrade = 0, TroopTimeToEvolvedUpgrade= 1, TurretDamageUpgrade =2, TurretHealthUpgrade =3
}
