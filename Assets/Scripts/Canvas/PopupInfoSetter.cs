﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PopupInfoSetter : MonoBehaviour
{
    public TMP_Text TimeText;
    public TMP_Text NameText;
    public TMP_Text DescriptionText;
    public TMP_Text EnergyText;
    public TMP_Text BiomassText;
    public TMP_Text FoodText;

    public void SetPopupInfo(float time, string name, string description, int energy, int biomass, int food)
    {
        TimeText.text = time.ToString();
        NameText.text = name;
        DescriptionText.text = description;
        EnergyText.text = energy.ToString();
        BiomassText.text = biomass.ToString();
        FoodText.text = food.ToString();
        
        EnergyText.transform.parent.gameObject.SetActive(energy != 0);
        FoodText.transform.parent.gameObject.SetActive(food != 0);
    }
}
