﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.UI;

public class ShowPopup : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public RectTransform Popup;
    public float yOffset;
    public bool flipSprite;
    public Image popupImage;

    [Header("Popup Information")]
    public float Time;
    public string Name;
    [TextArea(3, 5)]
    public string Description;
    public int Energy;
    public int Biomass;
    public int Food;

    private bool _isOver;

    private PopupInfoSetter _popupSetter;

    void Awake()
    {
        _popupSetter = Popup.gameObject.GetComponent<PopupInfoSetter>();
    }

    private void Update()
    {
        if (_isOver)
        {
            MovePanel();
        }
    }

    void MovePanel()
    {
        Popup.position = Input.mousePosition + new Vector3(0,yOffset);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        _popupSetter.SetPopupInfo(Time, Name, Description, Energy, Biomass, Food);
        _isOver = true;
        if (flipSprite)
        {
            popupImage.transform.localScale = new Vector3(-1, 1, 1);
            Popup.pivot = new Vector2(1, 0);
            
        }
        else
        {
            popupImage.transform.localScale = new Vector3(1, 1, 1);
            Popup.pivot = new Vector2(0, 0);
        }
        Popup.gameObject.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _isOver = false;
        Popup.gameObject.SetActive(false);
    }
}