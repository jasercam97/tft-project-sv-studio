﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEvents : MonoBehaviour
{

    void Start()
    {
    }

    public void Attack()
    {
    }

    public void CommandMove()
    {
    }

    public void CommandAttack()
    {
    }

    public void CreateTroop()
    {
    }

    public void Construction()
    {
    }

    public void Repair()
    {
    }
    public void Shoot()
    {
    }

    public void Impact()
    {
    }

    public void Death()
    {
    }

    public void Victory()
    {

    }

    public void GameOver()
    {

    }
}
