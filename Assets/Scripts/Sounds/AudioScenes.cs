using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScenes : MonoBehaviour
{
    public const string KEY_MUSIC_VOLUME = "MusicVolume";
    public const string KEY_SFX_VOLUME = "SFXVolume";

    [FMODUnity.EventRef]
    public string SceneSoundEvent;
    private FMOD.Studio.EventInstance SceneSoundInstance;

    FMOD.Studio.Bus SFX;

    public float SFXVolume;

    void Awake()
    {
        SFXVolume = PlayerPrefs.GetFloat(KEY_SFX_VOLUME, 0.5f);
        SFX = FMODUnity.RuntimeManager.GetBus("bus:/SFX");
    }

    void Start()
    {
        SFX.setVolume(SFXVolume);
        SceneSoundInstance = FMODUnity.RuntimeManager.CreateInstance(SceneSoundEvent);
        SceneSound();
    }

    public void SceneSound()
    {
        SceneSoundInstance.start();
    }

    private void OnDestroy()
    {
        Debug.Log("OnDestroy");
        SceneSoundInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
    }
}
