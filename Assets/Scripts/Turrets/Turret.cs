﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(LineRenderer))]
[RequireComponent(typeof(SphereCollider))]
public class Turret : MonoBehaviour
{

    public Sprite Mariquita;
    [Header("Range Zone")]
    [Range(0, 50)]
    public int Segments = 50;
    public float Width = 0.5f;
    public float TurretRange;

    [Header("Shoot Settings")]
    public GameObject ProjectilePrefab;
    public Transform ShootPos;
    public float ShootRate;
    public LayerMask TroopLayer;

    private LineRenderer _line;
    private SphereCollider _collider;
    private float ShootTimer = 0;

    private Selectable selectable;
    private AtributteManager _atributteManager;
    private Selectable _currentTroop;

    [SerializeField] private List<GameObject> _listOfTargets;
    private GameObject _currentTarget;
    AnimationEvents _events;

    public bool canShot = false;

    private void Awake()
    {
        _events = GetComponent<AnimationEvents>();
        selectable = GetComponent<Selectable>();
        _atributteManager = GetComponent<AtributteManager>();
    }

    private void Start()
    {
        _listOfTargets.Clear();

        _line = GetComponent<LineRenderer>();
        _collider = GetComponent<SphereCollider>();


        _line.positionCount = Segments + 1;
        _line.useWorldSpace = false;
        _line.startWidth = Width;
        _line.endWidth = Width;

        _line.enabled = true;

        //_collider.radius = TurretRange;

        CreatePoints();
    }

    private void Update()
    {
        if (selectable.ready)
        {
            DetectEnemies();
            if (_listOfTargets.Count > 0)
            {
                if (!_currentTarget) { SetNewTarget(); }

                SetRangeColor(Color.red);
                ShootToTarget(_currentTarget);
            }
            else
            {
                SetRangeColor(Color.yellow);
                ShootTimer = 0;
            }
        }
    }

    void SetNewTarget()
    {
        float dist = 0f;

        foreach(GameObject t in _listOfTargets)
        {
            if(Vector3.Distance(transform.position, t.transform.position) < dist || dist == 0)
            {
                dist = Vector3.Distance(transform.position, t.transform.position);
                _currentTarget = t;
            }
        }
    }

    void ShootToTarget(GameObject t)
    {
        if(!canShot){ return; }
        
        if (ShootTimer < ShootRate)
        {
            ShootTimer += Time.deltaTime;
        }
        else
        {
            if (Vector3.Distance(transform.position, _currentTarget.transform.position) > TurretRange)
            {
                _currentTarget = null;
            }
            else
            {
                AudioManager.Instance.PlayTurretShootSound();
                ShootTimer = 0;
                GetComponent<ShootController>().Fire(selectable.Group, selectable.Team, _atributteManager._attackDamage, t.GetComponent<Selectable>().RenderTroop, false, _atributteManager._projectileSpeed);
            }
        }
    }

    public void SetNewDamage(int newDmg)
    {
        _atributteManager._attackDamage = newDmg;
    }

    void SetRangeColor(Color c)
    {
        _line.startColor = c;
        _line.endColor = c;
    }

    private void CreatePoints()
    {
        float x;
        float y;

        float angle = 20f;

        for (int i = 0; i < (Segments + 1); i++)
        {
            x = Mathf.Sin(Mathf.Deg2Rad * angle) * TurretRange;
            y = Mathf.Cos(Mathf.Deg2Rad * angle) * TurretRange;

            _line.SetPosition(i, new Vector3(x, y, 0));

            angle += (360f / Segments);
        }
    }

    public void AllowShot(bool state)
    {
        canShot = state;
        
    }
/*
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Troop")) { _listOfTargets.Add(other.gameObject); }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Troop")) 
        {
            if(other.gameObject == _currentTarget) { _currentTarget = null; }
            _listOfTargets.Remove(other.gameObject); 
        }
    }
*/
    #region DETECTION ENEMIES
    protected void DetectEnemies()
    {
        if (_currentTarget == null)
        {
            _listOfTargets.Clear();

            foreach (Collider troop in Physics.OverlapSphere(transform.position, TurretRange, TroopLayer))
            {
                if (troop.gameObject.activeInHierarchy)
                {
                    _currentTroop = troop.GetComponent<Selectable>();

                    if (_currentTroop != null && _currentTroop.Team != selectable.Team && _currentTroop.Group != -1 && !_currentTroop.IsDead)    // Detects troops only if they are in opposite Team and alive
                    {
                        _listOfTargets.Add(troop.gameObject);
                    }
                }
            }
        }
    }
    #endregion
/*
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, ShootRate);
    }
*/
}
