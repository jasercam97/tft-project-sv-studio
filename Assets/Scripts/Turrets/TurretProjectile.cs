﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretProjectile : MonoBehaviour
{
    public float Speed;

    private float _damage;
    [SerializeField] private GameObject _target;

    private void Update()
    {
        if (_target)
        {
            transform.LookAt(_target.transform);
            transform.position = Vector3.Lerp(transform.position, _target.transform.position, Speed * Time.deltaTime);

            /*
            if(Vector3.Distance(transform.position, _target.transform.position) < 0.1f) 
            {
                Selectable troop = _target.GetComponent<Selectable>();
                DealDamage(troop, Damage);
            }*/
        }
    }

    public void SetTarget(GameObject t, int dmg)
    {
        _target = t;
        _damage = dmg;
    }

    private void DealDamage(Selectable troop, float damage)
    {
        troop.TakeDamage(damage);
        Destroy(gameObject);
    }
}
