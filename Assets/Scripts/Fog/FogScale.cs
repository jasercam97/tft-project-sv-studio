/*
 * Jose Manuel Herrera Vera 
 * @projects_ia (Twitter)
 * https://josemhv.itch.io
 *
 * Fog Scale RTS v1.0
 * 
 * Date: 2021/06/27
 */
using UnityEngine;

namespace Josemhv.FogOfWar.FogScale
{
    [ExecuteAlways]
    public class FogScale : MonoBehaviour
    {
        [Range(1, 200)] [SerializeField] private float _sizeFactor = 1;
        [SerializeField] private Camera _mainCameraOfFog;
        [SerializeField] private Camera _secondaryCameraOfFog;
        [SerializeField] private Transform _fog;

        void Update()
        {
            _fog.localScale = new Vector3(_sizeFactor / 5, _fog.localScale.y, _sizeFactor / 5);
            _mainCameraOfFog.orthographicSize = _sizeFactor;
            _secondaryCameraOfFog.orthographicSize = _sizeFactor;
        }
    }
}
