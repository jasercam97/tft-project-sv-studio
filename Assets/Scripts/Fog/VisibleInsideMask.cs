using System.Collections.Generic;
using UnityEngine;

public class VisibleInsideMask : MonoBehaviour
{
    public List<GameObject> gameObjectsInArea;
    public GameObject HealthCanvas;
    public GameObject Mask;
    public GameObject Shadow;
    public GameObject UpgradeEffect;
    private int Team=-1;

    private void Awake()
    {
        gameObjectsInArea = new List<GameObject>();
    }

    public void Initialize(int team)
    {
        Team = team;
        InvokeRepeating(nameof(CheckGameObjectsInsideMask), 0.1f, 0.1f);
    }

    public void DestroyVisibleInsideMask()
    {
        Destroy(this);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (Team != -1)
        {
            if (other.CompareTag("Capturable") && other.gameObject.GetComponentInParent<CapturableBuilding>().team == SelectorRTS.Instance._team && !gameObjectsInArea.Contains(other.gameObject))
            {
                gameObjectsInArea.Add(other.gameObject);
                Debug.Log("VisibleInsideMaskEnterCapturable");
            }
            else if (other.CompareTag("Mask") && other.gameObject.GetComponentInParent<Selectable>().Team == SelectorRTS.Instance._team && !gameObjectsInArea.Contains(other.gameObject))
            {
                gameObjectsInArea.Add(other.gameObject);
                Debug.Log("VisibleInsideMaskEnter");
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (Team != -1)
        {
            if (other.CompareTag("Capturable") && other.gameObject.GetComponentInParent<CapturableBuilding>().team == SelectorRTS.Instance._team && gameObjectsInArea.Contains(other.gameObject))
            {
                gameObjectsInArea.Remove(other.gameObject);
                Debug.Log("VisibleInsideMaskExitCapturable");
            }
            else if (other.CompareTag("Mask") && other.gameObject.GetComponentInParent<Selectable>().Team == SelectorRTS.Instance._team && gameObjectsInArea.Contains(other.gameObject))
            {
                Debug.Log("VisibleInsideMaskExit");
                gameObjectsInArea.Remove(other.gameObject);
            }
        }
    }

    private void CheckGameObjectsInsideMask()
    {
        for(int i = 0;i< gameObjectsInArea.Count;i++)
        {
            if (gameObjectsInArea[i] == null)
                gameObjectsInArea.Remove(gameObjectsInArea[i]);
        }
        SetStateMesh(gameObjectsInArea.Count <= 0 ? false : true);
    }

    private void SetStateMesh(bool state)
    {
        SpriteRenderer mesh;
        if (TryGetComponent(out mesh))
        {
            mesh.enabled = state;
            if(HealthCanvas != null) HealthCanvas.SetActive(state);
            if (Shadow != null) Shadow.SetActive(state);
            if (UpgradeEffect != null) UpgradeEffect.SetActive(state);
        }
        var SpriteRenderers = GetComponentsInChildren<SpriteRenderer>();
        foreach (var SpriteRenderer in SpriteRenderers)
        {
            SpriteRenderer.enabled = state;
            if(HealthCanvas!=null) HealthCanvas.SetActive(state);
            if (Shadow != null) Shadow.SetActive(state);
            if (UpgradeEffect != null) UpgradeEffect.SetActive(state);
        }
    }
}