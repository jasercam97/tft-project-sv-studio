﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameManagers;
using UnityEngine;
using Utils;

public class ResourceTrigger : Singleton<ResourceTrigger>
{
    public List<ResourceEvent> resourceEvents;

    public void RegisterResourceChange(float food, float biomass, float energy)
    {
        for(int i =0; i<resourceEvents.Count;i++)
        {
            
            switch (resourceEvents[i].eventType)
            {
                case TypeOfResourceEvents.FirstResourceCollected:
                    if(CheckCondition(resourceEvents[i], food, biomass, energy))
                    {
                        resourceEvents[i].ResolveEvent();
                        resourceEvents.Remove(resourceEvents[i]);
                    }
                    break;
                case TypeOfResourceEvents.ResourcesToBuild:
                    if (CheckCondition(resourceEvents[i], food, biomass, energy))
                    {
                        resourceEvents[i].ResolveEvent();
                        resourceEvents.Remove(resourceEvents[i]);
                    }
                    break;
                case TypeOfResourceEvents.ResourcesToUpgrade:
                    if (CheckCondition(resourceEvents[i], food, biomass, energy))
                    {
                        resourceEvents[i].ResolveEvent();
                        resourceEvents.Remove(resourceEvents[i]);
                    }
                    break;
            }
        }
    }

    public bool CheckCondition(ResourceEvent resourceEvent, float food, float biomass, float energy)
    {
        bool f = false,b = false, e = false;
        if (resourceEvent.Food ==-1 || resourceEvent.Food<=food)
        {
            f = true;
        }

        if (resourceEvent.Biomass == -1 || resourceEvent.Biomass <= biomass)
        {
            b = true;
        }

        if (resourceEvent.Energy == -1 || resourceEvent.Energy <= energy)
        {
            e = true;
        }
        return f && b && e;
    }
}

[System.Serializable]
public class ResourceEvent
{
    public string LabelEvent;
    public DialogueObject Dialogue;
    public TypeOfResourceEvents eventType;

    [Header("Valor a obtener para que suceda el evento, si un recurso no es necesario = -1")]
    public float Food;
    public float Biomass;
    public float Energy;

    public void ResolveEvent()
    {
        TutorialManager.Instance.PauseGame();
        DialogueUI.Instance.ShowDialogue(Dialogue);
    }
}

public enum TypeOfResourceEvents
{
    FirstResourceCollected, ResourcesToBuild, ResourcesToUpgrade
}