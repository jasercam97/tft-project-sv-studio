using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    [SerializeField] private DialogueObject dialogue;
    
    public void StartDialogue()
    {
        DialogueUI.Instance.ShowDialogue(dialogue);
    }
    
    public void EndDialogue()
    {
        //if (completeQuest) { QuestManager.Instance.CompleteCurrentQuest(); }

        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider collision)
    {
        StartDialogue();
    }
}
