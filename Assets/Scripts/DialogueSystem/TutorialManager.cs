using System.Collections;
using System.Collections.Generic;
using GameManagers;
using UnityEngine;
using UnityEngine.AI;
using Utils;

public class TutorialManager : Singleton<TutorialManager>
{
    private int _dialoguesResolved = 0;
    public GameObject[] _resources;
    public GameObject PausedPanel;
    public List<GameObject> PanelMarkers;
    public GameObject LimitVertical;

    public void Init()
    {
        PauseGame();
        SetStateActionPanel(false);
        _resources = GameObject.FindGameObjectsWithTag("Resources");
        SetVisibleResources(false, false, false);
        DialogueUI.Instance.Init();
    }

    public int DialoguesResolved
    {
        get { return _dialoguesResolved; }
        set
        {
            _dialoguesResolved = value;
            switch (_dialoguesResolved)
            {
                case 1:
                    ResumeGame();
                    SetStateSelectorRTS(true);
                    SetStateMinimapMovement(true);
                    SetCameraSpeedAndPosition(20, GameManager.Instance.QueenPositionCamera());
                    SetVisibleResources(true, true, false);
                    ActivePanelMarkers(0, 0);
                    break;
                case 2:
                    ResumeGame();
                    SetStateActionPanel(true);
                    CanvasManager.Instance.OpenBuildCanvas();
                    ActivePanelMarkers(1, 1);
                    break;
                case 3:
                    Debug.Log("Aqui");
                    ResumeGame();
                    SetVisibleResources(true, true, true);
                    CanvasManager.Instance.OpenBuildCanvas();
                    ActivePanelMarkers(2, 4);
                    break;
                case 4:
                    ResumeGame();
                    LimitVertical.SetActive(false);
                    DeactivePanelMarkers(0, PanelMarkers.Count);
                    break;
                case 5:
                    if(ResourceManager.Instance.Energy<10)
                        GameManager.Instance.ShowVictory();
                    else
                        ResumeGame();
                    break;
                case 6:
                    GameManager.Instance.ShowVictory();
                    break;
                default:
                    ResumeGame();
                    break;
            }
        }
    }  

    public void SetStateSelectorRTS(bool state)
    {
        SelectorRTS.Instance.enabled = state;
    }

    public void SetStateMinimapMovement(bool state)
    {
        MinimapMovement.Instance.enabled = state;
    }

    public void SetCameraSpeedAndPosition(float speed, Vector3 position)
    {
        RTSCameraController.Instance._movSpeed = speed;
        RTSCameraController.Instance.MoveToPosition(position);
    }

    public void SetStateActionPanel(bool state)
    {
        CanvasManager.Instance.BuildButton.interactable = state;
        CanvasManager.Instance.QueenButton.interactable = state;
    }

    public void SetVisibleResources(bool food, bool biomass, bool energy)
    {
        for (int i =0; i < _resources.Length; i++)
        {
            if(_resources[i] != null && _resources[i].TryGetComponent<Resource>(out Resource resource))
            {
                switch (resource._resourcesType)
                {
                    case Resource.ResourcesType.Food:
                            _resources[i].SetActive(food);
                        break;
                    case Resource.ResourcesType.Biomass:
                            _resources[i].SetActive(biomass);
                        break;
                    case Resource.ResourcesType.Energy:
                            _resources[i].SetActive(energy);
                        break;
                }
            }
        }
    }

    public void SetSpeedTroops(bool play)
    {
        for (int j = 0; j < TeamManager.Instance.TroopsPerGroup.Count; j++)
        {
            for (int i = 0; i < TeamManager.Instance.TroopsPerGroup[j].Count; i++)
            {
                if (play)
                    TeamManager.Instance.TroopsPerGroup[j][i].GetComponent<NavMeshAgent>().speed = TeamManager.Instance.TroopsPerGroup[j][i]._atributteManager._moveSpeed;
                else
                    TeamManager.Instance.TroopsPerGroup[j][i].GetComponent<NavMeshAgent>().speed = 0;
            }
        }
    }

    public void PauseGame()
    {
        Debug.Log("DebugPauseGame");
        SetSpeedTroops(false);
        SetStateSelectorRTS(false);
        SetStateMinimapMovement(false);
        SetCameraSpeedAndPosition(0, RTSCameraController.Instance.transform.position);
        PausedPanel.SetActive(true);
        DialogueUI.Instance.NextDialogue();
    }

    public void ResumeGame()
    {
        Debug.Log("DebugResumeGame");
        SetSpeedTroops(true);
        SetStateSelectorRTS(true);
        SetStateMinimapMovement(true);
        SetCameraSpeedAndPosition(20, RTSCameraController.Instance.transform.position);
        PausedPanel.SetActive(false);
    }

    public void ActivePanelMarkers(int minPos, int maxPos)
    {
        for(int i = 0; i < PanelMarkers.Count; i++)
        {
            if (i >= minPos && i <= maxPos)
                PanelMarkers[i].SetActive(true);
            else
                PanelMarkers[i].SetActive(false);
        }
    }

    public void DeactivePanelMarkers(int minPos, int maxPos)
    {
        for (int i = 0; i < PanelMarkers.Count; i++)
        {
            if (i >= minPos && i <= maxPos)
                PanelMarkers[i].SetActive(false);
            else
                PanelMarkers[i].SetActive(true);
        }
    }

    public void BuildButtonPressed()
    {
        DeactivePanelMarkers(0, PanelMarkers.Count);
    }
}
