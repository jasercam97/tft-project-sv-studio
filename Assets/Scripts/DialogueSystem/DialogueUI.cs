using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Utils;
using static DialogueObject;

public class DialogueUI : Singleton<DialogueUI>
{
    [SerializeField] private GameObject dialogueBox;
    [SerializeField] private TMP_Text speakerLabel;
    [SerializeField] private TMP_Text textLabel;
    [SerializeField] private Image rightSpeakerImage;
    [SerializeField] private Image leftSpeakerImage;
    [SerializeField] private DialogueObject firstDialogue;
    [SerializeField] private Button interactable;
    private DialogueObject lastDialogue;

    private TypeEffect typeEffect;

    private void Awake()
    {
        typeEffect = GetComponent<TypeEffect>();
    }

    public void Init()
    {
        CloseDialogue();

        //FadeScreen.Instance.FadeIn();

        ShowDialogue(firstDialogue);
    }

    void SetSpeakerLabel(string speakerName) { speakerLabel.text = speakerName; }

    void SetRightSpeakerSprite(Sprite sprite) 
    {
        if (rightSpeakerImage != null)
        {
            rightSpeakerImage.sprite = sprite; 
            rightSpeakerImage.enabled = true;
        }

        if (leftSpeakerImage != null)
        {
            leftSpeakerImage.enabled = false;
        }
    }

    void SetLeftSpeakerSprite(Sprite sprite)
    {
        if (rightSpeakerImage != null)
        {
            leftSpeakerImage.sprite = sprite;
            leftSpeakerImage.enabled = true;
        }

        if (rightSpeakerImage != null)
        {
            rightSpeakerImage.enabled = false;
        }
    }

    public void ShowDialogue(DialogueObject dialogueObject)
    {
        dialogueBox.SetActive(true);
        SetInteractable(false);
        StartCoroutine(StepThroughDialogue(dialogueObject));
    }

    public void ShowDialogue()
    {
        dialogueBox.SetActive(true);
        SetInteractable(false);
        StartCoroutine(StepThroughDialogue(lastDialogue));
    }

    private IEnumerator StepThroughDialogue(DialogueObject dialogueObject)
    {
        bool repeatDialogue = false;
        if (lastDialogue == dialogueObject)
            repeatDialogue = true;
        lastDialogue = dialogueObject;
        int dialogueIndex = 0;
        foreach (Speaker speaker in dialogueObject.Dialogue)
        {
            SetSpeakerLabel(dialogueObject.GetSpeaker(dialogueIndex));

            SetSpriteOnPosition(dialogueObject, dialogueIndex);

            dialogueIndex++;
            yield return typeEffect.Run(speaker.dialogue, textLabel);
            yield return new WaitUntil(() => (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Mouse0)));
            //yield return new WaitForSeconds(dialogueObject.waitToChangeText);
        }

        SetInteractable(true);
        if(!repeatDialogue)
            TutorialManager.Instance.DialoguesResolved++;
        CloseDialogue();
    }

    void SetSpriteOnPosition(DialogueObject dialogueObject, int index)
    {
        switch (dialogueObject.GetSpritePosition(index))
        {
            case SpritePosition.Right:
                SetRightSpeakerSprite(dialogueObject.GetSprite(index));
                return;
            case SpritePosition.Left:
                SetLeftSpeakerSprite(dialogueObject.GetSprite(index));
                return;
        }
    }

    private void CloseDialogue()
    {
        dialogueBox.SetActive(false);
        textLabel.text = "";
        //FadeScreen.Instance.FadeOut();
    }

    private void SetInteractable(bool i)
    {
        interactable.interactable = i;
    }

    public void NextDialogue()
    {
        StopAllCoroutines();
        CloseDialogue();
    }
}
