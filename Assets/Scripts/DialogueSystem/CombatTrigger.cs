﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class CombatTrigger : Singleton<CombatTrigger>
{
    public List<CombatEvent> combatEvents;

    public void RegisterTroopChange(int meleeTroop, int distanceTroop, int airTroop)
    {
        for(int i =0; i< combatEvents.Count;i++)
        {
            
            switch (combatEvents[i].eventType)
            {
                case TypeOfCombatEvents.GettedNumberOfTroop:
                    if(CheckCondition(combatEvents[i],  meleeTroop,  distanceTroop,  airTroop))
                    {
                        combatEvents[i].ResolveEvent();
                        combatEvents.Remove(combatEvents[i]);
                    }
                    break;
            }
        }
    }

    public bool CheckCondition(CombatEvent combatEvents, int meleeTroop, int distanceTroop, int airTroop)
    {
        bool f = false,b = false, e = false, a = false;
        if (combatEvents.AllTroop != -1 && combatEvents.AllTroop <= meleeTroop + airTroop+ distanceTroop)
        {
            a = true;
        }
        if (combatEvents.MeleeTroop == -1 || combatEvents.MeleeTroop <= meleeTroop)
        {
            f = true;
        }

        if (combatEvents.DistanceTroop == -1 || combatEvents.DistanceTroop <= distanceTroop)
        {
            b = true;
        }

        if (combatEvents.AirTroop == -1 || combatEvents.AirTroop <= airTroop)
        {
            e = true;
        }

        return (f && b && e) || a;
    }
}

[System.Serializable]
public class CombatEvent
{
    public string LabelEvent;
    public DialogueObject Dialogue;
    public TypeOfCombatEvents eventType;

    [Header("Valor a obtener para que suceda el evento, si un tipo de tropa no es necesario = -1")]
    public int MeleeTroop;
    public int DistanceTroop;
    public int AirTroop;
    public int AllTroop;

    public void ResolveEvent()
    {
        TutorialManager.Instance.PauseGame();
        DialogueUI.Instance.ShowDialogue(Dialogue);
        TutorialManager.Instance.ActivePanelMarkers(5,5);
    }
}

public enum TypeOfCombatEvents
{
    GettedNumberOfTroop
}