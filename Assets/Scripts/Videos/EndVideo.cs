using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;
public class EndVideo : MonoBehaviour
{
    [SerializeField] private int nextSceneIndex;
    private VideoPlayer player;

    private void Awake()
    {
        player = GetComponent<VideoPlayer>();

        player.loopPointReached += OnClipFinished;
    }

    void OnClipFinished(VideoPlayer p)
    {
        p.Stop();
        SceneManager.LoadScene(nextSceneIndex);
    }
}
