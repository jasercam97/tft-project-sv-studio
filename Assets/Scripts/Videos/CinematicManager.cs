using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class CinematicManager : MonoBehaviour
{
    [SerializeField] private KeyCode skipKey;
    [SerializeField] private float skipTime;
    
    [SerializeField] private int lobbyIndex;

    [SerializeField] private Slider skipFill;
    [SerializeField] private GameObject skipText;
    
    private float timer;
    private bool hideText = false;
    
    private void Update()
    {
        if (Input.GetKey(skipKey))
        {
            ShowSkipText();
            
            timer += Time.deltaTime;
            if (timer >= skipTime)
            {
                LoadScene();
            }
        }
        else
        {
            if (timer > 0)
            {
                timer -= Time.deltaTime * 2.5f;
            }
            else if(hideText)
            {
                Invoke(nameof(HideSkipText), 0.15f);
                hideText = false;
            }
        }

        skipFill.value = timer / skipTime;
    }

    public void LoadScene()
    {
        SceneManager.LoadScene(lobbyIndex);
    }

    void ShowSkipText()
    {
        skipText.SetActive(true);
        hideText = true;
    }
    
    void HideSkipText()
    {
        skipText.SetActive(false);
    }
    
}
